INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Baked beans","This is a multiline description for\nBaked beans, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'B', 44.69, 20.00, 19.00, 81.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Canned Asparagus","This is a multiline description for\nCanned Asparagus, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'B', 40.70, 487.00, 1.00, 40.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Canned Ham","This is a multiline description for\nCanned Ham, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'B', 7.46, 2.00, 16.00, 53.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Canned sardines           ","This is a multiline description for\nCanned sardines           , aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'B', 75.62, 132.00, 2.00, 49.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Canned tuna","This is a multiline description for\nCanned tuna, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'B', 79.68, 449.00, 19.00, 193.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cream of mushroom soup","This is a multiline description for\nCream of mushroom soup, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'B', 44.08, 81.00, 15.00, 152.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Mushrooms","This is a multiline description for\nMushrooms, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'B', 40.66, 13.00, 0.00, 106.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Peaches","This is a multiline description for\nPeaches, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'B', 52.58, 310.00, 1.00, 720.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Pears","This is a multiline description for\nPears, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'B', 79.16, 1120.00, 3.00, 44.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Pineapple","This is a multiline description for\nPineapple, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'B', 84.66, 157.00, 4.00, 508.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Pink salmon","This is a multiline description for\nPink salmon, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'B', 47.79, 50.00, 1.00, 53.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Spam","This is a multiline description for\nSpam, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'B', 59.37, 128.00, 6.00, 99.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Tomato juice","This is a multiline description for\nTomato juice, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'B', 23.55, 91.00, 0.00, 225.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Tomato soup","This is a multiline description for\nTomato soup, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'B', 58.49, 386.00, 2.00, 953.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Vienna sausage","This is a multiline description for\nVienna sausage, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'B', 84.10, 74.00, 5.00, 91.00);
