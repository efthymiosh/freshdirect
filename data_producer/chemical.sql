INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("1,3 Butylene Glycol","This is a multiline description for\n1,3 Butylene Glycol, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 123.50, 232.00, 2.00, 27.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Acacia Powder (Gum Arabic)","This is a multiline description for\nAcacia Powder (Gum Arabic), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 118.24, 188.00, 10.00, 9.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Acetamide MEA 75%","This is a multiline description for\nAcetamide MEA 75%, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 37.83, 29.00, 3.00, 65.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Acetic Acid Glacial USP","This is a multiline description for\nAcetic Acid Glacial USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 57.72, 382.00, 4.00, 54.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Acetylated Lanolin","This is a multiline description for\nAcetylated Lanolin, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 87.35, 123.00, 6.00, 45.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Activated Charcoal","This is a multiline description for\nActivated Charcoal, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 179.59, 260.00, 9.00, 62.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Adipic Acid FCC Powder","This is a multiline description for\nAdipic Acid FCC Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 2.77, 132.00, 7.00, 41.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Algenic Acid NF","This is a multiline description for\nAlgenic Acid NF, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 78.81, 0.00, 3.00, 1.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Allantoin Powder","This is a multiline description for\nAllantoin Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 169.90, 272.00, 1.00, 58.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Almond Meal","This is a multiline description for\nAlmond Meal, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 133.06, 18.00, 3.00, 192.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Almond Oil Sweet N.F","This is a multiline description for\nAlmond Oil Sweet N.F, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 69.15, 2.00, 2.00, 100.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Aloe Vera Gel 1X(1:1)","This is a multiline description for\nAloe Vera Gel 1X(1:1), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 195.39, 311.00, 0.00, 53.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Alpha Terpineol Extra","This is a multiline description for\nAlpha Terpineol Extra, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 109.00, 190.00, 7.00, 58.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Aluminum Acetate Basic","This is a multiline description for\nAluminum Acetate Basic, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 131.35, 122.00, 12.00, 111.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Aluminum Chloride USP 6H2O","This is a multiline description for\nAluminum Chloride USP 6H2O, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 55.95, 138.00, 0.00, 112.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Aluminum Potassium","This is a multiline description for\nAluminum Potassium, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 134.84, 83.00, 5.00, 74.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Aluminum Sulfate USP","This is a multiline description for\nAluminum Sulfate USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 74.80, 347.00, 16.00, 4.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Aminoacetic Acid (Glycine)","This is a multiline description for\nAminoacetic Acid (Glycine), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 14.85, 84.00, 11.00, 54.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Aminodermin CLR","This is a multiline description for\nAminodermin CLR, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 103.07, 32.00, 0.00, 67.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Ammonyx 4002 (Stearalkonium Chloride)","This is a multiline description for\nAmmonyx 4002 (Stearalkonium Chloride), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 29.06, 14.00, 11.00, 117.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Ammonyx 4B (Stepan)","This is a multiline description for\nAmmonyx 4B (Stepan), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 178.20, 144.00, 0.00, 79.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Ammonium Bicarbonate FCC Powder","This is a multiline description for\nAmmonium Bicarbonate FCC Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 62.10, 5.00, 1.00, 89.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Ammonium Lauryl Sulfate","This is a multiline description for\nAmmonium Lauryl Sulfate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 142.48, 32.00, 1.00, 48.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("GE SAG-730(Antifoam)","This is a multiline description for\nGE SAG-730(Antifoam), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 143.69, 373.00, 12.00, 9.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Apricot Kernel Oil","This is a multiline description for\nApricot Kernel Oil, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 177.45, 259.00, 0.00, 325.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Apricot Kernal Oil (Persic Oil )","This is a multiline description for\nApricot Kernal Oil (Persic Oil ), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 73.27, 3.00, 2.00, 213.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Arlacel 165 ( ICI )","This is a multiline description for\nArlacel 165 ( ICI ), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 50.71, 71.00, 2.00, 50.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Arlacel 60 ( ICI )","This is a multiline description for\nArlacel 60 ( ICI ), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 109.07, 41.00, 0.00, 57.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Arlamol E(PPG 15 STEARYL ETHER)","This is a multiline description for\nArlamol E(PPG 15 STEARYL ETHER), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 193.15, 31.00, 20.00, 32.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Arlatone T (Unichema)","This is a multiline description for\nArlatone T (Unichema), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 53.08, 28.00, 10.00, 380.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Ascorbic Acid USP Crystal (Vitamin C)","This is a multiline description for\nAscorbic Acid USP Crystal (Vitamin C), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 86.80, 17.00, 2.00, 16.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Atlas Atlox 1045-A (ICI)","This is a multiline description for\nAtlas Atlox 1045-A (ICI), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 15.99, 40.00, 1.00, 341.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Atlas G-2162 (ICI)","This is a multiline description for\nAtlas G-2162 (ICI), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 29.86, 59.00, 13.00, 72.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Atmos 300","This is a multiline description for\nAtmos 300, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 80.41, 46.00, 2.00, 106.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Atmos 300 Kosher","This is a multiline description for\nAtmos 300 Kosher, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 149.22, 184.00, 0.00, 34.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Avocado Oil Refined Cosmetic","This is a multiline description for\nAvocado Oil Refined Cosmetic, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 10.33, 19.00, 6.00, 1.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("White Beeswax USP Slabs","This is a multiline description for\nWhite Beeswax USP Slabs, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 73.14, 155.00, 1.00, 99.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Beeswax Yellow Prills NF","This is a multiline description for\nBeeswax Yellow Prills NF, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 182.90, 21.00, 4.00, 42.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Yellow Beeswax NF Slabs","This is a multiline description for\nYellow Beeswax NF Slabs, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 58.12, 452.00, 3.00, 46.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Bentonite 4446, Albagel BC","This is a multiline description for\nBentonite 4446, Albagel BC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 49.58, 171.00, 2.00, 50.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Benzoic Acid USP Crystalline","This is a multiline description for\nBenzoic Acid USP Crystalline, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 24.46, 38.00, 12.00, 74.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Benzoic Acid USP Powder","This is a multiline description for\nBenzoic Acid USP Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 115.35, 46.00, 0.00, 38.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Benzocaine USP","This is a multiline description for\nBenzocaine USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 16.35, 33.00, 4.00, 14.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Benzoyl Peroxide 98%","This is a multiline description for\nBenzoyl Peroxide 98%, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 75.56, 52.00, 0.00, 136.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Benzyl Alcohol NF","This is a multiline description for\nBenzyl Alcohol NF, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 10.84, 73.00, 2.00, 346.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Benzyl Benzoate USP","This is a multiline description for\nBenzyl Benzoate USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 120.91, 513.00, 1.00, 225.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Betaine HCL USP","This is a multiline description for\nBetaine HCL USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 191.36, 213.00, 1.00, 22.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("BHA NF,FCC Flakes (Butylated Hydroxyanisole)","This is a multiline description for\nBHA NF,FCC Flakes (Butylated Hydroxyanisole), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 112.76, 115.00, 4.00, 115.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("BHT Crystals FCC","This is a multiline description for\nBHT Crystals FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 173.99, 90.00, 1.00, 59.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Bio-Terge AS-40 (Sodium C 14-16)","This is a multiline description for\nBio-Terge AS-40 (Sodium C 14-16 Olefin Sulfonate), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 164.99, 61.00, 7.00, 237.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Biotin Pure USP FCC","This is a multiline description for\nBiotin Pure USP FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 124.95, 60.00, 0.00, 0.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Biotin USP 1% Trituration","This is a multiline description for\nBiotin USP 1% Trituration, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 145.22, 26.00, 6.00, 151.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Blandol N.F. Mineral Oil (Witco)","This is a multiline description for\nBlandol N.F. Mineral Oil (Witco), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 127.09, 16.00, 6.00, 27.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Borage Oil 20%","This is a multiline description for\nBorage Oil 20%, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 20.83, 8.00, 0.00, 6.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Boric Acid NF Powder","This is a multiline description for\nBoric Acid NF Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 176.85, 64.00, 1.00, 59.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Brewers Yeast NF 18 Tableting Grade","This is a multiline description for\nBrewers Yeast NF 18 Tableting Grade, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 0.85, 252.00, 0.00, 207.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("BRIJ 30 (ICI)","This is a multiline description for\nBRIJ 30 (ICI), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 76.42, 52.00, 1.00, 23.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Ceteth - 2","This is a multiline description for\nCeteth - 2, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 147.02, 11.00, 0.00, 44.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("BRIJ 72 (ICI)","This is a multiline description for\nBRIJ 72 (ICI), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 94.87, 20.00, 2.00, 22.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("BRIJ 721 (ICI)","This is a multiline description for\nBRIJ 721 (ICI), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 69.00, 14.00, 19.00, 393.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("BRIJ 76 (ICI)","This is a multiline description for\nBRIJ 76 (ICI), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 45.80, 163.00, 1.00, 40.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("BRIJ 78 (Uniquema)","This is a multiline description for\nBRIJ 78 (Uniquema), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 159.18, 54.00, 2.00, 113.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("BT- Ylang Ylang Essential Oil III","This is a multiline description for\nBT- Ylang Ylang Essential Oil III, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 151.28, 134.00, 0.00, 142.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("BT-Bergamot Essential Oil (Italian)","This is a multiline description for\nBT-Bergamot Essential Oil (Italian), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 105.41, 38.00, 5.00, 239.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("BT-Cedarwood Essential Oil (Virginian)","This is a multiline description for\nBT-Cedarwood Essential Oil (Virginian), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 121.43, 389.00, 0.00, 41.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("BT-Clary Sage Essential Oil","This is a multiline description for\nBT-Clary Sage Essential Oil, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 9.23, 32.00, 6.00, 73.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("BT-Eucalyptus Essential Oil","This is a multiline description for\nBT-Eucalyptus Essential Oil, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 76.81, 8.00, 6.00, 130.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("BT-Fennel Essential Oil","This is a multiline description for\nBT-Fennel Essential Oil, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 34.57, 151.00, 6.00, 51.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("BT-Geranium Essential Oil","This is a multiline description for\nBT-Geranium Essential Oil, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 86.27, 332.00, 6.00, 25.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("BT- Grape Seed Oil","This is a multiline description for\nBT- Grape Seed Oil, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 57.11, 17.00, 11.00, 4.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("BT-Juniper Essential Oil","This is a multiline description for\nBT-Juniper Essential Oil, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 157.73, 4.00, 8.00, 37.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("BT-Lavender Essential Oil 40/42","This is a multiline description for\nBT-Lavender Essential Oil 40/42, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 59.62, 71.00, 1.00, 235.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("BT- Lemon Essential Oil","This is a multiline description for\nBT- Lemon Essential Oil, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 78.63, 52.00, 1.00, 57.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("BT-Lemongrass Essential Oil","This is a multiline description for\nBT-Lemongrass Essential Oil, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 114.21, 373.00, 16.00, 310.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("BT-Marjoram Essential Oil","This is a multiline description for\nBT-Marjoram Essential Oil, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 2.83, 142.00, 5.00, 20.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("BT-Orange Essential Oil (Brazil)","This is a multiline description for\nBT-Orange Essential Oil (Brazil), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 76.47, 62.00, 4.00, 168.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("BT-Parsley Seed Oil","This is a multiline description for\nBT-Parsley Seed Oil, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 64.74, 48.00, 0.00, 93.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("BT-Peppermint Essential Oil","This is a multiline description for\nBT-Peppermint Essential Oil, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 90.57, 370.00, 5.00, 27.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("BT-Pinus Sylvestris Essential Oil","This is a multiline description for\nBT-Pinus Sylvestris Essential Oil, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 196.80, 75.00, 4.00, 33.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("BT-Rose Otto Essential Oil","This is a multiline description for\nBT-Rose Otto Essential Oil, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 178.25, 190.00, 1.00, 33.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("BT-Rosemary Essential Oil (Spanish)","This is a multiline description for\nBT-Rosemary Essential Oil (Spanish), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 24.59, 71.00, 6.00, 118.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("BT-Rosewood Essential Oil/ Bois De Rose","This is a multiline description for\nBT-Rosewood Essential Oil/ Bois De Rose, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 76.92, 143.00, 5.00, 50.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("BT- Ylang Ylang Essential Oil #3","This is a multiline description for\nBT- Ylang Ylang Essential Oil #3, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 72.84, 50.00, 4.00, 137.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Butyl Paraben NF..20 Lb.","This is a multiline description for\nButyl Paraben NF..20 Lb., aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 145.41, 23.00, 4.00, 5.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Caffeine USP Anhydrous","This is a multiline description for\nCaffeine USP Anhydrous, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 99.69, 114.00, 5.00, 302.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Calcium Acetate FCC","This is a multiline description for\nCalcium Acetate FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 109.89, 46.00, 1.00, 76.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Calcium Boro Gluconate USP Powder","This is a multiline description for\nCalcium Boro Gluconate USP Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 153.02, 173.00, 8.00, 106.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Calcium Carbonate USP FCC Heavy Powder","This is a multiline description for\nCalcium Carbonate USP FCC Heavy Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 106.38, 0.00, 14.00, 106.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Calcium Carbonate USP FCC Lite Powder","This is a multiline description for\nCalcium Carbonate USP FCC Lite Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 87.57, 136.00, 2.00, 172.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Calcium Chloride FCC / USP Dihydrate","This is a multiline description for\nCalcium Chloride FCC / USP Dihydrate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 69.58, 779.00, 1.00, 124.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Calcium Citrate USP FCC Granular","This is a multiline description for\nCalcium Citrate USP FCC Granular, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 66.27, 205.00, 3.00, 6.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Calcium Citrate USP Powder","This is a multiline description for\nCalcium Citrate USP Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 196.31, 2.00, 0.00, 65.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Calcium Gluconate USP Mono Granular","This is a multiline description for\nCalcium Gluconate USP Mono Granular, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 123.98, 183.00, 2.00, 18.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Calcium Gluconate USP Mono Powder ","This is a multiline description for\nCalcium Gluconate USP Mono Powder , aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 16.08, 122.00, 6.00, 186.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Calcium Glycerophosphate Powder FCC","This is a multiline description for\nCalcium Glycerophosphate Powder FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 74.84, 91.00, 3.00, 236.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Calcium Hydroxide USP(Hydrated Lime)","This is a multiline description for\nCalcium Hydroxide USP(Hydrated Lime), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 104.23, 73.00, 4.00, 319.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Calcium Lactate Trihydrate USP","This is a multiline description for\nCalcium Lactate Trihydrate USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 21.41, 300.00, 7.00, 60.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Calcium Lactate Pentahydrate USP Powder ","This is a multiline description for\nCalcium Lactate Pentahydrate USP Powder , aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 181.58, 0.00, 0.00, 27.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Calcium Oxide USP (Lime USP)","This is a multiline description for\nCalcium Oxide USP (Lime USP), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 161.90, 122.00, 1.00, 156.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Calcium D Pantotherate","This is a multiline description for\nCalcium D Pantotherate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 128.90, 65.00, 1.00, 30.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Dicalcium Phosphate Anhydrous USP FCC Powder","This is a multiline description for\nDicalcium Phosphate Anhydrous USP FCC Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 35.19, 151.00, 2.00, 29.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Dicalcium Phosphate FCC","This is a multiline description for\nDicalcium Phosphate Dihydrate Unmilled USP FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 65.34, 18.00, 0.00, 120.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Tricalcium Phosphate NF","This is a multiline description for\nTricalcium Phosphate NF / FCC Powder Bulk Density > .4g/ml, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 130.95, 115.00, 4.00, 19.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Calcium Phosphate Tribasic NF FCC Powder","This is a multiline description for\nCalcium Phosphate Tribasic NF FCC Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 9.27, 22.00, 0.00, 16.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Calcium Saccharin USP / FCC","This is a multiline description for\nCalcium Saccharin USP / FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 15.56, 310.00, 4.00, 424.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Calcium Stearate NF Powder","This is a multiline description for\nCalcium Stearate NF Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 183.78, 186.00, 1.00, 130.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Calcium Sulfate Anhydrous USP/FCC","This is a multiline description for\nCalcium Sulfate Anhydrous USP/FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 80.21, 92.00, 3.00, 4.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Calendula Oil CLR","This is a multiline description for\nCalendula Oil CLR, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 185.88, 195.00, 1.00, 11.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Camphor USP Natural","This is a multiline description for\nCamphor USP Natural, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 98.63, 170.00, 2.00, 171.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Camphor Synthetic USP Powder","This is a multiline description for\nCamphor Synthetic USP Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 176.02, 77.00, 3.00, 76.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Canola Oil Refined","This is a multiline description for\nCanola Oil Refined, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 13.47, 17.00, 6.00, 12.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Caprylic / Capric Triglycerides FCC","This is a multiline description for\nCaprylic / Capric Triglycerides FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 59.12, 233.00, 1.00, 46.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Caramel Color DS400 NF / FCC","This is a multiline description for\nCaramel Color DS400 NF / FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 152.40, 260.00, 2.00, 114.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Carbomer 943 P","This is a multiline description for\nCarbomer 943 P, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 130.14, 109.00, 2.00, 5.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Carbomer 940","This is a multiline description for\nCarbomer 940, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 21.34, 330.00, 0.00, 3.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Carbomer 941","This is a multiline description for\nCarbomer 941, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 143.89, 65.00, 0.00, 23.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Carbopol ETD 2001 (Carbomer 2001)","This is a multiline description for\nCarbopol ETD 2001 (Carbomer 2001), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 30.16, 87.00, 0.00, 3.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Carbopol Ultrez 10","This is a multiline description for\nCarbopol Ultrez 10, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 193.37, 100.00, 4.00, 3.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Carnation Mineral Oil NF (Witco)","This is a multiline description for\nCarnation Mineral Oil NF (Witco), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 136.16, 40.00, 0.00, 374.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Carnauba Wax Light Yellow # 1 N.F. Flakes","This is a multiline description for\nCarnauba Wax Light Yellow # 1 N.F. Flakes, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 24.43, 67.00, 2.00, 215.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Carnauba Wax Yellow # 1 NF Powder Refined","This is a multiline description for\nCarnauba Wax Yellow # 1 NF Powder Refined, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 58.06, 154.00, 4.00, 283.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Carrageenan Gum NF","This is a multiline description for\nCarrageenan Gum NF, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 58.60, 457.00, 24.00, 51.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Carrot Oil CLR","This is a multiline description for\nCarrot Oil CLR, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 180.63, 11.00, 2.00, 97.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Castor Oil Pale Pressed","This is a multiline description for\nCastor Oil Pale Pressed, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 99.45, 107.00, 9.00, 104.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Castor Oil O&T USP","This is a multiline description for\nCastor Oil O&T USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 16.12, 252.00, 5.00, 4.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Castor Wax MP-70 Flakes","This is a multiline description for\nCastor Wax MP-70 Flakes, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 84.14, 35.00, 0.00, 10.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Castor Wax MP--88 (Hydrogenated Castor Oil)","This is a multiline description for\nCastor Wax MP--88 (Hydrogenated Castor Oil), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 104.24, 32.00, 0.00, 103.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cerasynt IP ","This is a multiline description for\nCerasynt IP , aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 128.91, 69.00, 3.00, 273.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cerasin Wax White","This is a multiline description for\nCerasin Wax White, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 27.32, 12.00, 10.00, 56.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cerasynt WM","This is a multiline description for\nCerasynt WM, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 13.37, 253.00, 8.00, 350.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Ceresine Wax White 155","This is a multiline description for\nCeresine Wax White 155, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 191.75, 38.00, 4.00, 60.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cetyl Acetate & Acetylated Lanolin Alcohol","This is a multiline description for\nCetyl Acetate & Acetylated Lanolin Alcohol, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 168.50, 69.00, 0.00, 256.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cetearyl Alcohol & Ceteareth 20","This is a multiline description for\nCetearyl Alcohol & Ceteareth 20, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 59.08, 190.00, 17.00, 125.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cetearyl Alcohol 30/70","This is a multiline description for\nCetearyl Alcohol 30/70, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 5.54, 6.00, 8.00, 79.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cetearyl Alcohol 50/50","This is a multiline description for\nCetearyl Alcohol 50/50, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 56.35, 282.00, 1.00, 260.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Ceteth 2 / BRIJ 52","This is a multiline description for\nCeteth 2 / BRIJ 52, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 64.64, 158.00, 9.00, 61.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cetyl Alcohol NF","This is a multiline description for\nCetyl Alcohol NF, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 162.44, 166.00, 11.00, 211.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cetyl Esters Wax NF (Spermacetti)","This is a multiline description for\nCetyl Esters Wax NF (Spermacetti), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 121.14, 158.00, 9.00, 83.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Chlorobutanol NF Hydrous","This is a multiline description for\nChlorobutanol NF Hydrous, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 196.78, 72.00, 0.00, 175.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Chlorophyll JJ","This is a multiline description for\nChlorophyll JJ, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 173.89, 16.00, 6.00, 18.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Choline Dihydrogen Citrate FCC.","This is a multiline description for\nCholine Dihydrogen Citrate FCC., aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 181.51, 159.00, 10.00, 273.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cholestrol NF","This is a multiline description for\nCholestrol NF, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 154.44, 149.00, 3.00, 5.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Choline Bitartrate FCC","This is a multiline description for\nCholine Bitartrate FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 118.83, 140.00, 19.00, 249.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Choline L-Bitartrate USP","This is a multiline description for\nCholine L-Bitartrate USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 79.19, 230.00, 2.00, 23.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Choline Chloride FCC","This is a multiline description for\nCholine Chloride FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 115.67, 38.00, 0.00, 20.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Chondroitin Sulfate 90% American","This is a multiline description for\nChondroitin Sulfate 90% American, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 197.05, 344.00, 6.00, 347.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Chondroitin Sulfate Chinese","This is a multiline description for\nChondroitin Sulfate 90% Schuster Tested Chinese, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 75.57, 183.00, 7.00, 28.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Chromium Oxide Green","This is a multiline description for\nChromium Oxide Green, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 178.71, 98.00, 2.00, 161.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Citric Acid Anhydrous Granular","This is a multiline description for\nCitric Acid Anhydrous Granular USP / FCC Kosher, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 53.02, 43.00, 6.00, 3.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Citric Acid Monohydrate USP Granular","This is a multiline description for\nCitric Acid Monohydrate USP Granular, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 198.37, 31.00, 0.00, 88.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Citric Acid Anhydrous USP Fine Granular (ADM)","This is a multiline description for\nCitric Acid Anhydrous USP Fine Granular (ADM), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 179.33, 9.00, 3.00, 34.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Citric Acid Anhydrous Powder USP FCC","This is a multiline description for\nCitric Acid Anhydrous Powder USP FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 182.56, 265.00, 3.00, 67.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("CMC 7HF PH USP/FCC (Sodium Carboxymethylcellulose)","This is a multiline description for\nCMC 7HF PH USP/FCC (Sodium Carboxymethylcellulose), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 29.39, 110.00, 5.00, 14.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("CMC Sodium 7 LF","This is a multiline description for\nCMC Sodium 7 LF, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 104.82, 68.00, 0.00, 232.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Coal Tar USP Topical Solution","This is a multiline description for\nCoal Tar USP Topical Solution, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 38.54, 284.00, 0.00, 9.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cocamidopropylamine Oxide (Barlox C)","This is a multiline description for\nCocamidopropylamine Oxide (Barlox C), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 88.40, 103.00, 10.00, 122.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cocamidopropyl Betaine","This is a multiline description for\nCocamidopropyl Betaine, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 170.95, 122.00, 6.00, 187.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cocoamide DEA","This is a multiline description for\nCocoamide DEA, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 168.80, 225.00, 2.00, 134.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cocoamide MEA","This is a multiline description for\nCocoamide MEA, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 181.43, 343.00, 0.00, 171.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cocoa Butter USP Deodorized","This is a multiline description for\nCocoa Butter USP Deodorized, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 163.26, 95.00, 1.00, 330.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Coconut Oil 76 Edible","This is a multiline description for\nCoconut Oil 76 Edible, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 24.37, 536.00, 0.00, 103.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cod Liver Oil USP 1000A / 100D","This is a multiline description for\nCod Liver Oil USP 1000A / 100D, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 82.01, 27.00, 3.00, 92.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cod Liver Oil USP 1000A / 100D","This is a multiline description for\nCod Liver Oil USP 1000A / 100D, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 18.75, 149.00, 5.00, 76.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cod Liver Oil Veterinary Grade 1000A-100D","This is a multiline description for\nCod Liver Oil Veterinary Grade 1000A-100D, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 51.75, 107.00, 1.00, 12.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Collagen (Solucol 1029)","This is a multiline description for\nCollagen (Solucol 1029), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 40.00, 545.00, 16.00, 4.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Collagen CLR","This is a multiline description for\nCollagen CLR, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 164.60, 62.00, 7.00, 130.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Copper Gluconate USP Powder","This is a multiline description for\nCopper Gluconate USP Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 65.70, 212.00, 3.00, 15.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Copper Sulfate Anhydrous USP FCC Pure","This is a multiline description for\nCopper Sulfate Anhydrous USP FCC Pure, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 70.37, 246.00, 0.00, 64.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Copper Sulfate Pentahydrate ACS","This is a multiline description for\nCopper Sulfate Pentahydrate ACS, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 169.53, 28.00, 0.00, 29.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Corn Oil NF FCC","This is a multiline description for\nCorn Oil NF FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 80.07, 137.00, 9.00, 68.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Corn Starch FCC","This is a multiline description for\nCorn Starch FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 72.96, 99.00, 0.00, 97.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("High Fructose Corn Syrup","This is a multiline description for\nHigh Fructose Corn Syrup, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 25.18, 39.00, 6.00, 39.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cosmedia Guar C261N","This is a multiline description for\nCosmedia Guar C261N, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 52.01, 149.00, 2.00, 8.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cosmedia SP","This is a multiline description for\nCosmedia SP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 17.95, 20.00, 8.00, 54.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cottonseed Oil NF/FCC","This is a multiline description for\nCottonseed Oil NF/FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 194.48, 56.00, 4.00, 39.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Croscarmellose Sodium, NF (Solutab A)","This is a multiline description for\nCroscarmellose Sodium, NF (Solutab A), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 44.92, 138.00, 7.00, 7.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("101-100 Cupric Oxide UP 13,600 Black","This is a multiline description for\n101-100 Cupric Oxide UP 13,600 Black, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 174.92, 311.00, 2.00, 32.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cupric Oxide UP 13,600 Black","This is a multiline description for\nCupric Oxide UP 13,600 Black, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 117.03, 26.00, 7.00, 299.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cupric Sulfate Mono Pure Powder","This is a multiline description for\nCupric Sulfate Mono Pure Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 148.00, 200.00, 4.00, 200.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("D-Biotin USP FCC Pure","This is a multiline description for\nD-Biotin USP FCC Pure, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 14.33, 46.00, 1.00, 40.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("D-Panthenol Cosmetic USP","This is a multiline description for\nD-Panthenol Cosmetic USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 1.53, 103.00, 2.00, 10.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("D-Xylitol NF FCC","This is a multiline description for\nD-Xylitol NF FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 143.26, 32.00, 3.00, 116.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Davana Oil","This is a multiline description for\nDavana Oil, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 118.24, 10.00, 0.00, 169.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("DC 11 Additive","This is a multiline description for\nDC 11 Additive, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 149.96, 208.00, 10.00, 97.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("DC Antifoam 1920","This is a multiline description for\nDC Antifoam 1920, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 145.70, 30.00, 11.00, 37.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Dextrose Hydrous USP (Monohydrate)","This is a multiline description for\nDextrose Hydrous USP (Monohydrate), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 81.71, 137.00, 1.00, 16.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Diatomaceous Earth (F-W60)","This is a multiline description for\nDiatomaceous Earth (F-W60), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 93.45, 226.00, 0.00, 77.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Diethanolamine NF","This is a multiline description for\nDiethanolamine NF, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 29.52, 28.00, 10.00, 16.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Dipropylene Glycol Low Odor","This is a multiline description for\nDipropylene Glycol Low Odor, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 153.61, 41.00, 1.00, 66.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("DL-Methionine USP FCC","This is a multiline description for\nDL-Methionine USP FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 102.62, 182.00, 0.00, 11.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("DL-Panthenol Cosmetic USP","This is a multiline description for\nDL-Panthenol Cosmetic USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 169.40, 25.00, 12.00, 90.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("DM DM Hydantoin","This is a multiline description for\nDM DM Hydantoin, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 199.39, 206.00, 1.00, 129.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Dimethylaminoethonol (DMAE)","This is a multiline description for\nDimethylaminoethonol (DMAE), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 5.25, 16.00, 0.00, 57.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("DMAE Bitartrate (Dimethylaminoethanol Bitartrate)","This is a multiline description for\nDMAE Bitartrate (Dimethylaminoethanol Bitartrate), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 157.78, 45.00, 4.00, 29.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Docusate Sodium 85% Granular USP","This is a multiline description for\nDocusate Sodium 85% Granular USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 27.94, 113.00, 7.00, 33.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Dowanol PM ","This is a multiline description for\nDowanol PM , aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 160.83, 136.00, 2.00, 84.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Duoprime 350 USP Mineral Oil ","This is a multiline description for\nDuoprime 350 USP Mineral Oil , aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 190.15, 148.00, 0.00, 62.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Duoprime 70 NF Mineral Oil","This is a multiline description for\nDuoprime 70 NF Mineral Oil, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 92.72, 32.00, 4.00, 37.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Calcium Disodium EDTA FCC","This is a multiline description for\nCalcium Disodium EDTA FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 83.13, 83.00, 11.00, 53.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("EDTA Disodium USP FCC","This is a multiline description for\nEDTA Disodium USP FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 59.30, 17.00, 0.00, 38.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("EDTA, Tetrasodium","This is a multiline description for\nEDTA, Tetrasodium, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 49.82, 24.00, 7.00, 3.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("EDTA Trisodium","This is a multiline description for\nEDTA Trisodium, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 95.94, 52.00, 1.00, 96.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Elastin 10 % Solution","This is a multiline description for\nElastin 10 % Solution, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 187.74, 11.00, 11.00, 135.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Elastin 30% ( Solu-Lastin 30).","This is a multiline description for\nElastin 30% ( Solu-Lastin 30)., aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 150.93, 53.00, 1.00, 122.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Elastin CLR","This is a multiline description for\nElastin CLR, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 8.92, 58.00, 0.00, 70.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Elfacos GT 282-S (15%)","This is a multiline description for\nElfacos GT 282-S (15%), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 120.25, 167.00, 8.00, 1.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Emery 1650 USP","This is a multiline description for\nEmery 1650 USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 45.16, 161.00, 2.00, 73.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Emulgade F (Henkel)","This is a multiline description for\nEmulgade F (Henkel), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 170.95, 104.00, 9.00, 10.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Emulgade PL 68/50","This is a multiline description for\nEmulgade PL 68/50, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 148.90, 115.00, 0.00, 25.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Emulgin L (Henkel)","This is a multiline description for\nEmulgin L (Henkel), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 188.70, 71.00, 9.00, 213.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Ethyl Vanillin NF FCC","This is a multiline description for\nEthyl Vanillin NF FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 105.02, 173.00, 4.00, 32.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Euperlan PK 3000 (Henkel)","This is a multiline description for\nEuperlan PK 3000 (Henkel), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 19.93, 351.00, 21.00, 15.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Euperlan PK--810","This is a multiline description for\nEuperlan PK--810, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 62.35, 20.00, 5.00, 244.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Eusolex 232","This is a multiline description for\nEusolex 232, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 178.37, 79.00, 13.00, 12.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Eutanol G","This is a multiline description for\nEutanol G, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 39.47, 55.00, 5.00, 5.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Evening Primrose Oil","This is a multiline description for\nEvening Primrose Oil, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 185.35, 82.00, 0.00, 111.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Ferric Ammonium Citrate Brown FCC","This is a multiline description for\nFerric Ammonium Citrate Brown FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 17.53, 44.00, 11.00, 3.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Ferrous Fumarate USP","This is a multiline description for\nFerrous Fumarate USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 146.76, 167.00, 12.00, 29.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Ferrous Gluconate USP Powder 2H2O","This is a multiline description for\nFerrous Gluconate USP Powder 2H2O, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 97.85, 96.00, 3.00, 88.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Ferrous Sulfate USP FCC Dried Powder","This is a multiline description for\nFerrous Sulfate USP FCC Dried Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 95.72, 92.00, 6.00, 156.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Ferrous Sulfate Heptahydrate USP FCC Granular","This is a multiline description for\nFerrous Sulfate Heptahydrate USP FCC Granular, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 135.63, 60.00, 6.00, 84.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Ferric Sulfate Technical Granular","This is a multiline description for\nFerric Sulfate Technical Granular, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 171.37, 125.00, 5.00, 5.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("D-Fructose Crystalline FCC/USP Fine Granular","This is a multiline description for\nD-Fructose Crystalline FCC/USP Fine Granular, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 198.49, 49.00, 0.00, 251.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Gingko Biloba 24/6 P.E. Low Acid","This is a multiline description for\nGingko Biloba 24/6 P.E. Low Acid, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 162.53, 38.00, 4.00, 230.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Glucosamine HCL","This is a multiline description for\nGlucosamine HCL, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 112.03, 83.00, 13.00, 34.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Glycerin 96 % USP","This is a multiline description for\nGlycerin 96 % USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 44.95, 232.00, 4.00, 125.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Glycerin 99.5 % USP","This is a multiline description for\nGlycerin 99.5 % USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 76.65, 11.00, 1.00, 197.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Glycerin 99.5 % USP","This is a multiline description for\nGlycerin 99.5 % USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 123.05, 4.00, 5.00, 34.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Glycerin 99.5% USP/FCC Kosher","This is a multiline description for\nGlycerin 99.5% USP/FCC Kosher, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 84.35, 54.00, 9.00, 32.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Glycereth-26","This is a multiline description for\nGlycereth-26, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 197.73, 100.00, 2.00, 397.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Glycine USP (Aminoacetic Acid)","This is a multiline description for\nGlycine USP (Aminoacetic Acid), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 175.74, 72.00, 11.00, 117.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Glycol Stearate Self Emulsifying","This is a multiline description for\nGlycol Stearate Self Emulsifying, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 140.28, 84.00, 4.00, 30.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Glycolic Acid 99 % Powder Cosmetic","This is a multiline description for\nGlycolic Acid 99 % Powder Cosmetic, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 195.32, 92.00, 4.00, 22.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Glyceryl Stearate & PEG 100 Stearate","This is a multiline description for\nGlyceryl Stearate & PEG 100 Stearate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 73.56, 14.00, 12.00, 139.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Glyceryl Monostearate Pure ","This is a multiline description for\nGlyceryl Monostearate Pure , aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 39.99, 30.00, 4.00, 168.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Glyceryl Monostearate Self Emulsifying","This is a multiline description for\nGlyceryl Monostearate Self Emulsifying, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 50.88, 38.00, 0.00, 112.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Grape Seed Oil","This is a multiline description for\nGrape Seed Oil, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 124.12, 446.00, 15.00, 93.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Guaifenesin USP","This is a multiline description for\nGuaifenesin USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 104.60, 42.00, 4.00, 51.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Guar Gum FCC 200 Mesh Powder","This is a multiline description for\nGuar Gum FCC 200 Mesh Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 145.32, 84.00, 0.00, 55.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Hexaplant-Richter (Henkel)","This is a multiline description for\nHexaplant-Richter (Henkel), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 196.30, 144.00, 5.00, 317.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Homo Menthyl Salicylate","This is a multiline description for\nHomo Menthyl Salicylate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 41.53, 35.00, 0.00, 85.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Hydrogen Peroxide 35 % Super D Cosmetic","This is a multiline description for\nHydrogen Peroxide 35 % Super D Cosmetic, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 70.72, 9.00, 1.00, 80.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Hydrogenated Polybutene","This is a multiline description for\nHydrogenated Polybutene, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 58.61, 350.00, 2.00, 79.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Hydrolyzed Soy Protein","This is a multiline description for\nHydrolyzed Soy Protein, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 40.91, 81.00, 1.00, 174.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Hydroxypropyl Methyl Cellulose 6 cps","This is a multiline description for\nHydroxypropyl Methyl Cellulose 6 cps, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 89.85, 24.00, 2.00, 358.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Imidazolidinyl Urea","This is a multiline description for\nImidazolidinyl Urea, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 93.17, 3.00, 2.00, 21.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Inositol NF FCC Powder","This is a multiline description for\nInositol NF FCC Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 103.09, 78.00, 4.00, 68.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Isopropyl Alcohol USP 99 %","This is a multiline description for\nIsopropyl Alcohol USP 99 %, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 30.33, 46.00, 3.00, 44.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Isopropyl Lanolate","This is a multiline description for\nIsopropyl Lanolate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 111.05, 119.00, 13.00, 239.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Isopropyl Myristate NF","This is a multiline description for\nIsopropyl Myristate NF, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 137.84, 37.00, 3.00, 403.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Isopropyl Myristate NF Odorless","This is a multiline description for\nIsopropyl Myristate NF Odorless, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 40.15, 222.00, 3.00, 51.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Isopropyl Palmitate NF","This is a multiline description for\nIsopropyl Palmitate NF, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 61.70, 174.00, 1.00, 141.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Jojoba Oil Clear","This is a multiline description for\nJojoba Oil Clear, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 186.35, 136.00, 6.00, 50.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Kaolin Colloidal USP","This is a multiline description for\nKaolin Colloidal USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 137.92, 13.00, 4.00, 22.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Karaya Gum #1 NF FCC Powder","This is a multiline description for\nKaraya Gum #1 NF FCC Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 114.21, 29.00, 6.00, 128.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Kaydol Mineral Oil USP","This is a multiline description for\nKaydol Mineral Oil USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 104.13, 21.00, 0.00, 7.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Klearol NF Mineral Oil","This is a multiline description for\nKlearol NF Mineral Oil, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 154.30, 2.00, 20.00, 61.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("L-Arginine Base","This is a multiline description for\nL-Arginine Base, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 55.62, 74.00, 2.00, 65.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("L-Lysine Mono HCL USP","This is a multiline description for\nL-Lysine Mono HCL USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 55.67, 450.00, 10.00, 88.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Lactic Acid 88% FCC","This is a multiline description for\nLactic Acid 88% FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 145.23, 15.00, 2.00, 107.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Lactic Acid 88% USP Heat Stable (ADM)","This is a multiline description for\nLactic Acid 88% USP Heat Stable (ADM), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 155.90, 9.00, 0.00, 63.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Lactose # 312","This is a multiline description for\nLactose # 312, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 35.73, 129.00, 5.00, 138.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Lactose Fast Flo NF Monohydrate 316 Fine Powder","This is a multiline description for\nLactose Fast Flo NF Monohydrate 316 Fine Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 171.24, 58.00, 3.00, 47.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Lamesoft PO-65","This is a multiline description for\nLamesoft PO-65, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 106.66, 101.00, 3.00, 36.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Laneth 10-Acetate","This is a multiline description for\nLaneth 10-Acetate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 114.24, 71.00, 7.00, 189.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Laneth 23","This is a multiline description for\nLaneth 23, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 176.92, 29.00, 1.00, 17.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Lanette 22","This is a multiline description for\nLanette 22, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 36.25, 29.00, 6.00, 237.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Lanette E (Sodium Cetearyl Sulfate)","This is a multiline description for\nLanette E (Sodium Cetearyl Sulfate), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 65.39, 12.00, 4.00, 11.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Lanolin USP Anhydrous","This is a multiline description for\nLanolin USP Anhydrous, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 33.78, 121.00, 4.00, 42.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Lanolin Alcohol","This is a multiline description for\nLanolin Alcohol, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 53.29, 35.00, 0.00, 13.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Lanolin Oil USP Cosmetic","This is a multiline description for\nLanolin Oil USP Cosmetic, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 125.18, 105.00, 3.00, 17.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Lathanol LAL Powder (Stepan) (Sodium Lauryl Sulfoacetate)","This is a multiline description for\nLathanol LAL Powder (Stepan) (Sodium Lauryl Sulfoacetate), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 24.81, 380.00, 3.00, 118.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Lauramide DEA","This is a multiline description for\nLauramide DEA, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 77.93, 32.00, 3.00, 32.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Laureth 23","This is a multiline description for\nLaureth 23, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 87.60, 257.00, 2.00, 96.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Linalool Oxide","This is a multiline description for\nLinalool Oxide, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 17.00, 25.00, 3.00, 53.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Linoleic Acid (Emersol 315)","This is a multiline description for\nLinoleic Acid (Emersol 315), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 56.25, 12.00, 3.00, 59.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Lipovol MOS-70","This is a multiline description for\nLipovol MOS-70, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 142.55, 12.00, 7.00, 387.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Liquid Glucose (High Maltose Corn Syrup)","This is a multiline description for\nLiquid Glucose (High Maltose Corn Syrup), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 68.73, 281.00, 0.00, 349.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Liver Fraction #1 Paste","This is a multiline description for\nLiver Fraction #1 Paste, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 92.91, 114.00, 0.00, 88.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Liver Granular Dessicated & Defatted","This is a multiline description for\nLiver Granular Dessicated & Defatted, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 110.72, 112.00, 1.00, 286.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Liver Powder Dessicated & Defatted","This is a multiline description for\nLiver Powder Dessicated & Defatted, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 164.45, 220.00, 1.00, 30.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Magnesium Aluminum Silicate","This is a multiline description for\nMagnesium Aluminum Silicate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 140.22, 132.00, 10.00, 154.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Magnesium Carbonate USP Granular","This is a multiline description for\nMagnesium Carbonate USP Granular, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 168.33, 65.00, 1.00, 13.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Magnesium Carbonate USP Heavy Powder","This is a multiline description for\nMagnesium Carbonate USP Heavy Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 99.62, 157.00, 4.00, 23.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Magnesium Carbonate USP Lite Powder ","This is a multiline description for\nMagnesium Carbonate USP Lite Powder , aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 162.08, 57.00, 1.00, 16.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Magnesium Chloride USP ACS Crystalline","This is a multiline description for\nMagnesium Chloride USP ACS Crystalline, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 53.14, 43.00, 0.00, 131.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Magnesium Citrate FCC 16% Granular","This is a multiline description for\nMagnesium Citrate FCC 16% Granular, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 178.67, 36.00, 1.00, 59.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Magnesium Citrate Dibasic Powder","This is a multiline description for\nMagnesium Citrate Dibasic Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 169.02, 115.00, 2.00, 110.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Magnesium Citrate Tribasic Purified","This is a multiline description for\nMagnesium Citrate Tribasic Purified, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 86.22, 116.00, 13.00, 14.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Magnesium Gluconate Granular USP","This is a multiline description for\nMagnesium Gluconate Granular USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 52.49, 205.00, 12.00, 10.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Magnesium Gluconate USP FCC Granular","This is a multiline description for\nMagnesium Gluconate USP FCC Granular, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 42.66, 15.00, 1.00, 124.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Magnesium Hydroxide USP Powder","This is a multiline description for\nMagnesium Hydroxide USP Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 166.57, 201.00, 12.00, 103.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Magnesium Oxide USP Heavy Powder","This is a multiline description for\nMagnesium Oxide USP Heavy Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 34.18, 14.00, 7.00, 57.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Magnesium Oxide USP Lite Powder","This is a multiline description for\nMagnesium Oxide USP Lite Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 80.85, 317.00, 0.00, 35.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Magnesium Oxide USP FCC Granular","This is a multiline description for\nMagnesium Oxide USP FCC Granular, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 34.65, 83.00, 6.00, 1.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Magnesium Phosphate Tribasic","This is a multiline description for\nMagnesium Phosphate Tribasic, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 17.67, 18.00, 5.00, 73.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Magnesium Salicylate USP Granular","This is a multiline description for\nMagnesium Salicylate USP Granular, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 148.50, 118.00, 5.00, 0.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Magnesium Stearate Vegetable Powder NF","This is a multiline description for\nMagnesium Stearate Vegetable Powder NF, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 84.10, 105.00, 0.00, 100.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Magnesium Stearate Animal NF Powder","This is a multiline description for\nMagnesium Stearate Animal NF Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 137.26, 51.00, 2.00, 73.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Magnesium Stearate NF Powder Kosher","This is a multiline description for\nMagnesium Stearate NF Powder Kosher, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 27.25, 38.00, 0.00, 36.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Magnesium Sulfate Anhydrous FCC USP Pure Powder","This is a multiline description for\nMagnesium Sulfate Anhydrous FCC USP Pure Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 75.95, 401.00, 6.00, 153.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Magnesium Trisilicate USP FCC","This is a multiline description for\nMagnesium Trisilicate USP FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 176.13, 301.00, 0.00, 5.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Malic Acid FCC Granular Kosher","This is a multiline description for\nMalic Acid FCC Granular Kosher, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 79.24, 218.00, 0.00, 66.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Malic Acid FCC","This is a multiline description for\nMalic Acid FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 192.19, 177.00, 0.00, 121.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Manganese Carbonate ACS","This is a multiline description for\nManganese Carbonate ACS, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 74.26, 133.00, 6.00, 156.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Manganese Gluconate USP FCC Granular","This is a multiline description for\nManganese Gluconate USP FCC Granular, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 45.11, 212.00, 2.00, 242.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Manganese Gluconate USP FCC Powder","This is a multiline description for\nManganese Gluconate USP FCC Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 30.75, 184.00, 2.00, 34.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Manganese Sulfate Monohydrate USP ","This is a multiline description for\nManganese Sulfate Monohydrate USP , aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 7.65, 112.00, 4.00, 54.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Mannitol Fine Granular","This is a multiline description for\nMannitol Fine Granular, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 110.63, 52.00, 1.00, 56.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Mannitol USP Fine Granular","This is a multiline description for\nMannitol USP Fine Granular, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 50.28, 41.00, 3.00, 104.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Mannitol Powder USP","This is a multiline description for\nMannitol Powder USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 21.97, 102.00, 0.00, 7.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Mannitol USP Granular (SPI)","This is a multiline description for\nMannitol USP Granular (SPI), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 63.59, 18.00, 7.00, 116.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Microcrystalline Cellulose -101","This is a multiline description for\nMicrocrystalline Cellulose -101, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 158.37, 82.00, 3.00, 1.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Microcrystalline Cellulose -102","This is a multiline description for\nMicrocrystalline Cellulose -102, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 60.06, 296.00, 13.00, 60.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Menthol Crystals USP Natural","This is a multiline description for\nMenthol Crystals USP Natural, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 135.50, 55.00, 6.00, 125.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Methenamine USP","This is a multiline description for\nMethenamine USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 190.73, 52.00, 0.00, 70.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Methocel E4M (HPMC)","This is a multiline description for\nMethocel E4M (HPMC), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 136.41, 51.00, 14.00, 271.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Methyl Paraben NF","This is a multiline description for\nMethyl Paraben NF, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 123.88, 69.00, 4.00, 130.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Methyl Paraben Sodium","This is a multiline description for\nMethyl Paraben Sodium, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 197.65, 156.00, 8.00, 36.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Methyl Salicylate NF","This is a multiline description for\nMethyl Salicylate NF, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 169.90, 106.00, 26.00, 102.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Mineral Jelly # 10","This is a multiline description for\nMineral Jelly # 10, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 11.41, 80.00, 5.00, 44.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Mineral Jelly # 25","This is a multiline description for\nMineral Jelly # 25, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 76.64, 110.00, 8.00, 6.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Mineral Spirits Odorless","This is a multiline description for\nMineral Spirits Odorless, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 189.67, 101.00, 7.00, 232.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Mineral Oil # 18 (Drakeol # 19)","This is a multiline description for\nMineral Oil # 18 (Drakeol # 19), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 179.33, 301.00, 2.00, 86.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Drakeol 21 Mineral Oil USP","This is a multiline description for\nDrakeol 21 Mineral Oil USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 49.17, 53.00, 3.00, 200.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Drakeol 35 Mineral Oil USP ","This is a multiline description for\nDrakeol 35 Mineral Oil USP , aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 155.53, 12.00, 4.00, 145.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Penetec 40 Mineral Oil ","This is a multiline description for\nPenetec 40 Mineral Oil , aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 161.32, 62.00, 0.00, 106.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Drakeol 5 Mineral Oil NF","This is a multiline description for\nDrakeol 5 Mineral Oil NF, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 193.77, 8.00, 1.00, 2.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Light Mineral Oil # 55 NF","This is a multiline description for\nLight Mineral Oil # 55 NF, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 108.81, 391.00, 2.00, 262.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Drakeol 6 Mineral Oil NF","This is a multiline description for\nDrakeol 6 Mineral Oil NF, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 108.88, 27.00, 22.00, 67.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Drakeol 7 Mineral Oil NF","This is a multiline description for\nDrakeol 7 Mineral Oil NF, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 150.04, 180.00, 2.00, 104.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Mineral Oil #9 NF","This is a multiline description for\nMineral Oil #9 NF, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 134.89, 203.00, 1.00, 252.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Odorless Mineral Spirits","This is a multiline description for\nOdorless Mineral Spirits, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 30.57, 167.00, 7.00, 266.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Myristyl Myristate Flakes","This is a multiline description for\nMyristyl Myristate Flakes, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 166.21, 59.00, 5.00, 101.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Monaterg 779 (Uniqema)","This is a multiline description for\nMonaterg 779 (Uniqema), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 154.91, 182.00, 1.00, 136.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Monosodium Glutamate FCC","This is a multiline description for\nMonosodium Glutamate FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 167.20, 111.00, 7.00, 47.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Multiwax W-445","This is a multiline description for\nMultiwax W-445, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 109.52, 20.00, 1.00, 9.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Multiwax W-835","This is a multiline description for\nMultiwax W-835, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 69.47, 43.00, 4.00, 10.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Multiwax X-145A","This is a multiline description for\nMultiwax X-145A, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 11.93, 485.00, 6.00, 132.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Myoxinol LS-9736","This is a multiline description for\nMyoxinol LS-9736, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 156.58, 437.00, 5.00, 274.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Myristyl Lactate","This is a multiline description for\nMyristyl Lactate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 141.53, 72.00, 10.00, 77.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Myrj 52 Flakes","This is a multiline description for\nMyrj 52 Flakes, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 164.01, 103.00, 0.00, 90.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("MYRJ 52 NF Solid","This is a multiline description for\nMYRJ 52 NF Solid, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 66.22, 79.00, 2.00, 18.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Myrj 59 (Unichema)","This is a multiline description for\nMyrj 59 (Unichema), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 12.28, 14.00, 3.00, 93.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Niacinamide USP","This is a multiline description for\nNiacinamide USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 51.99, 240.00, 6.00, 93.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Ninol 40-CO","This is a multiline description for\nNinol 40-CO, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 29.97, 42.00, 6.00, 23.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Nonoxynol-9","This is a multiline description for\nNonoxynol-9, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 53.02, 74.00, 14.00, 4.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Octyl Dimethyl PABA","This is a multiline description for\nOctyl Dimethyl PABA, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 83.81, 113.00, 20.00, 13.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Octyl Methoxycinnamate","This is a multiline description for\nOctyl Methoxycinnamate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 7.60, 237.00, 5.00, 195.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Octyl Palmitate","This is a multiline description for\nOctyl Palmitate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 82.50, 230.00, 3.00, 136.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Octyl Palmitate","This is a multiline description for\nOctyl Palmitate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 62.35, 195.00, 12.00, 46.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Oleic Acid NF","This is a multiline description for\nOleic Acid NF, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 179.61, 76.00, 20.00, 17.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Oleth - 3 ","This is a multiline description for\nOleth - 3 , aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 62.49, 143.00, 7.00, 62.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Oleyl Alcohol 95%","This is a multiline description for\nOleyl Alcohol 95%, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 193.75, 79.00, 5.00, 26.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Olive Oil NF / FCC Edible","This is a multiline description for\nOlive Oil NF / FCC Edible, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 183.85, 119.00, 1.00, 41.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Olive Oil Extra Virgin","This is a multiline description for\nOlive Oil Extra Virgin, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 42.67, 38.00, 3.00, 28.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Olive Oil Extra Virgin Organic","This is a multiline description for\nOlive Oil Extra Virgin Organic, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 23.08, 17.00, 0.00, 75.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Ozokerite Wax White","This is a multiline description for\nOzokerite Wax White, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 194.39, 103.00, 1.00, 33.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Para Aminobenzoic Acid USP Powder","This is a multiline description for\nPara Aminobenzoic Acid USP Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 70.54, 25.00, 5.00, 28.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Paraffin Wax NF Medium 128-130","This is a multiline description for\nParaffin Wax NF Medium 128-130, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 128.01, 127.00, 2.00, 304.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Paraffin Wax NF 140/45 Refined","This is a multiline description for\nParaffin Wax NF 140/45 Refined, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 64.40, 23.00, 2.00, 34.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Peanut Oil NF","This is a multiline description for\nPeanut Oil NF, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 128.82, 11.00, 3.00, 82.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("PEG 100 Stearate Flakes","This is a multiline description for\nPEG 100 Stearate Flakes, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 140.38, 205.00, 11.00, 246.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Polyethylene Glycol 1000 NF","This is a multiline description for\nPolyethylene Glycol 1000 NF, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 14.38, 57.00, 0.00, 20.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Carbowax PEG 1450 Flake NF Sentry ","This is a multiline description for\nCarbowax PEG 1450 Flake NF Sentry , aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 99.38, 40.00, 6.00, 63.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Polyethylene Glycol 1500/540 Blend NF Sentry","This is a multiline description for\nPolyethylene Glycol 1500/540 Blend NF Sentry, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 167.59, 539.00, 6.00, 21.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Polyethylene Glycol 20 / Carbowax Sentry PEG 1000","This is a multiline description for\nPolyethylene Glycol 20 / Carbowax Sentry PEG 1000, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 33.77, 65.00, 2.00, 165.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Polyethylene Glycol 20 M Compound","This is a multiline description for\nPolyethylene Glycol 20 M Compound, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 9.64, 237.00, 7.00, 46.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Polyethylene Glycol 200 Technical","This is a multiline description for\nPolyethylene Glycol 200 Technical, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 4.81, 24.00, 5.00, 110.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Polyethylene Glycol 30 Glyceryl Cocoate","This is a multiline description for\nPolyethylene Glycol 30 Glyceryl Cocoate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 52.54, 25.00, 1.00, 334.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("PEG 300 NF ","This is a multiline description for\nPEG 300 NF , aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 198.40, 17.00, 10.00, 6.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Polyethylene Glycol 300 Carbowax Sentry N.F","This is a multiline description for\nPolyethylene Glycol 300 Carbowax Sentry N.F, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 99.95, 5.00, 1.00, 568.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Polyethylene Glycol 300 Technical","This is a multiline description for\nPolyethylene Glycol 300 Technical, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 64.45, 424.00, 3.00, 17.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Polyethylene Glycol 3350 NF Flake","This is a multiline description for\nPolyethylene Glycol 3350 NF Flake, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 190.62, 30.00, 2.00, 13.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Polyethylene Glycol 3350 Granular Carbowax Sentry NF","This is a multiline description for\nPolyethylene Glycol 3350 Granular Carbowax Sentry NF, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 125.45, 196.00, 13.00, 112.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Polyethylene Glycol 3350 USP Powder","This is a multiline description for\nPolyethylene Glycol 3350 USP Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 128.44, 147.00, 0.00, 109.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Carbowax Polyethylene Glycol 3350 NF, FCC Powder","This is a multiline description for\nCarbowax Polyethylene Glycol 3350 NF, FCC Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 173.67, 153.00, 2.00, 14.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Polyethylene Glycol 4 Dilaurate","This is a multiline description for\nPolyethylene Glycol 4 Dilaurate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 178.46, 104.00, 0.00, 215.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Polyethylene Glycol 40 Castor Oil","This is a multiline description for\nPolyethylene Glycol 40 Castor Oil, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 139.81, 463.00, 14.00, 22.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Polyethylene Glycol 40 Stearate Flakes","This is a multiline description for\nPolyethylene Glycol 40 Stearate Flakes, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 4.36, 27.00, 1.00, 42.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Polyethylene Glycol 40 Stearate Flakes ","This is a multiline description for\nPolyethylene Glycol 40 Stearate Flakes , aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 33.17, 135.00, 3.00, 12.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Polyethylene Glycol 400 NF","This is a multiline description for\nPolyethylene Glycol 400 NF, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 175.76, 241.00, 1.00, 70.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Polyethylene Glycol 400 Monostearate","This is a multiline description for\nPolyethylene Glycol 400 Monostearate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 129.44, 23.00, 1.00, 34.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("PEG 50 Stearate","This is a multiline description for\nPEG 50 Stearate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 172.78, 32.00, 15.00, 131.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Polyethylene Glycol 6 Stearate","This is a multiline description for\nPolyethylene Glycol 6 Stearate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 186.77, 42.00, 9.00, 174.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Polyethylene Glycol 6000 Distearate","This is a multiline description for\nPolyethylene Glycol 6000 Distearate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 171.20, 399.00, 6.00, 80.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Polyethylene Glycol 75 Lanolin","This is a multiline description for\nPolyethylene Glycol 75 Lanolin, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 186.88, 134.00, 7.00, 312.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Polyethylene Glycol 8000 NF Flakes","This is a multiline description for\nPolyethylene Glycol 8000 NF Flakes, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 171.62, 372.00, 1.00, 2.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Carbowax-Sentry PEG 8000 Granular NF","This is a multiline description for\nCarbowax-Sentry PEG 8000 Granular NF, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 122.02, 204.00, 0.00, 67.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Polyethylene Glycol 8000 Powder","This is a multiline description for\nPolyethylene Glycol 8000 Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 131.91, 60.00, 26.00, 36.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Penrico #2251 Oil (Petrolatum Distillate)","This is a multiline description for\nPenrico #2251 Oil (Petrolatum Distillate), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 111.11, 24.00, 4.00, 54.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Peru Balsam NF Purified","This is a multiline description for\nPeru Balsam NF Purified, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 80.71, 100.00, 0.00, 41.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Snow White Petrolatum USP","This is a multiline description for\nSnow White Petrolatum USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 168.38, 1.00, 0.00, 119.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Yellow Petrolatum USP 2A (Amber Petrolatum USP)","This is a multiline description for\nYellow Petrolatum USP 2A (Amber Petrolatum USP), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 194.79, 258.00, 1.00, 238.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Propylene Glycol USP","This is a multiline description for\nPropylene Glycol USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 106.53, 0.00, 9.00, 49.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Propylene Glycol Dicarpylate / Dicaprate","This is a multiline description for\nPropylene Glycol Dicarpylate / Dicaprate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 191.20, 22.00, 2.00, 83.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Propylene Glycol USP/FCC Kosher","This is a multiline description for\nPropylene Glycol USP/FCC Kosher, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 193.85, 101.00, 0.00, 99.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Phenoxyethanol","This is a multiline description for\nPhenoxyethanol, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 188.31, 6.00, 13.00, 68.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Phenyl Ethyl Alcohol USP / FCC","This is a multiline description for\nPhenyl Ethyl Alcohol USP / FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 45.24, 94.00, 12.00, 99.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Phenyl Salicylate Pure Crystals","This is a multiline description for\nPhenyl Salicylate Pure Crystals, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 16.72, 8.00, 17.00, 259.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Plantaren 1200 (Henkel)","This is a multiline description for\nPlantaren 1200 (Henkel), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 16.64, 17.00, 4.00, 5.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Plantaren 2000 (Henkel)","This is a multiline description for\nPlantaren 2000 (Henkel), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 0.96, 261.00, 16.00, 77.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Plantaren APB (Henkel)","This is a multiline description for\nPlantaren APB (Henkel), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 176.62, 221.00, 13.00, 182.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Plantaren PS-400 (Henkel)","This is a multiline description for\nPlantaren PS-400 (Henkel), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 198.96, 52.00, 2.00, 332.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Polawax NF","This is a multiline description for\nPolawax NF, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 180.16, 77.00, 4.00, 184.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Polysorbate 20 NF","This is a multiline description for\nPolysorbate 20 NF, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 102.00, 93.00, 2.00, 133.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Polysorbate 20 NF","This is a multiline description for\nPolysorbate 20 NF, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 12.03, 36.00, 2.00, 15.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Polysorbate 40 NF","This is a multiline description for\nPolysorbate 40 NF, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 64.84, 50.00, 0.00, 52.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Polysorbate 60 NF","This is a multiline description for\nPolysorbate 60 NF, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 92.80, 147.00, 3.00, 63.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Polysorbate 80 NF","This is a multiline description for\nPolysorbate 80 NF, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 113.79, 70.00, 0.00, 60.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Polysorbate 80 NF Kosher","This is a multiline description for\nPolysorbate 80 NF Kosher, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 52.03, 340.00, 1.00, 29.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Polyglyceryl 4 Oleate","This is a multiline description for\nPolyglyceryl 4 Oleate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 55.52, 148.00, 1.00, 19.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Potassium Acetate Anhydrous USP","This is a multiline description for\nPotassium Acetate Anhydrous USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 31.30, 208.00, 0.00, 109.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Potassium Aluminum FCC Granular","This is a multiline description for\nPotassium Aluminum FCC Granular, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 68.03, 55.00, 2.00, 51.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Potassium Benzoate FCC","This is a multiline description for\nPotassium Benzoate FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 123.24, 182.00, 3.00, 281.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Potassium Bicarbonate USP FCC Powder","This is a multiline description for\nPotassium Bicarbonate USP FCC Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 20.38, 118.00, 2.00, 83.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Potassium Carbonate Anhydrous FCC Granular","This is a multiline description for\nPotassium Carbonate Anhydrous FCC Granular, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 61.17, 209.00, 0.00, 207.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Potassium Chloride USP Granular","This is a multiline description for\nPotassium Chloride USP Granular, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 48.50, 5.00, 1.00, 69.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Potassium Citrate USP FCC Granular","This is a multiline description for\nPotassium Citrate USP FCC Granular, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 195.11, 120.00, 7.00, 29.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Potassium Citrate USP FCC Granular","This is a multiline description for\nPotassium Citrate USP FCC Granular, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 62.85, 468.00, 2.00, 63.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Potassium Gluconate USP Granular","This is a multiline description for\nPotassium Gluconate USP Granular, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 157.65, 75.00, 0.00, 13.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Potassium Glycerophosphate 60 % Solution","This is a multiline description for\nPotassium Glycerophosphate 60 % Solution, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 162.50, 97.00, 3.00, 55.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Potassium Hydroxide NF Pellets","This is a multiline description for\nPotassium Hydroxide NF Pellets, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 61.70, 9.00, 2.00, 52.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Potassium Iodide USP ACS Granular","This is a multiline description for\nPotassium Iodide USP ACS Granular, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 137.19, 157.00, 18.00, 479.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Potassium Nitrate Pure Granular FCC","This is a multiline description for\nPotassium Nitrate Pure Granular FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 105.85, 1.00, 10.00, 147.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Potassium Nitrate Pure FCC","This is a multiline description for\nPotassium Nitrate Pure FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 10.42, 26.00, 1.00, 2.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Potassium Phosphate Monobasic NF Powder","This is a multiline description for\nPotassium Phosphate Monobasic NF Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 162.25, 4.00, 1.00, 13.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Potassium Sorbate Granular FCC","This is a multiline description for\nPotassium Sorbate Granular FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 1.70, 155.00, 4.00, 159.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Potassium Sulfate ACS Anhydrous Powder","This is a multiline description for\nPotassium Sulfate ACS Anhydrous Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 32.93, 165.00, 12.00, 377.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Potassium Sulfate Anhydrous Pure FCC USP Powder","This is a multiline description for\nPotassium Sulfate Anhydrous Pure FCC USP Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 54.66, 75.00, 2.00, 87.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Potassium Sulfate USP Anhydrous","This is a multiline description for\nPotassium Sulfate USP Anhydrous, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 105.32, 186.00, 24.00, 23.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Procaine HCL USP Crystals","This is a multiline description for\nProcaine HCL USP Crystals, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 98.35, 432.00, 1.00, 2.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Progesterone USP Micronized","This is a multiline description for\nProgesterone USP Micronized, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 195.92, 9.00, 7.00, 3.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Promulgen G (Stearyl Alcohol & Cetereth 20) ","This is a multiline description for\nPromulgen G (Stearyl Alcohol & Cetereth 20) , aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 52.83, 8.00, 0.00, 7.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Propyl Paraben NF","This is a multiline description for\nPropyl Paraben NF, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 19.83, 51.00, 1.00, 11.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Propyl Paraben Sodium","This is a multiline description for\nPropyl Paraben Sodium, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 10.51, 199.00, 11.00, 24.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Protochem AWS (PPG-5-Ceteth 20)","This is a multiline description for\nProtochem AWS (PPG-5-Ceteth 20), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 106.12, 221.00, 1.00, 162.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Pyridoxine HCL USP","This is a multiline description for\nPyridoxine HCL USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 56.71, 114.00, 2.00, 166.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Riboflavin USP (Vitamin B2 USP)","This is a multiline description for\nRiboflavin USP (Vitamin B2 USP), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 75.02, 59.00, 0.00, 201.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Rice Bran Wax Powder FCC","This is a multiline description for\nRice Bran Wax Powder FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 69.31, 32.00, 10.00, 39.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Rutin NF Powder","This is a multiline description for\nRutin NF Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 59.57, 142.00, 2.00, 82.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Saccharin Insoluble NF","This is a multiline description for\nSaccharin Insoluble NF, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 149.55, 67.00, 5.00, 187.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Safflower Oil Edible USP","This is a multiline description for\nSafflower Oil Edible USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 151.49, 227.00, 15.00, 166.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Safflower Oil Edible USP","This is a multiline description for\nSafflower Oil Edible USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 127.11, 244.00, 0.00, 18.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Salicylic Acid USP Crystalline","This is a multiline description for\nSalicylic Acid USP Crystalline, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 137.73, 31.00, 17.00, 358.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Salicylic Acid USP Powder","This is a multiline description for\nSalicylic Acid USP Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 164.37, 54.00, 7.00, 21.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("SD Alcohol 40-2 190 Proof","This is a multiline description for\nSD Alcohol 40-2 190 Proof, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 146.76, 204.00, 0.00, 8.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sesame Oil NF","This is a multiline description for\nSesame Oil NF, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 6.42, 378.00, 2.00, 40.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sesame Oil NF Refined","This is a multiline description for\nSesame Oil NF Refined, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 126.44, 180.00, 11.00, 47.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Silicone Fluid 100cs (Rhodia)","This is a multiline description for\nSilicone Fluid 100cs (Rhodia), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 36.32, 94.00, 1.00, 279.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("DC Silicone Fluid 1248","This is a multiline description for\nDC Silicone Fluid 1248, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 73.56, 25.00, 6.00, 0.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Silicone Fluid GE SF-132","This is a multiline description for\nSilicone Fluid GE SF-132, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 8.08, 187.00, 0.00, 130.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("DC Silicone 1401 Fluid","This is a multiline description for\nDC Silicone 1401 Fluid, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 199.58, 18.00, 6.00, 171.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Silicone Fluid #200/10CS","This is a multiline description for\nSilicone Fluid #200/10CS, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 99.10, 82.00, 0.00, 26.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("DC Silicone Fluid 200/10 CPS","This is a multiline description for\nDC Silicone Fluid 200/10 CPS, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 57.74, 10.00, 1.00, 38.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("DC Silicone 200/100 Fluid","This is a multiline description for\nDC Silicone 200/100 Fluid, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 6.44, 157.00, 5.00, 109.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("DC Silicone Fluid 244","This is a multiline description for\nDC Silicone Fluid 244, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 96.11, 97.00, 0.00, 108.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("DC Silicone Fluid 245","This is a multiline description for\nDC Silicone Fluid 245, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 14.41, 39.00, 4.00, 57.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("DC Silicone Fluid 345","This is a multiline description for\nDC Silicone Fluid 345, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 90.52, 53.00, 2.00, 67.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Silicone Fluid 350 CS","This is a multiline description for\nSilicone Fluid 350 CS, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 25.30, 118.00, 1.00, 13.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Silicone 50 C/S","This is a multiline description for\nSilicone 50 C/S, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 150.47, 320.00, 14.00, 28.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("DC Silicone 593 Fluid","This is a multiline description for\nDC Silicone 593 Fluid, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 81.39, 405.00, 3.00, 118.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Silicone Fluid GE 96/100","This is a multiline description for\nSilicone Fluid GE 96/100, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 8.31, 193.00, 0.00, 7.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Silicone Fluid GE 96/200","This is a multiline description for\nSilicone Fluid GE 96/200, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 100.59, 137.00, 6.00, 410.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Silicone Fluid GE 96/350","This is a multiline description for\nSilicone Fluid GE 96/350, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 41.97, 190.00, 5.00, 33.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Silicone Fluid GE 96/5 (Dimethicone)","This is a multiline description for\nSilicone Fluid GE 96/5 (Dimethicone), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 10.76, 35.00, 6.00, 70.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Silicone Fluid GE 96/50","This is a multiline description for\nSilicone Fluid GE 96/50, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 119.42, 22.00, 37.00, 128.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Silicone Fluid 1000 CS (DMF1000)","This is a multiline description for\nSilicone Fluid 1000 CS (DMF1000), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 155.54, 31.00, 9.00, 75.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Shea Butter Refined","This is a multiline description for\nShea Butter Refined, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 28.95, 196.00, 7.00, 80.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Silicone Antifoam FG-10","This is a multiline description for\nSilicone Antifoam FG-10, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 150.65, 36.00, 1.00, 158.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Silk Flo 366","This is a multiline description for\nSilk Flo 366, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 8.37, 38.00, 10.00, 78.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Simethicone Emulsion 30%","This is a multiline description for\nSimethicone Emulsion 30%, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 186.20, 116.00, 3.00, 15.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sodium Acid Pyrophosphate","This is a multiline description for\nSodium Acid Pyrophosphate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 77.88, 23.00, 2.00, 58.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sodium Alginate FCC","This is a multiline description for\nSodium Alginate FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 84.17, 19.00, 14.00, 1.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sodium Ascorbate USP","This is a multiline description for\nSodium Ascorbate USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 61.90, 372.00, 6.00, 9.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sodium Benzoate USP Powder","This is a multiline description for\nSodium Benzoate USP Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 34.10, 111.00, 4.00, 37.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sodium Bicarbonate USP / FCC Powder","This is a multiline description for\nSodium Bicarbonate USP / FCC Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 76.36, 62.00, 10.00, 14.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sodium Borate NF Powder","This is a multiline description for\nSodium Borate NF Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 130.04, 15.00, 13.00, 117.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sodium Chloride Reagent ACS","This is a multiline description for\nSodium Chloride Reagent ACS, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 75.62, 200.00, 1.00, 36.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sodium Chloride FCC","This is a multiline description for\nSodium Chloride FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 81.04, 181.00, 2.00, 158.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sodium Chloride USP Granular","This is a multiline description for\nSodium Chloride USP Granular, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 187.85, 257.00, 1.00, 44.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sodium Citrate USP Granular Dihydrate","This is a multiline description for\nSodium Citrate USP Granular Dihydrate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 3.44, 25.00, 2.00, 39.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sodium Fluoride USP Powder","This is a multiline description for\nSodium Fluoride USP Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 27.30, 15.00, 2.00, 36.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sodium Hexametaphosphate Food Grade Powder","This is a multiline description for\nSodium Hexametaphosphate Food Grade Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 141.83, 3.00, 4.00, 9.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sodium Hydroxide NF FCC Pellets","This is a multiline description for\nSodium Hydroxide NF FCC Pellets, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 85.27, 83.00, 1.00, 7.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sodium Lauryl Sulfate NF Needles","This is a multiline description for\nSodium Lauryl Sulfate NF Needles, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 120.69, 21.00, 7.00, 399.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sodium Lauryl Sulfate NF Powder","This is a multiline description for\nSodium Lauryl Sulfate NF Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 190.86, 0.00, 1.00, 44.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sodium Metabisulfate NF FCC Granular","This is a multiline description for\nSodium Metabisulfate NF FCC Granular, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 154.16, 168.00, 0.00, 123.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sodium Metasilicate 5H2O","This is a multiline description for\nSodium Metasilicate 5H2O, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 122.64, 62.00, 0.00, 99.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sodium Methyl Paraben","This is a multiline description for\nSodium Methyl Paraben, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 115.88, 74.00, 15.00, 125.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sodium Molybdate 2H2O","This is a multiline description for\nSodium Molybdate 2H2O, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 113.57, 57.00, 0.00, 32.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sodium Phosphate Mono USP Granular (Sodium Biphosphate)","This is a multiline description for\nSodium Phosphate Mono USP Granular (Sodium Biphosphate), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 80.44, 129.00, 2.00, 176.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Disodium Phosphate Anhydrous FCC Granular ","This is a multiline description for\nDisodium Phosphate Anhydrous FCC Granular , aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 26.86, 112.00, 11.00, 133.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sodium Propionate NF FCC","This is a multiline description for\nSodium Propionate NF FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 149.97, 140.00, 6.00, 121.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sodium Saccharin USP 40-80 Mesh","This is a multiline description for\nSodium Saccharin USP 40-80 Mesh, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 135.87, 1.00, 0.00, 59.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sodium Saccharin USP Powder","This is a multiline description for\nSodium Saccharin USP Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 150.09, 30.00, 0.00, 45.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sodium Sesquicarbonate FCC","This is a multiline description for\nSodium Sesquicarbonate FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 121.93, 32.00, 25.00, 20.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sodium Starch Glycolate NF","This is a multiline description for\nSodium Starch Glycolate NF, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 135.75, 377.00, 11.00, 28.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sodium Stearate NF","This is a multiline description for\nSodium Stearate NF, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 64.43, 328.00, 3.00, 128.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sodium Sulfate Anhydrous","This is a multiline description for\nSodium Sulfate Anhydrous, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 114.60, 277.00, 4.00, 122.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sodium Sulfate Anhydrous Granular USP","This is a multiline description for\nSodium Sulfate Anhydrous Granular USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 97.12, 76.00, 2.00, 26.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sodium Tri-Polyphosphate FCC Granular","This is a multiline description for\nSodium Tri-Polyphosphate FCC Granular, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 26.38, 22.00, 14.00, 166.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Soluvit-Richter (Henkel)","This is a multiline description for\nSoluvit-Richter (Henkel), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 34.87, 0.00, 1.00, 258.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sorbitol 70 % Solution USP","This is a multiline description for\nSorbitol 70 % Solution USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 44.19, 68.00, 1.00, 177.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sorbitol USP / FCC # 834","This is a multiline description for\nSorbitol USP / FCC # 834, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 130.45, 41.00, 0.00, 40.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sorbitol USP FCC Crystalline","This is a multiline description for\nSorbitol USP FCC Crystalline, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 46.72, 323.00, 4.00, 81.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sorbitol Crystalline NF FCC Granular","This is a multiline description for\nSorbitol Crystalline NF FCC Granular, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 170.49, 13.00, 1.00, 55.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sorbitol 70 % Solution Non- Crystallizing","This is a multiline description for\nSorbitol 70 % Solution Non- Crystallizing, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 198.57, 223.00, 0.00, 65.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sorbitol Special (SPI)","This is a multiline description for\nSorbitol Special (SPI), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 58.24, 107.00, 6.00, 55.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sorbic Acid USP Powder","This is a multiline description for\nSorbic Acid USP Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 148.45, 132.00, 1.00, 248.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sorbitan Mono Laurate","This is a multiline description for\nSorbitan Mono Laurate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 131.33, 85.00, 9.00, 210.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sorbitan Monostearate","This is a multiline description for\nSorbitan Monostearate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 183.52, 17.00, 6.00, 84.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sorbitan Mono-Oleate","This is a multiline description for\nSorbitan Mono-Oleate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 82.46, 162.00, 11.00, 103.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sorbitan Monopalmitate","This is a multiline description for\nSorbitan Monopalmitate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 130.49, 81.00, 1.00, 70.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sorbitan Sesquioleate","This is a multiline description for\nSorbitan Sesquioleate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 115.95, 82.00, 13.00, 85.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sorbitan Stearate","This is a multiline description for\nSorbitan Stearate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 62.98, 8.00, 0.00, 93.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sorbitan Palmitate","This is a multiline description for\nSorbitan Palmitate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 128.18, 180.00, 0.00, 48.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Soy Bean Oil #RB1 FCC","This is a multiline description for\nSoy Bean Oil #RB1 FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 83.67, 38.00, 1.00, 7.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Soy Bean Oil USP Refined","This is a multiline description for\nSoy Bean Oil USP Refined, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 151.89, 19.00, 15.00, 113.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Soy Lecithin Oil","This is a multiline description for\nSoy Lecithin Oil, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 54.19, 135.00, 1.00, 114.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Squalane NF (Synthetic)","This is a multiline description for\nSqualane NF (Synthetic), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 77.71, 19.00, 1.00, 4.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Stearic Acid T/P NF/FCC Flakes","This is a multiline description for\nStearic Acid T/P NF/FCC Flakes, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 125.26, 94.00, 3.00, 23.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Stearic Acid NF T/P Flakes (Emersol 132)","This is a multiline description for\nStearic Acid NF T/P Flakes (Emersol 132), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 2.54, 80.00, 7.00, 39.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Stearic Acid NF T/P Flakes (Hystrene 5016)","This is a multiline description for\nStearic Acid NF T/P Flakes (Hystrene 5016), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 107.88, 221.00, 13.00, 19.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Stearic Acid NF T/P Powder (Hystrene 5016)","This is a multiline description for\nStearic Acid NF T/P Powder (Hystrene 5016), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 118.91, 411.00, 4.00, 1.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Stearic Acid T/P NF Powder Vegetable Grade (Hystrene 5016)","This is a multiline description for\nStearic Acid T/P NF Powder Vegetable Grade (Hystrene 5016), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 45.30, 248.00, 0.00, 426.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Stearic Acid NF T/P Powder Kosher","This is a multiline description for\nStearic Acid NF T/P Powder Kosher, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 0.86, 5.00, 5.00, 32.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Standapol ES-2 (Henkel)","This is a multiline description for\nStandapol ES-2 (Henkel), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 140.22, 300.00, 15.00, 9.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cetearyl Alcohol & PEG 40 Hydrogenated Castor Oil & Stearalkonium ","This is a multiline description for\nCetearyl Alcohol & PEG 40 Hydrogenated Castor Oil & Stearalkonium , aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 11.55, 42.00, 0.00, 115.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Chloride (1002 Equivalent)","This is a multiline description for\nChloride (1002 Equivalent), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 163.73, 141.00, 11.00, 177.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Standapol ES-250 (Sodium Laureth Sulfate) ","This is a multiline description for\nStandapol ES-250 (Sodium Laureth Sulfate) , aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 30.57, 138.00, 0.00, 117.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Standapol A (Ammonium Lauryl Sulfate)","This is a multiline description for\nStandapol A (Ammonium Lauryl Sulfate), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 99.67, 12.00, 10.00, 9.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Standapol AP (Henkel)","This is a multiline description for\nStandapol AP (Henkel), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 11.11, 71.00, 0.00, 33.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Standapol EA3 (Ammonium Laureth Sulfate)","This is a multiline description for\nStandapol EA3 (Ammonium Laureth Sulfate), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 59.83, 26.00, 1.00, 36.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Standapol ES-1 (Sodium Laureth Sulfate)","This is a multiline description for\nStandapol ES-1 (Sodium Laureth Sulfate), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 46.81, 334.00, 4.00, 273.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Standapol ES-40 (Sodium Myreth Sulfate) ","This is a multiline description for\nStandapol ES-40 (Sodium Myreth Sulfate) , aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 168.18, 96.00, 0.00, 228.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Standapol T (TEA Lauryl Sulfate)","This is a multiline description for\nStandapol T (TEA Lauryl Sulfate), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 164.99, 21.00, 4.00, 103.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Standapol WAQ Special 28% Paste ","This is a multiline description for\nStandapol WAQ Special 28% Paste , aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 166.41, 80.00, 3.00, 419.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Standapol WAQ-LC (Henkel)","This is a multiline description for\nStandapol WAQ-LC (Henkel), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 28.29, 125.00, 5.00, 102.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Stannous Fluoride USP","This is a multiline description for\nStannous Fluoride USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 145.31, 99.00, 6.00, 152.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Steareth 2","This is a multiline description for\nSteareth 2, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 197.13, 227.00, 0.00, 33.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Steareth 20","This is a multiline description for\nSteareth 20, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 6.44, 50.00, 4.00, 30.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Steareth 21","This is a multiline description for\nSteareth 21, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 52.61, 22.00, 1.00, 10.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Stearyl Alcohol NF ","This is a multiline description for\nStearyl Alcohol NF , aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 140.81, 32.00, 0.00, 75.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Stearyl Stearate","This is a multiline description for\nStearyl Stearate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 76.31, 110.00, 0.00, 24.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Steol CS-460 (Sodium Laureth Sulfate)","This is a multiline description for\nSteol CS-460 (Sodium Laureth Sulfate), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 11.59, 130.00, 1.00, 22.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Stepan 653","This is a multiline description for\nStepan 653, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 56.28, 260.00, 9.00, 0.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Stepanol WA-100","This is a multiline description for\nStepanol WA-100, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 85.07, 106.00, 7.00, 52.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Stepanol WA Paste","This is a multiline description for\nStepanol WA Paste, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 82.10, 240.00, 5.00, 58.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Stepanol WAC","This is a multiline description for\nStepanol WAC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 37.25, 41.00, 6.00, 59.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Stepanol WAT","This is a multiline description for\nStepanol WAT, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 90.16, 301.00, 4.00, 16.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Stretch Wrap","This is a multiline description for\nStretch Wrap, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 27.26, 101.00, 5.00, 72.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Succinic Acid 25 PB","This is a multiline description for\nSuccinic Acid 25 PB, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 28.22, 242.00, 6.00, 22.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sucrose NF Fine Granular","This is a multiline description for\nSucrose NF Fine Granular, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 100.78, 332.00, 2.00, 153.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sunflower Seed Oil","This is a multiline description for\nSunflower Seed Oil, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 161.13, 33.00, 7.00, 26.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Syloid 63FP","This is a multiline description for\nSyloid 63FP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 161.92, 109.00, 5.00, 24.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("T-Det-09 (Octoxynol-9)","This is a multiline description for\nT-Det-09 (Octoxynol-9), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 87.47, 51.00, 5.00, 97.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Talc # 1745","This is a multiline description for\nTalc # 1745, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 124.33, 58.00, 0.00, 7.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Talc # 2755","This is a multiline description for\nTalc # 2755, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 56.36, 122.00, 1.00, 54.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Talc # 141(IMP1886L)","This is a multiline description for\nTalc # 141(IMP1886L), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 36.40, 302.00, 13.00, 39.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Tannic Acid USP FCC Powder","This is a multiline description for\nTannic Acid USP FCC Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 153.51, 137.00, 3.00, 31.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Tartaric Acid NF FCC Granular","This is a multiline description for\nTartaric Acid NF FCC Granular, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 119.50, 316.00, 0.00, 51.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Taurine USP","This is a multiline description for\nTaurine USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 31.65, 237.00, 5.00, 343.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Triethanolamine 85%","This is a multiline description for\nTriethanolamine 85%, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 86.64, 40.00, 1.00, 204.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Triethanolamine 99% NF (TEA 99)","This is a multiline description for\nTriethanolamine 99% NF (TEA 99), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 122.77, 237.00, 3.00, 26.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Tetracaine HCL USP","This is a multiline description for\nTetracaine HCL USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 44.45, 104.00, 1.00, 70.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Texapon ASV 50","This is a multiline description for\nTexapon ASV 50, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 38.65, 95.00, 12.00, 47.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Thiamine Hydrochloride USP (Vitamin B1 HCL)","This is a multiline description for\nThiamine Hydrochloride USP (Vitamin B1 HCL), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 89.30, 244.00, 5.00, 24.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Thioglycolic Acid 80%","This is a multiline description for\nThioglycolic Acid 80%, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 105.14, 31.00, 3.00, 13.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Thiourea Purified 99%","This is a multiline description for\nThiourea Purified 99%, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 28.21, 97.00, 3.00, 35.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Thymol NF Crystal","This is a multiline description for\nThymol NF Crystal, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 78.44, 21.00, 1.00, 174.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Titanium Dioxide USP 328 Oil Dispersible","This is a multiline description for\nTitanium Dioxide USP 328 Oil Dispersible, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 87.35, 75.00, 5.00, 5.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Titanium Dioxide # 3328 USP Water Dispersible ","This is a multiline description for\nTitanium Dioxide # 3328 USP Water Dispersible , aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 61.14, 26.00, 1.00, 75.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Tolnaftate USP","This is a multiline description for\nTolnaftate USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 151.44, 112.00, 0.00, 300.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Tragacanth Gum 3200-4000S NF FCC","This is a multiline description for\nTragacanth Gum 3200-4000S NF FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 25.91, 385.00, 1.00, 85.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Triacetin FCC/ USP Kosher","This is a multiline description for\nTriacetin FCC/ USP Kosher, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 162.49, 3.00, 7.00, 41.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Triacetin FCC USP","This is a multiline description for\nTriacetin FCC USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 48.90, 40.00, 3.00, 48.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Triclosan USP","This is a multiline description for\nTriclosan USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 163.56, 551.00, 5.00, 23.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Tridecyl Trimellitate","This is a multiline description for\nTridecyl Trimellitate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 29.69, 379.00, 0.00, 87.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Trisodium EDTA","This is a multiline description for\nTrisodium EDTA, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 81.41, 164.00, 5.00, 124.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Triton X-100","This is a multiline description for\nTriton X-100, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 184.30, 318.00, 1.00, 113.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Tween 20 (Unichema)","This is a multiline description for\nTween 20 (Unichema), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 103.25, 39.00, 4.00, 103.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Tween 40 (ICI)(polysorbate 40)","This is a multiline description for\nTween 40 (ICI)(polysorbate 40), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 45.40, 387.00, 1.00, 4.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Tween 60 NF (ICI) (Polysorbate 60)","This is a multiline description for\nTween 60 NF (ICI) (Polysorbate 60), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 195.76, 1.00, 2.00, 25.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Tween 80 NF(ICI - Uniqema)","This is a multiline description for\nTween 80 NF(ICI - Uniqema), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 57.49, 228.00, 4.00, 11.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Tyloxapol USP","This is a multiline description for\nTyloxapol USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 165.36, 43.00, 1.00, 369.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Undecylenic Acid USP","This is a multiline description for\nUndecylenic Acid USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 49.15, 53.00, 4.00, 105.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Urea Chemical Grade","This is a multiline description for\nUrea Chemical Grade, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 16.39, 27.00, 2.00, 0.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Urea USP","This is a multiline description for\nUrea USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 23.67, 116.00, 5.00, 100.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Vanadyl Sulfate","This is a multiline description for\nVanadyl Sulfate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 175.80, 46.00, 1.00, 11.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Vanillin NF/FCC","This is a multiline description for\nVanillin NF/FCC, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 53.21, 87.00, 0.00, 28.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Varisoft CTB-40 (Witco)","This is a multiline description for\nVarisoft CTB-40 (Witco), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 102.80, 92.00, 3.00, 161.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Varonic APM (PPG Myristyl Ether)","This is a multiline description for\nVaronic APM (PPG Myristyl Ether), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 72.39, 10.00, 11.00, 20.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Veegum F","This is a multiline description for\nVeegum F, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 74.17, 31.00, 0.00, 95.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Veegum K..100 Lb. Drum","This is a multiline description for\nVeegum K..100 Lb. Drum, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 114.50, 161.00, 1.00, 106.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Vitamin E Acetate USP Oil","This is a multiline description for\nVitamin E Acetate USP Oil, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 184.16, 125.00, 5.00, 3.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Vitamin E Linoleate","This is a multiline description for\nVitamin E Linoleate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 48.25, 51.00, 5.00, 12.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Vitamin K-1 USP (Phytonadione)","This is a multiline description for\nVitamin K-1 USP (Phytonadione), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 130.26, 384.00, 1.00, 249.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Wheat Germ Oil Cold Pressed","This is a multiline description for\nWheat Germ Oil Cold Pressed, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 91.23, 58.00, 0.00, 351.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Wheat Germ Oil ","This is a multiline description for\nWheat Germ Oil , aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 178.94, 124.00, 7.00, 29.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("White Fonoline USP (Witco)","This is a multiline description for\nWhite Fonoline USP (Witco), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 141.51, 141.00, 2.00, 8.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("White Petrolatum 1S USP(Witco)","This is a multiline description for\nWhite Petrolatum 1S USP(Witco), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 180.03, 117.00, 5.00, 63.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Witch Hazel Extract","This is a multiline description for\nWitch Hazel Extract, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 66.79, 417.00, 0.00, 29.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Zinc Acetate Dihydrate USP ACS Powder","This is a multiline description for\nZinc Acetate Dihydrate USP ACS Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 119.89, 192.00, 1.00, 183.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Zinc Citrate Trihydrate Purified","This is a multiline description for\nZinc Citrate Trihydrate Purified, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 25.01, 428.00, 16.00, 149.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Zinc Gluconate USP Granular","This is a multiline description for\nZinc Gluconate USP Granular, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 184.32, 25.00, 5.00, 47.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Zinc Gluconate USP Powder","This is a multiline description for\nZinc Gluconate USP Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 111.79, 242.00, 2.00, 60.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Zinc Oxide USP Powder","This is a multiline description for\nZinc Oxide USP Powder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 97.27, 15.00, 6.00, 21.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Zinc Phenolsulfonate Purified","This is a multiline description for\nZinc Phenolsulfonate Purified, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 159.55, 21.00, 1.00, 119.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Zinc Sulfate Heptahydrate, USP","This is a multiline description for\nZinc Sulfate Heptahydrate, USP, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 153.03, 427.00, 0.00, 10.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Zinc Sulfate USP Monohydrate","This is a multiline description for\nZinc Sulfate USP Monohydrate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'C', 186.36, 44.00, 2.00, 75.00);
