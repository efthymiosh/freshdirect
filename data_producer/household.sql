INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("air conditioner","This is a multiline description for\nair conditioner, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 286.66, 13.00, 3.00, 91.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("alarm clock","This is a multiline description for\nalarm clock, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 298.15, 162.00, 12.00, 6.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("answering machine","This is a multiline description for\nanswering machine, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 221.22, 93.00, 1.00, 12.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("BBQ grill","This is a multiline description for\nBBQ grill, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 125.97, 82.00, 4.00, 142.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("barbecue grill","This is a multiline description for\nbarbecue grill, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 179.85, 4.00, 0.00, 3.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("blender","This is a multiline description for\nblender, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 190.47, 30.00, 0.00, 57.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("blowdryer","This is a multiline description for\nblowdryer, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 296.10, 75.00, 0.00, 158.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("burglar alarm","This is a multiline description for\nburglar alarm, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 197.74, 183.00, 2.00, 62.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("calculator","This is a multiline description for\ncalculator, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 12.71, 26.00, 1.00, 2.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("camera","This is a multiline description for\ncamera, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 39.42, 36.00, 0.00, 134.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("can opener","This is a multiline description for\ncan opener, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 8.66, 69.00, 0.00, 39.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("CD player","This is a multiline description for\nCD player, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 59.83, 126.00, 1.00, 105.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("ceiling fan","This is a multiline description for\nceiling fan, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 250.80, 40.00, 5.00, 42.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("cell phone","This is a multiline description for\ncell phone, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 208.75, 358.00, 0.00, 75.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("clock","This is a multiline description for\nclock, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 174.57, 28.00, 2.00, 18.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("clothes dryer","This is a multiline description for\nclothes dryer, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 53.76, 17.00, 5.00, 16.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("clothes washer","This is a multiline description for\nclothes washer, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 137.65, 20.00, 0.00, 58.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("coffee grinder","This is a multiline description for\ncoffee grinder, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 172.32, 40.00, 1.00, 61.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("coffee maker","This is a multiline description for\ncoffee maker, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 285.24, 57.00, 0.00, 10.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("computer","This is a multiline description for\ncomputer, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 83.41, 40.00, 8.00, 7.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("convection oven","This is a multiline description for\nconvection oven, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 232.15, 134.00, 2.00, 41.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("copier","This is a multiline description for\ncopier, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 236.69, 83.00, 0.00, 30.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("crock pot","This is a multiline description for\ncrock pot, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 277.46, 113.00, 2.00, 7.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("curling iron","This is a multiline description for\ncurling iron, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 279.44, 52.00, 1.00, 32.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("dishwasher","This is a multiline description for\ndishwasher, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 206.91, 72.00, 11.00, 20.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("doorbell","This is a multiline description for\ndoorbell, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 289.82, 48.00, 6.00, 164.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("dryer","This is a multiline description for\ndryer, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 280.94, 208.00, 0.00, 15.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("edger","This is a multiline description for\nedger, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 91.61, 1.00, 1.00, 5.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("electric blanket","This is a multiline description for\nelectric blanket, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 245.57, 203.00, 2.00, 61.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("electric drill","This is a multiline description for\nelectric drill, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 164.92, 100.00, 11.00, 42.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("electric fan","This is a multiline description for\nelectric fan, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 133.80, 74.00, 2.00, 31.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("electric guitar","This is a multiline description for\nelectric guitar, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 17.37, 194.00, 4.00, 90.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("electric keyboard","This is a multiline description for\nelectric keyboard, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 146.30, 114.00, 0.00, 39.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("electric pencil sharpener","This is a multiline description for\nelectric pencil sharpener, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 56.38, 175.00, 1.00, 8.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("electric razor","This is a multiline description for\nelectric razor, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 239.50, 47.00, 1.00, 7.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("electric toothbrush","This is a multiline description for\nelectric toothbrush, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 136.24, 91.00, 0.00, 21.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("espresso maker","This is a multiline description for\nespresso maker, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 174.31, 109.00, 6.00, 9.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("fan","This is a multiline description for\nfan, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 129.98, 162.00, 2.00, 139.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("fax machine","This is a multiline description for\nfax machine, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 134.66, 16.00, 0.00, 46.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("fire alarm","This is a multiline description for\nfire alarm, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 26.48, 2.00, 0.00, 56.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("fire extinguisher","This is a multiline description for\nfire extinguisher, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 231.88, 25.00, 0.00, 213.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("fireplace","This is a multiline description for\nfireplace, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 43.52, 42.00, 0.00, 189.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("flashlight","This is a multiline description for\nflashlight, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 251.44, 45.00, 0.00, 22.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("flatscreen TV","This is a multiline description for\nflatscreen TV, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 27.51, 21.00, 2.00, 73.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("food processor","This is a multiline description for\nfood processor, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 288.56, 48.00, 4.00, 33.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("freezer","This is a multiline description for\nfreezer, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 91.12, 43.00, 0.00, 92.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("furnace","This is a multiline description for\nfurnace, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 177.84, 62.00, 0.00, 75.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("garage door","This is a multiline description for\ngarage door, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 166.73, 59.00, 0.00, 26.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("garbage disposal","This is a multiline description for\ngarbage disposal, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 233.95, 28.00, 1.00, 170.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("GPS","This is a multiline description for\nGPS, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 206.12, 38.00, 6.00, 49.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("grill","This is a multiline description for\ngrill, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 239.70, 204.00, 5.00, 146.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("hair clippers","This is a multiline description for\nhair clippers, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 43.62, 39.00, 2.00, 7.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("hair dryer","This is a multiline description for\nhair dryer, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 248.25, 37.00, 2.00, 9.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("headphones","This is a multiline description for\nheadphones, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 232.69, 124.00, 6.00, 30.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("heater","This is a multiline description for\nheater, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 92.41, 12.00, 0.00, 131.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("hood","This is a multiline description for\nhood, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 197.10, 30.00, 0.00, 37.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("hot plate","This is a multiline description for\nhot plate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 177.97, 54.00, 1.00, 21.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("humidifier","This is a multiline description for\nhumidifier, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 257.07, 21.00, 4.00, 70.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("ice cream maker","This is a multiline description for\nice cream maker, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 43.61, 80.00, 2.00, 22.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("iron","This is a multiline description for\niron, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 1.59, 9.00, 1.00, 118.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("juice","This is a multiline description for\njuice, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 143.04, 191.00, 9.00, 19.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("kerosene heater","This is a multiline description for\nkerosene heater, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 203.20, 156.00, 2.00, 278.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("lamp","This is a multiline description for\nlamp, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 297.34, 92.00, 5.00, 68.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("lantern","This is a multiline description for\nlantern, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 293.06, 3.00, 0.00, 55.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("laptop","This is a multiline description for\nlaptop, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 157.14, 49.00, 5.00, 31.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("lawn mower","This is a multiline description for\nlawn mower, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 203.00, 88.00, 0.00, 113.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("leaf blower","This is a multiline description for\nleaf blower, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 83.64, 72.00, 0.00, 22.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("light","This is a multiline description for\nlight, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 207.92, 59.00, 0.00, 12.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("microwave oven","This is a multiline description for\nmicrowave oven, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 123.42, 7.00, 1.00, 6.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("mixer","This is a multiline description for\nmixer, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 278.66, 3.00, 0.00, 166.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("mousetrap","This is a multiline description for\nmousetrap, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 214.05, 213.00, 2.00, 77.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("MP3 player","This is a multiline description for\nMP3 player, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 289.49, 87.00, 0.00, 44.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("oven","This is a multiline description for\noven, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 29.94, 9.00, 6.00, 98.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("percolator","This is a multiline description for\npercolator, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 230.00, 196.00, 2.00, 3.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("pressure cooker","This is a multiline description for\npressure cooker, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 146.74, 103.00, 1.00, 13.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("printer","This is a multiline description for\nprinter, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 66.69, 49.00, 1.00, 66.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("radio","This is a multiline description for\nradio, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 158.22, 123.00, 3.00, 40.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("range","This is a multiline description for\nrange, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 245.25, 132.00, 1.00, 49.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("record player","This is a multiline description for\nrecord player, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 215.92, 239.00, 0.00, 76.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("refrigerator","This is a multiline description for\nrefrigerator, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 179.25, 54.00, 0.00, 78.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("rotisserie","This is a multiline description for\nrotisserie, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 177.77, 3.00, 1.00, 29.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("scale","This is a multiline description for\nscale, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 284.69, 6.00, 1.00, 38.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("scanner","This is a multiline description for\nscanner, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 235.09, 109.00, 2.00, 0.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("sewing machine","This is a multiline description for\nsewing machine, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 54.18, 297.00, 3.00, 81.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("smoke detector","This is a multiline description for\nsmoke detector, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 224.01, 37.00, 0.00, 54.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("stapler","This is a multiline description for\nstapler, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 47.54, 64.00, 0.00, 138.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("stereo","This is a multiline description for\nstereo, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 145.47, 31.00, 2.00, 5.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("stove","This is a multiline description for\nstove, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 239.49, 110.00, 4.00, 32.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("telephone","This is a multiline description for\ntelephone, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 233.81, 24.00, 4.00, 86.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("television","This is a multiline description for\ntelevision, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 97.71, 13.00, 0.00, 7.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("timer","This is a multiline description for\ntimer, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 266.67, 128.00, 0.00, 4.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("toaster","This is a multiline description for\ntoaster, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 227.73, 116.00, 4.00, 46.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("toaster oven","This is a multiline description for\ntoaster oven, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 35.07, 235.00, 0.00, 21.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("torch","This is a multiline description for\ntorch, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 144.66, 14.00, 0.00, 225.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("trash compactor","This is a multiline description for\ntrash compactor, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 144.51, 104.00, 0.00, 21.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("trimmer","This is a multiline description for\ntrimmer, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 135.87, 146.00, 3.00, 17.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("vacuum cleaner","This is a multiline description for\nvacuum cleaner, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 24.77, 45.00, 9.00, 34.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("vaporizer","This is a multiline description for\nvaporizer, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 172.70, 11.00, 2.00, 41.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("VCR","This is a multiline description for\nVCR, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 274.12, 83.00, 2.00, 73.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("video camera","This is a multiline description for\nvideo camera, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 116.34, 30.00, 0.00, 46.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("video game machine","This is a multiline description for\nvideo game machine, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 75.97, 23.00, 4.00, 87.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("waffle iron","This is a multiline description for\nwaffle iron, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 124.13, 225.00, 3.00, 149.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("walkie-talkie","This is a multiline description for\nwalkie-talkie, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 195.05, 115.00, 0.00, 7.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("washing machine","This is a multiline description for\nwashing machine, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 174.35, 155.00, 1.00, 71.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("watch","This is a multiline description for\nwatch, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 80.18, 29.00, 0.00, 122.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("water heater","This is a multiline description for\nwater heater, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 126.88, 73.00, 1.00, 27.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("armchair","This is a multiline description for\narmchair, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 66.76, 152.00, 0.00, 62.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("armoire","This is a multiline description for\narmoire, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 45.91, 17.00, 0.00, 34.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("bar stool","This is a multiline description for\nbar stool, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 131.01, 187.00, 0.00, 125.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("bassinet","This is a multiline description for\nbassinet, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 248.65, 154.00, 4.00, 43.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("beach chair","This is a multiline description for\nbeach chair, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 178.87, 211.00, 2.00, 12.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("bean bag chair","This is a multiline description for\nbean bag chair, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 230.33, 207.00, 5.00, 2.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("bed","This is a multiline description for\nbed, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 64.01, 202.00, 6.00, 66.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("bed table","This is a multiline description for\nbed table, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 150.85, 17.00, 10.00, 85.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("bench","This is a multiline description for\nbench, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 8.07, 335.00, 1.00, 13.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("bentwood rocker","This is a multiline description for\nbentwood rocker, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 51.19, 49.00, 2.00, 61.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("bergere","This is a multiline description for\nbergere, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 103.78, 108.00, 1.00, 12.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("bookcase","This is a multiline description for\nbookcase, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 171.62, 18.00, 3.00, 12.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("bookshelf","This is a multiline description for\nbookshelf, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 37.55, 22.00, 1.00, 147.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("breakfront","This is a multiline description for\nbreakfront, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 46.00, 17.00, 7.00, 30.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("buffet","This is a multiline description for\nbuffet, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 30.03, 136.00, 0.00, 60.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("bunk bed","This is a multiline description for\nbunk bed, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 297.39, 309.00, 1.00, 1.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("bureau","This is a multiline description for\nbureau, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 287.57, 90.00, 0.00, 9.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("cabinet","This is a multiline description for\ncabinet, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 34.46, 127.00, 4.00, 40.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("canopy bed","This is a multiline description for\ncanopy bed, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 153.65, 17.00, 3.00, 5.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("captain's chair","This is a multiline description for\ncaptain's chair, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 119.42, 28.00, 0.00, 48.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("card table","This is a multiline description for\ncard table, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 157.59, 75.00, 1.00, 74.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("carpet","This is a multiline description for\ncarpet, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 235.57, 35.00, 0.00, 142.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("cart","This is a multiline description for\ncart, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 45.02, 4.00, 2.00, 10.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("chair","This is a multiline description for\nchair, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 7.34, 140.00, 0.00, 268.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("chaise lounge","This is a multiline description for\nchaise lounge, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 144.21, 36.00, 0.00, 59.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("chandelier","This is a multiline description for\nchandelier, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 42.61, 156.00, 0.00, 70.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("chest","This is a multiline description for\nchest, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 12.91, 97.00, 4.00, 38.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("chest of drawers","This is a multiline description for\nchest of drawers, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 294.41, 391.00, 10.00, 46.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("china cabinet","This is a multiline description for\nchina cabinet, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 176.80, 39.00, 0.00, 30.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("clothes valet","This is a multiline description for\nclothes valet, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 217.42, 21.00, 0.00, 137.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("club chair","This is a multiline description for\nclub chair, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 83.70, 114.00, 0.00, 23.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("coffee table","This is a multiline description for\ncoffee table, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 169.45, 16.00, 1.00, 3.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("console","This is a multiline description for\nconsole, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 160.03, 42.00, 3.00, 74.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("cot","This is a multiline description for\ncot, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 74.22, 99.00, 1.00, 22.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("couch","This is a multiline description for\ncouch, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 116.82, 5.00, 3.00, 30.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("cradle","This is a multiline description for\ncradle, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 17.75, 98.00, 6.00, 69.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("credenza","This is a multiline description for\ncredenza, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 22.95, 5.00, 0.00, 106.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("crib","This is a multiline description for\ncrib, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 87.98, 27.00, 3.00, 56.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("cubbies","This is a multiline description for\ncubbies, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 9.36, 97.00, 6.00, 59.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("cupboard","This is a multiline description for\ncupboard, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 260.35, 14.00, 3.00, 33.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("curio","This is a multiline description for\ncurio, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 168.28, 27.00, 0.00, 109.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("curtains","This is a multiline description for\ncurtains, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 294.02, 43.00, 0.00, 30.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("cushion","This is a multiline description for\ncushion, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 142.35, 147.00, 4.00, 50.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("deck chair","This is a multiline description for\ndeck chair, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 157.87, 79.00, 0.00, 60.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("desk","This is a multiline description for\ndesk, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 204.09, 19.00, 1.00, 239.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("desk chair","This is a multiline description for\ndesk chair, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 146.46, 6.00, 2.00, 48.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("dining room table","This is a multiline description for\ndining room table, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 231.61, 45.00, 0.00, 67.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("director's chair","This is a multiline description for\ndirector's chair, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 175.17, 117.00, 0.00, 10.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("display cabinet","This is a multiline description for\ndisplay cabinet, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 11.87, 10.00, 9.00, 1.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("divan","This is a multiline description for\ndivan, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 157.26, 4.00, 1.00, 437.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("drapery","This is a multiline description for\ndrapery, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 264.67, 11.00, 2.00, 34.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("drapes","This is a multiline description for\ndrapes, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 224.11, 104.00, 0.00, 36.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("dresser","This is a multiline description for\ndresser, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 275.91, 44.00, 1.00, 34.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("easel","This is a multiline description for\neasel, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 148.50, 243.00, 7.00, 20.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("easy chair","This is a multiline description for\neasy chair, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 110.53, 3.00, 7.00, 200.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("end table","This is a multiline description for\nend table, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 239.88, 245.00, 0.00, 120.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("fateuil","This is a multiline description for\nfateuil, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 19.75, 4.00, 5.00, 58.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("file cabinet","This is a multiline description for\nfile cabinet, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 30.68, 23.00, 2.00, 275.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("folding chair","This is a multiline description for\nfolding chair, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 113.20, 10.00, 1.00, 8.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("folding screen","This is a multiline description for\nfolding screen, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 250.66, 42.00, 2.00, 92.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("footrest","This is a multiline description for\nfootrest, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 245.78, 253.00, 0.00, 24.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("footstool","This is a multiline description for\nfootstool, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 269.83, 7.00, 2.00, 20.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("four-poster bed","This is a multiline description for\nfour-poster bed, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 39.92, 55.00, 0.00, 178.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("furnishings","This is a multiline description for\nfurnishings, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 130.86, 31.00, 4.00, 46.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("furniture","This is a multiline description for\nfurniture, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 110.31, 82.00, 0.00, 41.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("futon","This is a multiline description for\nfuton, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 263.58, 94.00, 1.00, 19.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("game table","This is a multiline description for\ngame table, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 240.53, 156.00, 1.00, 66.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("garden bench","This is a multiline description for\ngarden bench, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 75.49, 223.00, 1.00, 4.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("gateleg table","This is a multiline description for\ngateleg table, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 265.38, 67.00, 1.00, 101.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("glider rocker","This is a multiline description for\nglider rocker, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 193.82, 281.00, 0.00, 99.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("grandfather clock","This is a multiline description for\ngrandfather clock, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 136.39, 26.00, 3.00, 146.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("hall tree","This is a multiline description for\nhall tree, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 176.84, 54.00, 1.00, 207.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("hammock","This is a multiline description for\nhammock, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 34.31, 55.00, 1.00, 327.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("hassock","This is a multiline description for\nhassock, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 49.68, 177.00, 0.00, 225.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("hat stand","This is a multiline description for\nhat stand, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 218.75, 82.00, 2.00, 260.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("headboard","This is a multiline description for\nheadboard, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 168.35, 487.00, 0.00, 38.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("highchair","This is a multiline description for\nhighchair, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 165.86, 47.00, 0.00, 14.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("hope chest","This is a multiline description for\nhope chest, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 120.99, 27.00, 11.00, 128.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("hutch","This is a multiline description for\nhutch, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 166.13, 97.00, 4.00, 10.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("island","This is a multiline description for\nisland, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 52.10, 12.00, 0.00, 22.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("kitchen island","This is a multiline description for\nkitchen island, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 187.94, 62.00, 0.00, 103.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("ladderback chair","This is a multiline description for\nladderback chair, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 122.79, 62.00, 4.00, 9.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("lamp","This is a multiline description for\nlamp, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 47.74, 37.00, 0.00, 84.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("lawn chair","This is a multiline description for\nlawn chair, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 108.37, 15.00, 0.00, 161.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("lift chair","This is a multiline description for\nlift chair, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 182.37, 41.00, 0.00, 0.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("light","This is a multiline description for\nlight, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 203.24, 7.00, 6.00, 17.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("lintel","This is a multiline description for\nlintel, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 234.33, 79.00, 1.00, 204.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("lounge chair","This is a multiline description for\nlounge chair, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 227.84, 53.00, 0.00, 32.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("lounger","This is a multiline description for\nlounger, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 0.75, 61.00, 0.00, 35.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("love seat","This is a multiline description for\nlove seat, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 16.14, 229.00, 2.00, 15.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("mantle","This is a multiline description for\nmantle, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 90.42, 91.00, 8.00, 71.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("mattress","This is a multiline description for\nmattress, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 252.18, 8.00, 2.00, 39.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("mirror","This is a multiline description for\nmirror, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 148.52, 97.00, 1.00, 12.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Murphy bed","This is a multiline description for\nMurphy bed, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 234.27, 33.00, 1.00, 54.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("nightstand","This is a multiline description for\nnightstand, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 284.01, 124.00, 2.00, 80.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("office chair","This is a multiline description for\noffice chair, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 75.92, 118.00, 0.00, 19.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("ottoman","This is a multiline description for\nottoman, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 85.02, 26.00, 3.00, 27.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("pantry","This is a multiline description for\npantry, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 62.24, 20.00, 2.00, 46.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("park bench","This is a multiline description for\npark bench, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 266.16, 54.00, 0.00, 85.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("patio chair","This is a multiline description for\npatio chair, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 174.78, 114.00, 0.00, 5.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("patio table","This is a multiline description for\npatio table, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 131.41, 83.00, 0.00, 16.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("pew","This is a multiline description for\npew, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 289.42, 88.00, 4.00, 159.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("piano bench","This is a multiline description for\npiano bench, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 129.63, 29.00, 2.00, 76.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("picnic table","This is a multiline description for\npicnic table, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 17.15, 102.00, 8.00, 27.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("pillow","This is a multiline description for\npillow, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 293.94, 77.00, 3.00, 13.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("porch swing","This is a multiline description for\nporch swing, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 247.72, 21.00, 3.00, 82.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("rack","This is a multiline description for\nrack, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 213.36, 167.00, 1.00, 22.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("recliner","This is a multiline description for\nrecliner, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 184.76, 79.00, 1.00, 3.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("rocking chair","This is a multiline description for\nrocking chair, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 75.87, 80.00, 0.00, 16.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("room divider","This is a multiline description for\nroom divider, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 82.65, 4.00, 0.00, 81.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("rug","This is a multiline description for\nrug, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 105.41, 113.00, 1.00, 34.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("sconce","This is a multiline description for\nsconce, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 136.90, 33.00, 4.00, 37.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("screen","This is a multiline description for\nscreen, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 282.47, 46.00, 2.00, 96.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("seat","This is a multiline description for\nseat, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 196.42, 31.00, 1.00, 30.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("secretary","This is a multiline description for\nsecretary, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 56.13, 210.00, 3.00, 107.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("sectional sofa","This is a multiline description for\nsectional sofa, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 161.36, 2.00, 5.00, 103.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("settee","This is a multiline description for\nsettee, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 195.83, 11.00, 0.00, 173.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("shelf","This is a multiline description for\nshelf, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 55.03, 12.00, 3.00, 50.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("shoji screen","This is a multiline description for\nshoji screen, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 265.23, 2.00, 8.00, 27.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("sideboard","This is a multiline description for\nsideboard, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 108.63, 97.00, 4.00, 23.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("sleeper sofa","This is a multiline description for\nsleeper sofa, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 35.80, 42.00, 0.00, 98.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("sofa","This is a multiline description for\nsofa, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 215.83, 66.00, 0.00, 156.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("sofabed","This is a multiline description for\nsofabed, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 141.61, 133.00, 3.00, 0.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("stool","This is a multiline description for\nstool, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 249.25, 69.00, 4.00, 43.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("table","This is a multiline description for\ntable, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 208.84, 137.00, 1.00, 139.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("tansu","This is a multiline description for\ntansu, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 283.95, 4.00, 1.00, 117.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("tea cart","This is a multiline description for\ntea cart, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 22.91, 36.00, 0.00, 38.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("throne","This is a multiline description for\nthrone, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 15.06, 244.00, 4.00, 12.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("throw rug","This is a multiline description for\nthrow rug, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 104.26, 139.00, 8.00, 4.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("trundle bed","This is a multiline description for\ntrundle bed, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 109.26, 7.00, 10.00, 119.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("tuffet","This is a multiline description for\ntuffet, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 264.96, 95.00, 5.00, 82.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("valet","This is a multiline description for\nvalet, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 74.82, 86.00, 0.00, 192.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("vanity","This is a multiline description for\nvanity, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 136.76, 78.00, 5.00, 33.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Venetian blinds","This is a multiline description for\nVenetian blinds, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 197.17, 22.00, 0.00, 87.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("vitrine","This is a multiline description for\nvitrine, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 179.17, 42.00, 0.00, 68.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("wardrobe","This is a multiline description for\nwardrobe, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 105.48, 203.00, 5.00, 79.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("waste basket","This is a multiline description for\nwaste basket, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 218.19, 118.00, 4.00, 6.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("waterbed","This is a multiline description for\nwaterbed, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 253.20, 111.00, 7.00, 85.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("window shades","This is a multiline description for\nwindow shades, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 126.78, 123.00, 1.00, 73.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Windsor chair","This is a multiline description for\nWindsor chair, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 126.18, 84.00, 2.00, 138.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("wing chair","This is a multiline description for\nwing chair, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 74.49, 48.00, 0.00, 155.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("work table","This is a multiline description for\nwork table, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 204.65, 59.00, 3.00, 21.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("writing desk","This is a multiline description for\nwriting desk, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'H', 274.74, 138.00, 7.00, 20.00);
