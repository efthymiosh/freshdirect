INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Aarts","This is a multiline description for\nAarts, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 85.73, 105.00, 4.00, 21.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Amasi","This is a multiline description for\nAmasi, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 7.15, 109.00, 2.00, 406.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Ayran","This is a multiline description for\nAyran, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 71.13, 41.00, 2.00, 240.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Baked milk","This is a multiline description for\nBaked milk, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 51.24, 37.00, 4.00, 840.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Basundi","This is a multiline description for\nBasundi, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 10.87, 300.00, 1.00, 17.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Bhuna khoya","This is a multiline description for\nBhuna khoya, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 5.55, 498.00, 6.00, 1062.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Blaand","This is a multiline description for\nBlaand, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 28.50, 277.00, 8.00, 351.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Black Kashk","This is a multiline description for\nBlack Kashk, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 1.22, 311.00, 1.00, 407.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Central Asia","This is a multiline description for\nCentral Asia, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 5.60, 173.00, 34.00, 27.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Boozan yogurt","This is a multiline description for\nBoozan yogurt, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 85.36, 41.00, 56.00, 166.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Butterfat","This is a multiline description for\nButterfat, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 30.37, 58.00, 3.00, 338.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Buttermilk","This is a multiline description for\nButtermilk, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 34.70, 261.00, 16.00, 121.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Buttermilk","This is a multiline description for\nButtermilk, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 34.83, 829.00, 7.00, 103.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cacık","This is a multiline description for\nCacık, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 82.79, 10.00, 5.00, 23.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Camel milk","This is a multiline description for\nCamel milk, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 71.21, 909.00, 28.00, 257.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Caudle","This is a multiline description for\nCaudle, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 68.64, 18.00, 9.00, 271.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Chaas","This is a multiline description for\nChaas, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 57.01, 206.00, 20.00, 110.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Chal","This is a multiline description for\nChal, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 71.51, 503.00, 44.00, 3.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Chalap","This is a multiline description for\nChalap, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 7.36, 72.00, 18.00, 109.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Chass","This is a multiline description for\nChass, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 90.07, 252.00, 20.00, 57.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cheese","This is a multiline description for\nCheese, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 59.99, 133.00, 10.00, 111.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Clotted cream","This is a multiline description for\nClotted cream, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 45.58, 13.00, 7.00, 36.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Condensed milk","This is a multiline description for\nCondensed milk, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 2.53, 164.00, 22.00, 248.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cottage cheese","This is a multiline description for\nCottage cheese, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 55.16, 166.00, 6.00, 25.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cream","This is a multiline description for\nCream, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 4.88, 97.00, 7.00, 288.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cream cheese","This is a multiline description for\nCream cheese, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 19.03, 190.00, 15.00, 61.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Crème anglaise","This is a multiline description for\nCrème anglaise, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 78.63, 240.00, 11.00, 232.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Crème fraîche","This is a multiline description for\nCrème fraîche, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 25.38, 205.00, 27.00, 384.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cuajada","This is a multiline description for\nCuajada, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 97.53, 222.00, 3.00, 112.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Curd","This is a multiline description for\nCurd, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 64.03, 431.00, 9.00, 219.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Curd snack","This is a multiline description for\nCurd snack, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 30.39, 168.00, 4.00, 386.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Custard","This is a multiline description for\nCustard, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 97.70, 445.00, 37.00, 5.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Dadiah","This is a multiline description for\nDadiah, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 15.54, 184.00, 15.00, 84.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Daigo (dairy product)","This is a multiline description for\nDaigo (dairy product), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 5.64, 212.00, 9.00, 369.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Dondurma","This is a multiline description for\nDondurma, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 19.03, 87.00, 7.00, 117.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Donkey milk","This is a multiline description for\nDonkey milk, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 85.49, 156.00, 3.00, 354.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Doogh","This is a multiline description for\nDoogh, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 5.43, 216.00, 3.00, 237.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Evaporated milk","This is a multiline description for\nEvaporated milk, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 40.18, 491.00, 4.00, 244.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Filled milk","This is a multiline description for\nFilled milk, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 35.13, 334.00, 8.00, 79.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Filmjölk","This is a multiline description for\nFilmjölk, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 56.91, 193.00, 4.00, 257.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Feta","This is a multiline description for\nFeta, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 6.07, 38.00, 0.00, 24.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Fromage frais","This is a multiline description for\nFromage frais, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 71.65, 284.00, 32.00, 474.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Frozen custard","This is a multiline description for\nFrozen custard, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 97.37, 168.00, 4.00, 352.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Frozen yogurt","This is a multiline description for\nFrozen yogurt, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 95.53, 261.00, 10.00, 1.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("United States","This is a multiline description for\nUnited States, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 22.83, 631.00, 12.00, 198.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Galalith","This is a multiline description for\nGalalith, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 78.04, 16.00, 4.00, 28.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Gelato","This is a multiline description for\nGelato, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 80.21, 568.00, 6.00, 92.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Goat's milk","This is a multiline description for\nGoat's milk, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 43.52, 420.00, 1.00, 136.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Gombe (dish)","This is a multiline description for\nGombe (dish), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 95.82, 45.00, 9.00, 224.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Gomme (food)","This is a multiline description for\nGomme (food), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 84.54, 212.00, 8.00, 341.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Horse","This is a multiline description for\nHorse, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 6.26, 673.00, 10.00, 3.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Ice cream","This is a multiline description for\nIce cream, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 58.12, 91.00, 0.00, 331.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Ice milk","This is a multiline description for\nIce milk, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 22.68, 328.00, 5.00, 1.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Infant formula","This is a multiline description for\nInfant formula, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 79.61, 373.00, 1.00, 182.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Junket (dessert)","This is a multiline description for\nJunket (dessert), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 67.23, 228.00, 34.00, 22.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Kashk","This is a multiline description for\nKashk, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 41.32, 16.00, 9.00, 92.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Kaymak","This is a multiline description for\nKaymak, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 23.06, 60.00, 0.00, 15.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Kefir","This is a multiline description for\nKefir, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 78.94, 246.00, 22.00, 382.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Khoa","This is a multiline description for\nKhoa, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 56.01, 195.00, 20.00, 30.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Kulfi","This is a multiline description for\nKulfi, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 84.95, 568.00, 30.00, 15.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Kumis","This is a multiline description for\nKumis, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 62.88, 123.00, 0.00, 110.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Lassi","This is a multiline description for\nLassi, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 16.56, 62.00, 0.00, 363.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Leben (milk product)","This is a multiline description for\nLeben (milk product), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 82.13, 1216.00, 29.00, 53.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Malai","This is a multiline description for\nMalai, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 5.72, 198.00, 9.00, 67.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Matzoon (yogurt)","This is a multiline description for\nMatzoon (yogurt), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 76.52, 263.00, 4.00, 161.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Milk","This is a multiline description for\nMilk, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 27.97, 74.00, 5.00, 366.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Milk skin","This is a multiline description for\nMilk skin, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 80.17, 80.00, 39.00, 210.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Míša","This is a multiline description for\nMíša, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 17.36, 671.00, 13.00, 324.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Mitha Dahi","This is a multiline description for\nMitha Dahi, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 32.20, 416.00, 2.00, 133.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Moose milk","This is a multiline description for\nMoose milk, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 3.35, 72.00, 3.00, 385.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Mursik","This is a multiline description for\nMursik, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 24.27, 78.00, 0.00, 71.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Paneer","This is a multiline description for\nPaneer, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 81.97, 297.00, 8.00, 175.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Powdered milk","This is a multiline description for\nPowdered milk, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 34.72, 755.00, 1.00, 197.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Processed cheese","This is a multiline description for\nProcessed cheese, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 21.01, 197.00, 6.00, 2.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Pytia","This is a multiline description for\nPytia, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 82.26, 148.00, 10.00, 1119.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Qimiq","This is a multiline description for\nQimiq, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 37.96, 123.00, 16.00, 241.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Quark (dairy product)","This is a multiline description for\nQuark (dairy product), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 20.16, 6.00, 2.00, 53.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Qatiq","This is a multiline description for\nQatiq, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 27.65, 180.00, 0.00, 146.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Ryazhenka","This is a multiline description for\nRyazhenka, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 78.55, 49.00, 17.00, 185.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Baked milk","This is a multiline description for\nBaked milk, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 87.44, 130.00, 2.00, 50.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Semifreddo","This is a multiline description for\nSemifreddo, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 35.81, 100.00, 18.00, 167.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sergem","This is a multiline description for\nSergem, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 87.60, 94.00, 8.00, 239.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sheep milk","This is a multiline description for\nSheep milk, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 76.33, 69.00, 11.00, 30.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Shrikhand","This is a multiline description for\nShrikhand, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 63.36, 170.00, 18.00, 360.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Skorup","This is a multiline description for\nSkorup, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 51.98, 8.00, 0.00, 318.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Skyr","This is a multiline description for\nSkyr, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 54.81, 43.00, 3.00, 81.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Smântână","This is a multiline description for\nSmântână, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 33.95, 36.00, 27.00, 48.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Smetana (dairy product)","This is a multiline description for\nSmetana (dairy product), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 54.15, 34.00, 5.00, 459.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("So (dairy product)","This is a multiline description for\nSo (dairy product), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 45.87, 83.00, 6.00, 81.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Soft serve","This is a multiline description for\nSoft serve, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 59.95, 12.00, 0.00, 90.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Sour cream","This is a multiline description for\nSour cream, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 29.10, 325.00, 7.00, 516.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Soured milk","This is a multiline description for\nSoured milk, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 22.19, 107.00, 14.00, 270.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Spaghettieis","This is a multiline description for\nSpaghettieis, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 38.34, 443.00, 7.00, 536.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Stewler","This is a multiline description for\nStewler, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 99.04, 480.00, 3.00, 80.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Strained yogurt","This is a multiline description for\nStrained yogurt, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 98.85, 56.00, 7.00, 151.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Urdă","This is a multiline description for\nUrdă, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 33.27, 967.00, 5.00, 313.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Uunijuusto","This is a multiline description for\nUunijuusto, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 26.32, 569.00, 1.00, 397.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Vaccenic acid","This is a multiline description for\nVaccenic acid, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 94.56, 43.00, 2.00, 53.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Viili","This is a multiline description for\nViili, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 88.74, 328.00, 1.00, 23.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Vla","This is a multiline description for\nVla, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 8.09, 634.00, 19.00, 124.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Whey","This is a multiline description for\nWhey, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 75.10, 123.00, 5.00, 270.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Whey protein","This is a multiline description for\nWhey protein, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 24.27, 270.00, 0.00, 52.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Whipped cream","This is a multiline description for\nWhipped cream, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 86.46, 238.00, 14.00, 43.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Yak butter","This is a multiline description for\nYak butter, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 59.41, 52.00, 44.00, 389.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Pack yak","This is a multiline description for\nPack yak, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 8.85, 30.00, 12.00, 6.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Yakult","This is a multiline description for\nYakult, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 27.38, 726.00, 3.00, 35.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Ymer (dairy product)","This is a multiline description for\nYmer (dairy product), aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 67.64, 141.00, 3.00, 283.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Yogurt","This is a multiline description for\nYogurt, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 35.26, 32.00, 2.00, 21.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("bacon","This is a multiline description for\nbacon, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 72.87, 264.00, 3.00, 36.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("beef","This is a multiline description for\nbeef, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 54.99, 33.00, 8.00, 266.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("chicken","This is a multiline description for\nchicken, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 16.25, 102.00, 3.00, 139.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("chicken breast","This is a multiline description for\nchicken breast, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 23.16, 207.00, 17.00, 223.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("chicken leg","This is a multiline description for\nchicken leg, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 7.41, 98.00, 2.00, 99.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("chicken wing","This is a multiline description for\nchicken wing, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 14.97, 419.00, 10.00, 809.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("ham","This is a multiline description for\nham, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 64.04, 6.00, 3.00, 491.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("hamburger","This is a multiline description for\nhamburger, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 27.75, 346.00, 0.00, 1.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("heart","This is a multiline description for\nheart, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 40.31, 119.00, 1.00, 611.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("hot dog","This is a multiline description for\nhot dog, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 50.71, 346.00, 11.00, 221.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("kidney","This is a multiline description for\nkidney, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 8.57, 1045.00, 1.00, 76.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("lamb chop","This is a multiline description for\nlamb chop, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 54.92, 0.00, 45.00, 195.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("mutton","This is a multiline description for\nmutton, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 32.72, 59.00, 0.00, 129.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("pork","This is a multiline description for\npork, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 2.44, 268.00, 6.00, 218.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("roast","This is a multiline description for\nroast, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 68.52, 331.00, 8.00, 658.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("sausage","This is a multiline description for\nsausage, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 48.46, 272.00, 34.00, 437.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("steak","This is a multiline description for\nsteak, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 4.43, 38.00, 18.00, 160.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("tongue","This is a multiline description for\ntongue, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 89.06, 188.00, 2.00, 748.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("turkey","This is a multiline description for\nturkey, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 51.50, 124.00, 3.00, 13.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("veal","This is a multiline description for\nveal, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 39.36, 83.00, 11.00, 255.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("veniso","This is a multiline description for\nveniso, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'M', 49.45, 303.00, 2.00, 146.00);
