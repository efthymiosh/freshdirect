#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>

#define PRINT_HELP(PROGNAME) printf( "Execute as:\n%1$s -p {C,G,V,H,B,I} {Price level} < product_names.txt\n%1$s -s {Amount of insertions to produce} {Amount of Products in database} {Decimal number in [0..1] signifying the probability of a supplier to provide a specific product} < supplier_names.txt\n%1$s -u {Amount of insertions to produce} {Highest Product Code} {Real number in [0..1] signifying the probability of a user to have bought a specific product} < user_names.txt\n", PROGNAME);

double expon_distr(int mean) {
  double temp;
  temp = (double) rand() / RAND_MAX;
  temp = -(log(1 - temp))*mean;
  return (int) temp;
}

double uniform_distr(int max) {
    return rand() / (double)RAND_MAX * max;
}

int main(int argc, char *argv[]) {
    char buf[256] = {0};
    int i;
    srand(time(NULL));
    if (argc != 4) {
        PRINT_HELP(argv[0]);
        return EXIT_FAILURE;
    }
    if (argv[1][0] == '-' && argv[1][2] == '\0') {
        switch (argv[1][1]) {
            case 'p':
                lvl = atoi(argv[3]);
                for (i = 0; fgets(buf, 256, stdin) != NULL; i++) {
                    double price, qty_on_hand, procur_qty, procur_level;
                    int lvl;
                    buf[44] = '\0';
                    price = uniform_distr(100 *lvl);
                    qty_on_hand = expon_distr(250 / lvl);
                    procur_level = expon_distr(10 / lvl);
                    procur_qty = expon_distr(200 / lvl);
                    printf("INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)\n"
                        "values (\"%1$s\",\"This is a multiline description for\\n%1$s, aiming to provide some kind of useful\\ninformation about it, for the end user.\\n\""
                        ", \'%2$c\', %3$.2f, %4$.2f, %5$.2f, %6$.2f);\n",
                        buf, argv[2][0], price, qty_on_hand, procur_level, procur_qty);
                }
                fprintf(stderr, "Generated %d MYSQL insertion queries\n", i);
                return EXIT_SUCCESS;
            case 's':
                /*supplier details*/
                for (i = 0; fgets(buf, 256, stdin) != NULL; i++) {
                    buf[44] = '\0';
                    price = uniform_distr(100 * lvl);
                    qty_on_hand = expon_distr(250 / lvl);
                    procur_level = expon_distr(10 / lvl);
                    procur_qty = expon_distr(200 / lvl);
                    printf("INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)\n"
                        "values (\"%1$s\",\"This is a multiline description for\\n%1$s, aiming to provide some kind of useful\\ninformation about it, for the end user.\\n\""
                        ", \'%2$c\', %3$.2f, %4$.2f, %5$.2f, %6$.2f);\n",
                        buf, argv[2][0], price, qty_on_hand, procur_level, procur_qty);
                }
                fprintf(stderr, "Generated %d MYSQL insertion queries\n", i);
                return EXIT_SUCCESS;
            case '
    }
    PRINT_HELP(argv[0]);
    return EXIT_FAILURE;
}
