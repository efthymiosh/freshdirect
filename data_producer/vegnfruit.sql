INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("acorn squash","This is a multiline description for\nacorn squash, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 62.35, 134.00, 2.00, 212.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("alfalfa","This is a multiline description for\nalfalfa, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 67.61, 378.00, 12.00, 943.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("artichoke","This is a multiline description for\nartichoke, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 85.16, 116.00, 10.00, 486.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("arugula","This is a multiline description for\narugula, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 44.80, 46.00, 0.00, 173.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("asparagus","This is a multiline description for\nasparagus, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 23.42, 470.00, 5.00, 315.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("avocado","This is a multiline description for\navocado, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 59.11, 127.00, 2.00, 143.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("bamboo shoots","This is a multiline description for\nbamboo shoots, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 8.81, 289.00, 28.00, 80.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("basil","This is a multiline description for\nbasil, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 1.48, 49.00, 26.00, 203.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("beans","This is a multiline description for\nbeans, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 59.59, 44.00, 3.00, 63.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("beets","This is a multiline description for\nbeets, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 94.25, 2855.00, 3.00, 316.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("bell pepper","This is a multiline description for\nbell pepper, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 37.13, 569.00, 12.00, 342.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("black-eyed peas","This is a multiline description for\nblack-eyed peas, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 6.66, 388.00, 5.00, 71.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("broccoli","This is a multiline description for\nbroccoli, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 63.60, 432.00, 0.00, 51.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Brussels sprouts","This is a multiline description for\nBrussels sprouts, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 22.23, 85.00, 13.00, 74.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("capers","This is a multiline description for\ncapers, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 97.39, 288.00, 10.00, 897.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("carrot","This is a multiline description for\ncarrot, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 86.58, 211.00, 9.00, 123.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("cauliflower","This is a multiline description for\ncauliflower, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 73.44, 630.00, 13.00, 225.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("celeriac","This is a multiline description for\nceleriac, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 91.98, 1437.00, 6.00, 68.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("celery","This is a multiline description for\ncelery, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 89.44, 48.00, 1.00, 648.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("chard","This is a multiline description for\nchard, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 96.56, 178.00, 3.00, 184.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("chick peas","This is a multiline description for\nchick peas, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 33.32, 110.00, 17.00, 162.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Chinese cabbage","This is a multiline description for\nChinese cabbage, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 64.46, 210.00, 20.00, 192.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("chives","This is a multiline description for\nchives, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 25.39, 176.00, 9.00, 25.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("collard greens","This is a multiline description for\ncollard greens, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 7.86, 66.00, 8.00, 335.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("cress","This is a multiline description for\ncress, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 15.42, 94.00, 6.00, 15.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("cucumber","This is a multiline description for\ncucumber, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 31.19, 809.00, 4.00, 46.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("daikon","This is a multiline description for\ndaikon, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 13.82, 161.00, 1.00, 21.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("dandelion greens","This is a multiline description for\ndandelion greens, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 98.57, 140.00, 12.00, 76.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("eggplant","This is a multiline description for\neggplant, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 78.54, 190.00, 20.00, 112.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("endive","This is a multiline description for\nendive, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 10.29, 336.00, 0.00, 88.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("fava bean","This is a multiline description for\nfava bean, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 24.73, 266.00, 6.00, 78.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("fennel","This is a multiline description for\nfennel, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 89.02, 14.00, 1.00, 9.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("garlic","This is a multiline description for\ngarlic, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 37.30, 247.00, 1.00, 230.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("ginger","This is a multiline description for\nginger, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 58.94, 165.00, 22.00, 260.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("gourd","This is a multiline description for\ngourd, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 95.90, 15.00, 17.00, 579.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("greenbean","This is a multiline description for\ngreenbean, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 48.77, 192.00, 3.00, 63.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("greens","This is a multiline description for\ngreens, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 7.06, 37.00, 12.00, 38.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("hot chile peppers","This is a multiline description for\nhot chile peppers, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 87.82, 348.00, 7.00, 26.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("iceberg lettuce","This is a multiline description for\niceberg lettuce, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 40.74, 1.00, 6.00, 70.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("jicama","This is a multiline description for\njicama, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 6.47, 222.00, 4.00, 115.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("kale","This is a multiline description for\nkale, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 21.87, 154.00, 1.00, 330.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("kohlrabi","This is a multiline description for\nkohlrabi, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 94.42, 3.00, 7.00, 466.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("leek","This is a multiline description for\nleek, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 7.25, 114.00, 18.00, 164.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("lentils","This is a multiline description for\nlentils, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 90.38, 29.00, 17.00, 733.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("lettuce","This is a multiline description for\nlettuce, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 24.96, 192.00, 1.00, 27.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Lima bean","This is a multiline description for\nLima bean, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 28.78, 283.00, 2.00, 237.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("maize","This is a multiline description for\nmaize, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 68.51, 304.00, 49.00, 277.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("mung bean","This is a multiline description for\nmung bean, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 29.47, 101.00, 2.00, 144.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("mushroom","This is a multiline description for\nmushroom, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 79.55, 92.00, 3.00, 269.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("mustard greens","This is a multiline description for\nmustard greens, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 32.40, 486.00, 10.00, 101.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("okra","This is a multiline description for\nokra, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 22.46, 168.00, 31.00, 27.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("olive","This is a multiline description for\nolive, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 60.23, 390.00, 1.00, 382.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("onion","This is a multiline description for\nonion, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 32.61, 72.00, 39.00, 190.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("parsley","This is a multiline description for\nparsley, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 92.91, 66.00, 3.00, 190.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("parsnip","This is a multiline description for\nparsnip, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 93.77, 89.00, 4.00, 52.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("pattypan squash","This is a multiline description for\npattypan squash, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 63.67, 200.00, 13.00, 113.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("pea","This is a multiline description for\npea, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 86.16, 17.00, 1.00, 41.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("peapod","This is a multiline description for\npeapod, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 92.49, 421.00, 8.00, 32.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("peanut","This is a multiline description for\npeanut, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 30.53, 193.00, 3.00, 476.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("peppers","This is a multiline description for\npeppers, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 32.89, 119.00, 14.00, 212.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("pickle","This is a multiline description for\npickle, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 63.14, 336.00, 3.00, 164.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("potato","This is a multiline description for\npotato, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 97.26, 215.00, 1.00, 482.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("pumpkin","This is a multiline description for\npumpkin, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 87.98, 193.00, 1.00, 145.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("radicchio","This is a multiline description for\nradicchio, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 9.02, 548.00, 29.00, 606.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("radish","This is a multiline description for\nradish, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 95.59, 32.00, 1.00, 425.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("rhubarb","This is a multiline description for\nrhubarb, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 93.51, 318.00, 0.00, 55.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("rocket","This is a multiline description for\nrocket, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 25.87, 92.00, 1.00, 177.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("romaine","This is a multiline description for\nromaine, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 68.86, 595.00, 2.00, 77.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("rutabaga","This is a multiline description for\nrutabaga, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 64.73, 179.00, 21.00, 193.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("salad","This is a multiline description for\nsalad, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 8.94, 14.00, 7.00, 695.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("salsa","This is a multiline description for\nsalsa, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 59.37, 279.00, 6.00, 230.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("scallion","This is a multiline description for\nscallion, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 56.11, 142.00, 10.00, 145.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("seaweed","This is a multiline description for\nseaweed, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 55.48, 370.00, 5.00, 134.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("shallot","This is a multiline description for\nshallot, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 49.29, 139.00, 13.00, 278.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("sorrel","This is a multiline description for\nsorrel, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 73.59, 527.00, 4.00, 110.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("soybean","This is a multiline description for\nsoybean, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 78.64, 217.00, 13.00, 113.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("spinach","This is a multiline description for\nspinach, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 9.32, 245.00, 0.00, 40.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("sprouts","This is a multiline description for\nsprouts, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 67.99, 219.00, 1.00, 63.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("S cont.","This is a multiline description for\nS cont., aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 25.63, 253.00, 31.00, 340.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("salsify","This is a multiline description for\nsalsify, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 7.18, 224.00, 4.00, 197.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("spuds","This is a multiline description for\nspuds, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 36.63, 329.00, 1.00, 392.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("squash","This is a multiline description for\nsquash, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 16.00, 469.00, 9.00, 452.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("string bean","This is a multiline description for\nstring bean, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 72.56, 748.00, 3.00, 143.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("succotash","This is a multiline description for\nsuccotash, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 53.17, 16.00, 29.00, 196.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("sweet potato","This is a multiline description for\nsweet potato, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 68.96, 1761.00, 16.00, 92.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Swiss chard","This is a multiline description for\nSwiss chard, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 58.27, 800.00, 10.00, 365.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("taro","This is a multiline description for\ntaro, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 59.68, 229.00, 10.00, 220.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("tomatillo","This is a multiline description for\ntomatillo, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 19.38, 1170.00, 3.00, 164.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("tomato","This is a multiline description for\ntomato, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 72.29, 132.00, 5.00, 428.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("tuber","This is a multiline description for\ntuber, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 25.90, 7.00, 15.00, 835.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("turnip","This is a multiline description for\nturnip, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 98.00, 26.00, 6.00, 143.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("vegetable","This is a multiline description for\nvegetable, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 16.39, 145.00, 1.00, 384.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("wasabi","This is a multiline description for\nwasabi, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 44.14, 721.00, 2.00, 4.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("water chestnut","This is a multiline description for\nwater chestnut, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 90.36, 503.00, 19.00, 138.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("watercress","This is a multiline description for\nwatercress, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 46.69, 183.00, 1.00, 216.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("yam","This is a multiline description for\nyam, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 51.02, 156.00, 2.00, 53.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("zucchini","This is a multiline description for\nzucchini, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 87.64, 255.00, 1.00, 29.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Apple","This is a multiline description for\nApple, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 70.38, 195.00, 2.00, 86.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Apricot","This is a multiline description for\nApricot, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 51.23, 164.00, 16.00, 114.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Avocado","This is a multiline description for\nAvocado, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 89.97, 59.00, 2.00, 4.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Banana","This is a multiline description for\nBanana, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 45.34, 49.00, 6.00, 36.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Breadfruit","This is a multiline description for\nBreadfruit, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 54.23, 972.00, 18.00, 103.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Bilberry","This is a multiline description for\nBilberry, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 80.19, 140.00, 2.00, 247.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Blackberry","This is a multiline description for\nBlackberry, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 86.88, 156.00, 7.00, 62.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Blackcurrant","This is a multiline description for\nBlackcurrant, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 42.45, 400.00, 0.00, 27.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Blueberry","This is a multiline description for\nBlueberry, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 34.10, 97.00, 6.00, 383.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Boysenberry","This is a multiline description for\nBoysenberry, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 80.46, 86.00, 3.00, 243.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cantaloupe","This is a multiline description for\nCantaloupe, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 50.35, 179.00, 13.00, 629.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Currant","This is a multiline description for\nCurrant, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 69.29, 60.00, 1.00, 53.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cherry","This is a multiline description for\nCherry, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 19.56, 869.00, 10.00, 1202.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cherimoya","This is a multiline description for\nCherimoya, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 39.81, 472.00, 12.00, 62.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cloudberry","This is a multiline description for\nCloudberry, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 31.48, 65.00, 7.00, 268.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Coconut","This is a multiline description for\nCoconut, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 3.06, 230.00, 20.00, 92.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cranberry","This is a multiline description for\nCranberry, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 92.53, 106.00, 2.00, 261.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cucumber","This is a multiline description for\nCucumber, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 63.89, 180.00, 5.00, 30.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Damson","This is a multiline description for\nDamson, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 2.75, 44.00, 1.00, 254.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Date","This is a multiline description for\nDate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 37.77, 63.00, 31.00, 170.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Dragonfruit","This is a multiline description for\nDragonfruit, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 19.33, 225.00, 8.00, 178.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Durian","This is a multiline description for\nDurian, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 44.29, 81.00, 19.00, 283.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Eggplant","This is a multiline description for\nEggplant, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 50.97, 124.00, 6.00, 155.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Elderberry","This is a multiline description for\nElderberry, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 99.55, 113.00, 24.00, 507.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Feijoa","This is a multiline description for\nFeijoa, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 71.15, 36.00, 10.00, 86.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Fig","This is a multiline description for\nFig, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 65.12, 22.00, 6.00, 227.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Goji berry","This is a multiline description for\nGoji berry, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 24.64, 224.00, 5.00, 195.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Gooseberry","This is a multiline description for\nGooseberry, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 81.65, 109.00, 2.00, 1.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Grape","This is a multiline description for\nGrape, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 94.85, 365.00, 9.00, 99.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Raisin","This is a multiline description for\nRaisin, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 4.61, 153.00, 1.00, 162.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Grapefruit","This is a multiline description for\nGrapefruit, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 85.21, 259.00, 1.00, 376.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Guava","This is a multiline description for\nGuava, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 1.03, 2.00, 14.00, 255.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Huckleberry","This is a multiline description for\nHuckleberry, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 14.47, 135.00, 0.00, 317.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Honeydew","This is a multiline description for\nHoneydew, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 50.37, 208.00, 6.00, 277.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Jackfruit","This is a multiline description for\nJackfruit, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 15.76, 516.00, 4.00, 731.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Jambul","This is a multiline description for\nJambul, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 22.82, 211.00, 41.00, 38.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Jujube","This is a multiline description for\nJujube, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 33.97, 219.00, 8.00, 97.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Kiwi fruit","This is a multiline description for\nKiwi fruit, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 4.44, 315.00, 28.00, 453.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Kumquat","This is a multiline description for\nKumquat, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 36.29, 9.00, 13.00, 93.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Lemon","This is a multiline description for\nLemon, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 4.58, 179.00, 0.00, 42.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Lime","This is a multiline description for\nLime, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 93.13, 45.00, 42.00, 114.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Loquat","This is a multiline description for\nLoquat, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 73.27, 154.00, 2.00, 442.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Lychee","This is a multiline description for\nLychee, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 33.45, 204.00, 19.00, 165.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Mango","This is a multiline description for\nMango, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 13.05, 471.00, 13.00, 127.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Marion berry","This is a multiline description for\nMarion berry, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 43.35, 91.00, 19.00, 129.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Melon","This is a multiline description for\nMelon, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 2.43, 399.00, 4.00, 97.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Cantaloupe","This is a multiline description for\nCantaloupe, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 83.53, 31.00, 14.00, 425.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Honeydew","This is a multiline description for\nHoneydew, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 63.09, 483.00, 0.00, 165.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Watermelon","This is a multiline description for\nWatermelon, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 2.29, 14.00, 58.00, 281.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Rock melon","This is a multiline description for\nRock melon, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 51.90, 50.00, 10.00, 384.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Miracle fruit","This is a multiline description for\nMiracle fruit, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 74.12, 178.00, 5.00, 410.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Mulberry","This is a multiline description for\nMulberry, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 35.88, 42.00, 4.00, 314.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Nectarine","This is a multiline description for\nNectarine, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 46.25, 55.00, 3.00, 133.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Nut","This is a multiline description for\nNut, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 99.54, 258.00, 20.00, 355.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Olive","This is a multiline description for\nOlive, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 76.31, 251.00, 12.00, 100.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Orange","This is a multiline description for\nOrange, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 48.99, 382.00, 31.00, 143.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Mandarine","This is a multiline description for\nMandarine, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 84.15, 765.00, 3.00, 89.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Blood Orange","This is a multiline description for\nBlood Orange, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 13.54, 614.00, 2.00, 418.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Tangerine","This is a multiline description for\nTangerine, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 42.49, 248.00, 13.00, 306.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Papaya","This is a multiline description for\nPapaya, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 78.53, 23.00, 8.00, 56.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Passionfruit","This is a multiline description for\nPassionfruit, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 28.79, 468.00, 13.00, 66.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Peach","This is a multiline description for\nPeach, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 49.09, 234.00, 1.00, 58.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Pepper","This is a multiline description for\nPepper, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 24.30, 436.00, 10.00, 264.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Chili pepper","This is a multiline description for\nChili pepper, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 60.93, 231.00, 2.00, 119.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Bell pepper","This is a multiline description for\nBell pepper, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 55.73, 180.00, 16.00, 235.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Pear","This is a multiline description for\nPear, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 42.87, 6.00, 8.00, 384.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Williams pear or Bartlett pear","This is a multiline description for\nWilliams pear or Bartlett pear, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 65.53, 95.00, 10.00, 116.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Persimmon","This is a multiline description for\nPersimmon, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 40.77, 60.00, 11.00, 237.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Physalis","This is a multiline description for\nPhysalis, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 5.96, 137.00, 38.00, 159.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Plum/prune","This is a multiline description for\nPlum/prune, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 3.17, 24.00, 16.00, 64.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Pineapple","This is a multiline description for\nPineapple, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 91.88, 150.00, 0.00, 150.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Pomegranate","This is a multiline description for\nPomegranate, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 5.64, 73.00, 38.00, 190.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Pomelo","This is a multiline description for\nPomelo, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 76.76, 390.00, 3.00, 43.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Purple Mangosteen","This is a multiline description for\nPurple Mangosteen, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 81.57, 521.00, 0.00, 127.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Quince","This is a multiline description for\nQuince, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 19.33, 290.00, 24.00, 183.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Raspberry","This is a multiline description for\nRaspberry, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 90.07, 229.00, 3.00, 645.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Rambutan","This is a multiline description for\nRambutan, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 2.29, 80.00, 7.00, 11.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Redcurrant","This is a multiline description for\nRedcurrant, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 36.85, 94.00, 3.00, 67.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Salal berry","This is a multiline description for\nSalal berry, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 76.77, 102.00, 16.00, 347.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Salmon berry","This is a multiline description for\nSalmon berry, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 59.03, 395.00, 5.00, 88.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Satsuma","This is a multiline description for\nSatsuma, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 58.50, 340.00, 8.00, 102.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Star fruit","This is a multiline description for\nStar fruit, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 62.00, 231.00, 20.00, 335.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Strawberry","This is a multiline description for\nStrawberry, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 29.16, 382.00, 5.00, 42.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Tamarillo","This is a multiline description for\nTamarillo, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 38.30, 310.00, 1.00, 104.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Tomato","This is a multiline description for\nTomato, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 98.67, 272.00, 6.00, 87.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Ugli fruit","This is a multiline description for\nUgli fruit, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 97.87, 390.00, 10.00, 274.00);
INSERT INTO products (name, description, prod_group, list_price, qty_on_hand, procur_level, procur_qty)
values ("Watermelo","This is a multiline description for\nWatermelo, aiming to provide some kind of useful\ninformation about it, for the end user.\n", 'V', 12.68, 153.00, 8.00, 252.00);
