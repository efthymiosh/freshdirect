package beanlayer;

import dbcommlayer.DBComm;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import model.Product;

@ManagedBean(name = "basketBean")
@SessionScoped
public class Basket implements Serializable {

    List<Product> inBasket;
    private int first;
    private String creditCard;
    String errorMessage;
    private double total_cost;
    private boolean credit;

    final int rows = 10;
    DataModel<Product> modelB;

    @ManagedProperty("#{suggestionsBean}")
    Suggestions suggestions;

    public Basket() {
        inBasket = new ArrayList();
    }

    public void addToBasket(Product prod, double amount) {
        prod.setInBasketAmt(amount);
        for (Product product : inBasket)
            if (prod.getProd_code() == product.getProd_code())
                return;
        inBasket.add(0, prod);
    }

    public List<Product> getInBasket() {
        return inBasket;
    }

    public void setInBasket(List<Product> inBasket) {
        this.inBasket = inBasket;
    }

    public void emptyBasket() {
        inBasket.clear();
    }

    public boolean isEmpty() {
        return inBasket.isEmpty();
    }

    /* Paged Products logic */
    public void pageFirst() {
        setFirst(0);
    }

    public void pagePrevious() {
        setFirst(getFirst() - rows);
    }

    public void pageNext() {
        setFirst(getFirst() + rows);
    }

    public void pageLast() {
        int count = modelB.getRowCount();
        setFirst(count - ((count % rows != 0) ? count % rows : rows));
    }

    public int getCurrentPage() {
        int count = modelB.getRowCount();
        return (count / rows) - ((count - first) / rows) + 1;
    }

    public int getTotalPages() {
        return modelB.getRowCount() / rows;
    }

    public int getFirst() {
        return first;
    }

    public void setFirst(int first) {
        this.first = first;
    }

    public int getRows() {
        return rows;
    }

    public DataModel<Product> getModelBasket() {
        modelB = new ListDataModel<>(inBasket);
        return modelB;
    }

    public int getModelBasketRowCount() {
        return (modelB == null) ? 0 : modelB.getRowCount();
    }

    @ManagedProperty(value = "#{userBean}")
    private UserSession userBean;

    public String checkout() {
        try {
            creditCard = DBComm.getInstance().getCreditCard(userBean.getCust_no());
        } catch (SQLException ex) {
            creditCard = null;
            credit = true;
            errorMessage = "No credit card given!";
            return "";
        }
        if (creditCard == null) {
            credit = true;
        } else {
            credit = true;
        }
        errorMessage="";
        return "checkout?faces-redirect=true";
    }

    public int getRowNumber() {
        return modelB.getRowIndex();
    }

    public UserSession getUserBean() {
        return userBean;
    }

    public void setUserBean(UserSession userBean) {
        this.userBean = userBean;
    }

    public String getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(String creditCard) {
        this.creditCard = creditCard;
    }

    public String trytobuy() {
        int retInt = 0;
        if (creditCard.equals("") || creditCard == null) {
            errorMessage = "Please fill in your Credit Card Number!!";
            return "";
        }
        DBComm db = DBComm.getInstance();
        suggestions.buildSuggestions(inBasket, userBean.getCust_no());
        try {
            retInt = db.checkout(creditCard, userBean.getCust_no(), inBasket);
            if (retInt != 1) {
                errorMessage = "Insufficient quantity for product: " + retInt;
                return "";
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            errorMessage = "Unable to access database..";
            return "";
        }
        if (!buy()) {
            return "";
        }
        inBasket.clear();
        return "thank_you?faces-redirect=true";
    }

    public boolean buy() {
        int retBool;
        DBComm db = DBComm.getInstance();
        try {
            if (!db.finalCheckout(creditCard, userBean.getCust_no())) {
                retBool = 0;
                errorMessage = "Error!";
                return retBool == 1;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            errorMessage = "Unable to access database..";
            return false;
        }
        return true;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public double getTotal_cost() {
        Iterator<Product> it = inBasket.iterator();
        total_cost = 0.0D;
        while (it.hasNext()) {
            total_cost = total_cost + (it.next().getTotal_sum());
        }
        return total_cost;
    }

    public void setTotal_cost(double total_cost) {
        this.total_cost = total_cost;
    }

    public boolean isCredit() {
        return credit;
    }

    public void setCredit(boolean credit) {
        this.credit = credit;
    }

    public Suggestions getSuggestions() {
        return suggestions;
    }

    public void setSuggestions(Suggestions suggestions) {
        this.suggestions = suggestions;
    }

}
