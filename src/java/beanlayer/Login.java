package beanlayer;

import dbcommlayer.DBComm;
import java.io.Serializable;
import java.sql.SQLException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import model.User;

@ManagedBean(name = "loginBean")
@SessionScoped
public class Login implements Serializable {

    private String username;
    private String password;
    private String errorLabel;
    
    @ManagedProperty(value="#{userBean}")
	private UserSession userBean;

    public Login() {       
    }

    public String login() {
        DBComm db = DBComm.getInstance();
        try {
            User user = db.verify(username, password);
            if (user != null) {
                errorLabel = "";
                userBean.setData(user);
                return "profile.xhtml?faces-redirect=true";
            }
            else
                errorLabel = "Invalid username/password";
        } catch (SQLException ex) {
            ex.printStackTrace();
            errorLabel = "Unable to retrieve data from database";
            return "index.xhtml?faces-redirect=true";
        }
        return "index.xhtml?faces-redirect=true";
    }

    public String logout() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "index.xhtml?faces-redirect=true";
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getErrorLabel() {
        return errorLabel;
    }

    public void setErrorLabel(String errorLabel) {
        this.errorLabel = errorLabel;
    }

    public UserSession getUserBean() {
        return userBean;
    }

    public void setUserBean(UserSession userBean) {
        this.userBean = userBean;
    }
    
    
}
