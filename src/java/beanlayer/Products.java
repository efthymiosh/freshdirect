package beanlayer;

import dbcommlayer.DBComm;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import model.Product;

@ManagedBean(name = "productsBean")
@SessionScoped
public class Products implements Serializable {

    String errorMessage;
    private int first;
    final int rows = 20;
    Product prod;
    Product posProd;
    String remQuantity;
    int prod_code;
    int updateSup;
    String name;
    String description;
    char prod_group;
    double list_price;
    double qty_on_hand;
    double procur_level;
    double procur_qty;
    double order_qty;
    double quantity;
    String supplier;

    String errormessage;
    String searchName;
    String searchSupplier;
    String searchDescription;
    String searchCategory;
    Integer searchOrder;
    Integer searchM;

    boolean searchFilter; //enabled / disabled
    private int searchPage;
    private List<Product> listP;
    private transient DataModel<Product> modelP;

    HashMap<String, Integer> suppliers;

    public Products() {
        first = 0;
        errorMessage = "";
        posProd = null;
//        posGraph = null;
        searchFilter = false;
        searchPage = 0;
        supplier = "";
        try {
            suppliers = DBComm.getInstance().getSuppliers();
            listP = (List<Product>) DBComm.getInstance().browse();
        } catch (SQLException ex) {
            ex.printStackTrace();
            listP = new ArrayList<>();
            suppliers = new HashMap<>();
        }
    }

    public void pageFirst() {
        setFirst(0);
    }

    public void pagePrevious() {
        setFirst(getFirst() - rows);
    }

    public void pageNext() {
        setFirst(getFirst() + rows);
    }

    public void pageLast() {
        int count = modelP.getRowCount();
        setFirst(count - ((count % rows != 0) ? count % rows : rows));
    }

    public int getCurrentPage() {
        int count = modelP.getRowCount();
        return (count / rows) - ((count - first) / rows) + 1;
    }

    public int getTotalPages() {
        return modelP.getRowCount() / rows;
    }

    public int getFirst() {
        return first;
    }

    public void setFirst(int first) {
        this.first = first;
    }

    public int getRows() {
        return rows;
    }

    public void setSelectedProduct(ValueChangeEvent event) {
        posProd = modelP.getRowData();
    }

    public String refresh() {
        return "product_list";
    }

    private void copyData() {
        prod_code = posProd.getProd_code();
        name = posProd.getName();
        description = posProd.getDescription();
        prod_group = posProd.getProd_group();
        list_price = posProd.getList_price();
        qty_on_hand = posProd.getQty_on_hand();
        procur_level = posProd.getProcur_level();
        procur_qty = posProd.getProcur_qty();
        quantity = posProd.getQuantity();
    }

    public String viewProduct() {
        if (posProd == null) {
            return "";
        }
        copyData();
        return "proddetails?faces-redirect=true";
    }

    @ManagedProperty(value = "#{basketBean}")
    Basket basket;

    public String addToBasket() {
        if (posProd != null) {
            basket.addToBasket(posProd, 1.0D);
        }
        return "";
    }

    public String editProduct() {
        if (posProd == null) {
            return "product_list?faces-redirect=true";
        }
        copyData();
        prod = posProd;
        return "proddetails";
    }

    public DataModel<Product> getModelProduct() {
        modelP = new ListDataModel<>(listP);
        return modelP;
    }

    public int getModelProductRowCount() {
        return (modelP == null) ? 0 : modelP.getRowCount();
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String initNewRecord() {
        prod_code = 0;
        name = description = "";
        prod_group = ' ';
        list_price = 0.0D;
        qty_on_hand = 0;
        procur_level = 0;
        procur_qty = 0;
        quantity = 0.0D;
        return "";
    }

    public String cancel() {
        prod = null;
        errorMessage = "";
        return "";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getProd_code() {
        return prod_code;
    }

    public void setProd_code(int prod_code) {
        this.prod_code = prod_code;
    }

    public char getProd_group() {
        return prod_group;
    }

    public void setProd_group(char prod_group) {
        this.prod_group = prod_group;
    }

    public double getList_price() {
        return list_price;
    }

    public void setList_price(double list_price) {
        this.list_price = list_price;
    }

    public double getQty_on_hand() {
        return qty_on_hand;
    }

    public void setQty_on_hand(double qty_on_hand) {
        this.qty_on_hand = qty_on_hand;
    }

    public double getProcur_level() {
        return procur_level;
    }

    public void setProcur_level(double procur_level) {
        this.procur_level = procur_level;
    }

    public double getProcur_qty() {
        return procur_qty;
    }

    public void setProcur_qty(double procur_qty) {
        this.procur_qty = procur_qty;
    }

    public boolean isSearchFilter() {
        return searchFilter;
    }

    public void setSearchFilter(boolean searchFilter) {
        this.searchFilter = searchFilter;
    }

    public int getSearchPage() {
        return searchPage;
    }

    public void setSearchPage(int searchPage) {
        this.searchPage = searchPage;
    }

    public int getRowNumber() {
        return modelP.getRowIndex();
    }

    public Product getPosProd() {
        return posProd;
    }

    public void setPosProd(Product posProd) {
        this.posProd = posProd;
    }

    public String search() throws SQLException {
        DBComm db = DBComm.getInstance();
        if (searchSupplier == null) {
            searchSupplier = "";
        }
        if (searchCategory == null) {
            searchCategory = "";
        }
        listP = (List<Product>) db.searchFilters(searchSupplier, searchName, searchDescription, searchCategory, searchOrder);
        modelP = new ListDataModel<>(listP);
        return "";
    }

    public String order_quantity()  {
        if ( order_qty > 0.0D)
        {
            try {
                DBComm.getInstance().order_qty(order_qty, prod_code, suppliers.get(supplier));
                order_qty = 0.0D;
                listP = DBComm.getInstance().browse();
                modelP = new ListDataModel<>(listP);
                return "product_list?faces-redirect=true";
            } catch (SQLException ex) {
                ex.printStackTrace();
                errorMessage = "Unable to query database";
                return "product_list?faces-redirect=true";
            }
        }
        else 
        {
            errorMessage = "Please insert a valid amount for addition";
            return "product_list?faces-redirect=true";
        }
    }

    public String removeFilters() throws SQLException {
        searchSupplier = searchName = searchDescription = "";
        searchFilter = false;
        listP = (List<Product>) DBComm.getInstance().browse();
        return "";
    }

    public void addProduct() {
        if (name.isEmpty() || description.isEmpty()) {
            errorMessage = "Didn't insert...ERROR! ";
            return;
        }
        if (!DBComm.getInstance().addProduct(name, description, Character.toString(prod_group), list_price, qty_on_hand, procur_level, procur_qty, suppliers.get(supplier))) {
            errorMessage = "Didn't insert...ERROR..";
        }
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }

    public String getSearchDescription() {
        return searchDescription;
    }

    public void setSearchDescription(String searchDescription) {
        this.searchDescription = searchDescription;
    }

    public String getRemQuantity() {
        return remQuantity;
    }

    public void setRemQuantity(String remQuantity) {
        this.remQuantity = remQuantity;
    }

    public String getSearchCategory() {
        return searchCategory;
    }

    public void setSearchCategory(String searchCategory) {
        this.searchCategory = searchCategory;
    }

    public Integer getSearchOrder() {
        return searchOrder;
    }

    public void setSearchOrder(Integer searchOrder) {
        this.searchOrder = searchOrder;
    }

    public Integer getSearchM() {
        return searchM;
    }

    public void setSearchM(Integer searchM) {
        this.searchM = searchM;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public Basket getBasket() {
        return basket;
    }

    public void setBasket(Basket basket) {
        this.basket = basket;
    }

    public double getOrder_qty() {
        return order_qty;
    }

    public void setOrder_qty(double order_qty) {
        this.order_qty = order_qty;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public Set<String> getSupplierNames() {
        return suppliers.keySet();
    }

    public String getSearchSupplier() {
        return searchSupplier;
    }

    public void setSearchSupplier(String searchSupplier) {
        this.searchSupplier = searchSupplier;
    }

    public HashMap<String, Integer> getSupplierMappings() {
        return suppliers;
    }

}
