package beanlayer;

import dbcommlayer.DBComm;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import model.Order;
import model.Product;
import model.Supplier;
import model.User;

@ManagedBean(name = "queryBean")
@ViewScoped
public class Querying implements Serializable {

    String errorMessage;
    private int first;
    final int rows = 5;

    Product prod;
    Supplier sup;
    User usr;
    Order ord;

    int queryno;

    //edit mode
    Product posProd;
    String remQuantity;
    int prod_code;
    String name;
    String description;
    char prod_group;
    double list_price;
    double qty_on_hand;
    double procur_level;
    double procur_qty;
    double quantity;

    String searchName;

    int searchM;
    int searchY;
    int searchAm;
    int limitM;

    boolean searchFilter; //enabled / disabled
    private int searchPage;

    private List<Product> listP;
    private transient DataModel<Product> modelP;

    private List<User> listU;
    private transient DataModel<User> modelU;

    private List<Supplier> listS;
    private transient DataModel<Supplier> modelS;

    private List<Order> listO;
    private transient DataModel<Order> modelO;

    @ManagedProperty(value = "#{productsBean}")
    private Products productsBean;

    String degreesSupplier1;
    String degreesSupplier2;
    String degreesResult;

    public Querying() {
        limitM = 5;
        searchM = 1;
        searchY = 2014;
        searchAm = 5;
        first = 0;
        queryno = 0;
        errorMessage = "";
        posProd = null;
        searchFilter = false;
        searchPage = 0;
        listP = new ArrayList<>();
        listO = new ArrayList<>();
        listU = new ArrayList<>();
        listS = new ArrayList<>();
    }

    public void pageFirst() {
        setFirst(0);
    }

    public void pagePrevious() {
        setFirst(getFirst() - rows);
    }

    public void pageNext() {
        setFirst(getFirst() + rows);
    }

    public void pageLastP() {
        int count = modelP.getRowCount();
        setFirst(count - ((count % rows != 0) ? count % rows : rows));
    }

    public int getCurrentPageP() {
        int count = modelP.getRowCount();
        return (count / rows) - ((count - first) / rows) + 1;
    }

    public void pageLastO() {
        int count = modelO.getRowCount();
        setFirst(count - ((count % rows != 0) ? count % rows : rows));
    }

    public int getCurrentPageO() {
        int count = modelO.getRowCount();
        return (count / rows) - ((count - first) / rows) + 1;
    }

    public void pageLastU() {
        int count = modelU.getRowCount();
        setFirst(count - ((count % rows != 0) ? count % rows : rows));
    }

    public int getCurrentPageU() {
        int count = modelU.getRowCount();
        return (count / rows) - ((count - first) / rows) + 1;
    }

    public void pageLastS() {
        int count = modelS.getRowCount();
        setFirst(count - ((count % rows != 0) ? count % rows : rows));
    }

    public int getCurrentPageS() {
        int count = modelS.getRowCount();
        return (count / rows) - ((count - first) / rows) + 1;
    }

    public int getTotalPagesP() {
        return modelP.getRowCount() / rows;
    }

    public int getTotalPagesO() {
        return modelO.getRowCount() / rows;
    }

    public int getTotalPagesU() {
        return modelU.getRowCount() / rows;
    }

    public int getTotalPagesS() {
        return modelS.getRowCount() / rows;
    }

    public int getFirst() {
        return first;
    }

    public void setFirst(int first) {
        this.first = first;
    }

    public int getRows() {
        return rows;
    }

    public String refresh() {
        return "statistics";
    }

    private void copyData() {
        prod_code = posProd.getProd_code();
        name = posProd.getName();
        description = posProd.getDescription();
        prod_group = posProd.getProd_group();
        list_price = posProd.getList_price();
        qty_on_hand = posProd.getQty_on_hand();
        procur_level = posProd.getProcur_level();
        procur_qty = posProd.getProcur_qty();
        quantity = posProd.getQuantity();
    }

    public String viewProduct() {
        if (posProd == null) {
            return "statistics?faces-redirect=true";
        }
        copyData();
        return "proddetails";
    }

    public DataModel<Product> getModelProduct() {
        modelP = new ListDataModel<>(listP);
        return modelP;
    }

    public DataModel<Order> getModelOrder() {
        modelO = new ListDataModel<>(listO);
        return modelO;
    }

    public DataModel<User> getModelUser() {
        modelU = new ListDataModel<>(listU);
        return modelU;
    }

    public DataModel<Supplier> getModelSupplier() {
        modelS = new ListDataModel<>(listS);
        return modelS;
    }

    public int getModelProductRowCount() {
        return (modelP == null) ? 0 : modelP.getRowCount();
    }

    public int getModelOrderRowCount() {
        return (modelO == null) ? 0 : modelO.getRowCount();
    }

    public int getModelUserRowCount() {
        return (modelU == null) ? 0 : modelU.getRowCount();
    }

    public int getModelSupplierRowCount() {
        return (modelS == null) ? 0 : modelS.getRowCount();
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getProd_code() {
        return prod_code;
    }

    public void setProd_code(int prod_code) {
        this.prod_code = prod_code;
    }

    public char getProd_group() {
        return prod_group;
    }

    public void setProd_group(char prod_group) {
        this.prod_group = prod_group;
    }

    public double getList_price() {
        return list_price;
    }

    public void setList_price(double list_price) {
        this.list_price = list_price;
    }

    public double getQty_on_hand() {
        return qty_on_hand;
    }

    public void setQty_on_hand(double qty_on_hand) {
        this.qty_on_hand = qty_on_hand;
    }

    public double getProcur_level() {
        return procur_level;
    }

    public void setProcur_level(double procur_level) {
        this.procur_level = procur_level;
    }

    public double getProcur_qty() {
        return procur_qty;
    }

    public void setProcur_qty(double procur_qty) {
        this.procur_qty = procur_qty;
    }

    public boolean isSearchFilter() {
        return searchFilter;
    }

    public void setSearchFilter(boolean searchFilter) {
        this.searchFilter = searchFilter;
    }

    public int getSearchPage() {
        return searchPage;
    }

    public void setSearchPage(int searchPage) {
        this.searchPage = searchPage;
    }

    public int getRowNumberP() {
        return modelP.getRowIndex();
    }

    public int getRowNumberO() {
        return modelO.getRowIndex();
    }

    public int getRowNumberU() {
        return modelU.getRowIndex();
    }

    public int getRowNumberS() {
        return modelS.getRowIndex();
    }

    public Product getPosProd() {
        return posProd;
    }

    public void setPosProd(Product posProd) {
        this.posProd = posProd;
    }

    public String queryingA() throws SQLException {
        queryno = 1;
        DBComm db = DBComm.getInstance();
        listP = (List<Product>) db.queryingA();
        modelP = new ListDataModel<>(listP);
        return "";
    }

    public String queryingB() throws SQLException {
        queryno = 2;
        DBComm db = DBComm.getInstance();
        listO = (List<Order>) db.queryingB(searchY, searchM, searchAm);
        modelO = new ListDataModel<>(listO);
        return "";
    }

    public String queryingC() throws SQLException {
        queryno = 3;
        DBComm db = DBComm.getInstance();
        listP = (List<Product>) db.queryingC(limitM);
        modelP = new ListDataModel<>(listP);
        return "";
    }

    public String queryingD() throws SQLException {
        queryno = 4;
        DBComm db = DBComm.getInstance();
        listP = (List<Product>) db.queryingD();
        modelP = new ListDataModel<>(listP);
        return "";
    }

    public String queryingE() throws SQLException {
        queryno = 5;
        DBComm db = DBComm.getInstance();
        listP = (List<Product>) db.queryingE();
        modelP = new ListDataModel<>(listP);
        return "";
    }

    public String queryingF() throws SQLException {
        queryno = 6;
        DBComm db = DBComm.getInstance();
        listP = (List<Product>) db.queryingE();
        modelP = new ListDataModel<>(listP);
        return "";
    }

    public boolean hasQueriedA() {
        return queryno == 1;
    }

    public boolean hasQueriedB() {
        System.out.println(queryno);
        return queryno == 2;
    }

    public boolean hasQueriedC() {
        return queryno == 3;
    }

    public boolean hasQueriedD() {
        return queryno == 4;
    }

    public boolean hasQueriedE() {
        return queryno == 5;
    }

    public boolean hasQueriedF() {
        return queryno == 6;
    }

    public String stats2() throws SQLException {
        DBComm db = DBComm.getInstance();
//        listU = (List<User>) db.statistics2(2, searchMU);
        modelU = new ListDataModel<>(listU);
        return "";
    }

    public String stats3() throws SQLException {
        DBComm db = DBComm.getInstance();
//        listS = (List<Supplier>) db.statistics3(3, searchMS);
        modelS = new ListDataModel<>(listS);
        return "";
    }

    public String removeFilters() throws SQLException {
        searchM = searchY = searchAm = 0;
        searchFilter = false;
        listP = null;
        listU = null;
        listS = null;
        return "statistics?faces-redirect=true";
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public Supplier getSup() {
        return sup;
    }

    public void setSup(Supplier sup) {
        this.sup = sup;
    }

    public User getUsr() {
        return usr;
    }

    public void setUsr(User usr) {
        this.usr = usr;
    }

    public Order getOrd() {
        return ord;
    }

    public void setOrd(Order ord) {
        this.ord = ord;
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }

    public Integer getSearchM() {
        return searchM;
    }

    public void setSearchM(Integer searchM) {
        this.searchM = searchM;
    }

    public Integer getSearchY() {
        return searchY;
    }

    public void setSearchY(Integer searchY) {
        this.searchY = searchY;
    }

    public Integer getSearchAm() {
        return searchAm;
    }

    public void setSearchAm(Integer searchAm) {
        this.searchAm = searchAm;
    }

    public Integer getLimitM() {
        return limitM;
    }

    public void setLimitM(Integer limitM) {
        this.limitM = limitM;
    }

    public Products getProductsBean() {
        return productsBean;
    }

    public void setProductsBean(Products productsBean) {
        this.productsBean = productsBean;
    }

    public String getDegreesSupplier1() {
        return degreesSupplier1;
    }

    public void setDegreesSupplier1(String degreesSupplier1) {
        this.degreesSupplier1 = degreesSupplier1;
    }

    public String getDegreesSupplier2() {
        return degreesSupplier2;
    }

    public void setDegreesSupplier2(String degreesSupplier2) {
        this.degreesSupplier2 = degreesSupplier2;
    }

    public String getDegreesResult() {
        return degreesResult;
    }

    public void setDegreesResult(String degreesResult) {
        this.degreesResult = degreesResult;
    }

    public String sixDegrees() {
        int supp1 = productsBean.getSupplierMappings().get(degreesSupplier1);
        int supp2 = productsBean.getSupplierMappings().get(degreesSupplier2);
        try {
            Integer res = DBComm.getInstance().sixDegrees(supp1, supp2);
            if (res < 0) {
                degreesResult = "More than six degrees..";
            } else {
                degreesResult = res.toString();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            degreesResult = "Unable to query database";
        }
        return "";
    }
}
