package beanlayer;

import dbcommlayer.DBComm;
import java.io.Serializable;
import java.sql.SQLException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean(name="registerBean")
@RequestScoped
public class Register implements Serializable {
    //Registration data:
    private String login;
    private String password;
    private String passwordx2;
    private String cust_name;
    private String email;
    private String post_code;
    private String street;
    private String town;
    
    String errorLabel;
    
    public Register() {
    }
    
    public String register() throws SQLException {
                if (password.isEmpty() || login.isEmpty() || cust_name.isEmpty() || passwordx2.isEmpty() || email.isEmpty() ||  post_code.isEmpty() || street.isEmpty() || town.isEmpty() ) {
            errorLabel = "Fill in all fields please..";
            return "";
        }
        if (!password.equals(passwordx2)) {
            errorLabel = "Password verification failed.. Be more careful!";
            return "";
        }
        DBComm db = DBComm.getInstance();
        if (!db.register(login, password, cust_name, email,  street, town,post_code)) {
            errorLabel = "Username already exists, please try a different one..";
            return "";
        }
        return "index.xhtml?faces-redirect=true";
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getCust_name() {
        return cust_name;
    }

    public void setCust_name(String cust_name) {
        this.cust_name = cust_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPost_code() {
        return post_code;
    }

    public void setPost_code(String post_code) {
        this.post_code = post_code;
    }

    public String getStreet() {
        return street;
    }

    public String getPasswordx2() {
        return passwordx2;
    }

    public void setPasswordx2(String passwordx2) {
        this.passwordx2 = passwordx2;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getErrorLabel() {
        return errorLabel;
    }

    public void setErrorLabel(String errorLabel) {
        this.errorLabel = errorLabel;
    }
    
}
