package beanlayer;

import dbcommlayer.DBComm;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import model.Product;

@ManagedBean(name = "searchBean")
@RequestScoped
public class Search implements Serializable {

    private String search;
    private int prod_code;
    private String name;
    private String description;
    private char prod_group;
    private double list_price;

    private String errorLabel;

    private static ArrayList<Product> items;
//            = new ArrayList<>();

    public ArrayList<Product> getItem() {
        SearchDb();
        return items;
    }
//
//    public String addItem() {
//        Item item = new Item();
//        items.add(item);
//        return null;
//    }

    public Search() {

    }

    public void SearchDb() {
        DBComm db = DBComm.getInstance();
        try {
            search = null;
            items = null;
            items = db.browse();
        } catch (SQLException ex) {

        }
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public String getErrorLabel() {
        return errorLabel;
    }

    public void setErrorLabel(String errorLabel) {
        this.errorLabel = errorLabel;
    }

    public int getProd_code() {
        return prod_code;
    }

    public void setProd_code(int prod_code) {
        this.prod_code = prod_code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public char getProd_group() {
        return prod_group;
    }

    public void setProd_group(char prod_group) {
        this.prod_group = prod_group;
    }

    public double getList_price() {
        return list_price;
    }

    public void setList_price(double list_price) {
        this.list_price = list_price;
    }

}
