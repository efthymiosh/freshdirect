package beanlayer;

import dbcommlayer.DBComm;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import model.Product;
import model.Supplier;
import model.User;

@ManagedBean(name = "statsBean")
@ViewScoped
public class Statistics implements Serializable {

    int detailsMode; //0 disabled. 1 viewmode, 2 createmode, 3 editmode.
    String errorMessage;
    private int firstProduct;
    private int firstUser;
    private int firstSupplier;
    final int rows = 20;

    Product prod;
    Supplier sup;
    User usr;

    //edit mode
    Product posProd;
    String remQuantity;
    int prod_code;
    String name;
    String description;
    String errorMessageProduct;
    String errorMessageUser;
    String errorMessageSupplier;
    char prod_group;
    double list_price;
    double qty_on_hand;
    double procur_level;
    double procur_qty;
    double quantity;

    String searchName;

    Integer searchMP;
    Integer searchMU;
    Integer searchMS;

    boolean searchFilter; //enabled / disabled
    private int searchPage;

    private List<Product> listP;
    private transient DataModel<Product> modelP;

    private List<User> listU;
    private transient DataModel<User> modelU;

    private List<Supplier> listS;
    private transient DataModel<Supplier> modelS;

    public Statistics() {
        searchMU = 5;
        searchMS = 5;
        searchMP = 5;
        firstProduct = firstSupplier = firstUser = 0;
        errorMessage = "";
        detailsMode = 0;
        posProd = null;
        searchFilter = false;
        searchPage = 0;
        listP = new ArrayList<>();
        listU = new ArrayList<>();
        listS = new ArrayList<>();
    }
    
    @PostConstruct
    public void postConstructStatistics() {
        stats1();
        stats2();
        stats3();
    }
    
    public void pageFirstProduct() {
        setFirstProduct(0);
    }

    public void pageFirstUser() {
        setFirstUser(0);
    }

    public void pageFirstSupplier() {
        setFirstSupplier(0);
    }
    public void pagePreviousProduct() {
        setFirstProduct(getFirstProduct() - rows);
    }
    public void pagePreviousUser() {
        setFirstUser(getFirstUser() - rows);
    }

    public void pagePreviousSupplier() {
        setFirstSupplier(getFirstSupplier() - rows);
    }

    public void pageNextProduct() {
        setFirstProduct(getFirstProduct() + rows);
    }

    public void pageNextUser() {
        setFirstUser(getFirstUser() + rows);
    }

    public void pageNextSupplier() {
        setFirstSupplier(getFirstSupplier() + rows);
    }

    public int getCurrentPageProduct() {
        int count = modelP.getRowCount();
        return (count / rows) - ((count - firstProduct) / rows) + 1;
    }

    public int getCurrentPageUser() {
        int count = modelU.getRowCount();
        return (count / rows) - ((count - firstUser) / rows) + 1;
    }
    
    public int getCurrentPageSupplier() {
        int count = modelS.getRowCount();
        return (count / rows) - ((count - firstSupplier) / rows) + 1;
    }

    public void pageLastProduct() {
        int count = modelP.getRowCount();
        setFirstProduct(count - ((count % rows != 0) ? count % rows : rows));
    }

    public void pageLastUser() {
        int count = modelU.getRowCount();
        setFirstUser(count - ((count % rows != 0) ? count % rows : rows));
    }

    public void pageLastSupplier() {
        int count = modelS.getRowCount();
        setFirstSupplier(count - ((count % rows != 0) ? count % rows : rows));
    }

    public int getTotalPagesProduct() {
        return modelP.getRowCount() / rows;
    }

    public int getTotalPagesUser() {
        return modelU.getRowCount() / rows;
    }

    public int getTotalPagesSupplier() {
        return modelS.getRowCount() / rows;
    }

    public int getFirstProduct() {
        return firstProduct;
    }

    public void setFirstProduct(int firstProduct) {
        this.firstProduct = firstProduct;
    }

    public int getFirstUser() {
        return firstUser;
    }

    public void setFirstUser(int firstUser) {
        this.firstUser = firstUser;
    }

    public int getFirstSupplier() {
        return firstSupplier;
    }

    public void setFirstSupplier(int firstSupplier) {
        this.firstSupplier = firstSupplier;
    }

    public int getRows() {
        return rows;
    }

    public String refresh() {
        return "statistics";
    }

    private void copyData() {
        prod_code = posProd.getProd_code();
        name = posProd.getName();
        description = posProd.getDescription();
        prod_group = posProd.getProd_group();
        list_price = posProd.getList_price();
        qty_on_hand = posProd.getQty_on_hand();
        procur_level = posProd.getProcur_level();
        procur_qty = posProd.getProcur_qty();
        quantity = posProd.getQuantity();
    }

    public String viewProduct() {
        if (posProd == null) {
            return "statistics?faces-redirect=true";
        }
        copyData();
        detailsMode = 1;
        return "proddetails";
    }

    public DataModel<Product> getModelProduct() {
        modelP = new ListDataModel<>(listP);
        return modelP;
    }

    public DataModel<User> getModelUser() {
        modelU = new ListDataModel<>(listU);
        return modelU;
    }

    public DataModel<Supplier> getModelSupplier() {
        modelS = new ListDataModel<>(listS);
        return modelS;
    }

    public int getModelProductRowCount() {
        return (modelP == null) ? 0 : modelP.getRowCount();
    }

    public int getModelUserRowCount() {
        return (modelU == null) ? 0 : modelU.getRowCount();
    }

    public int getModelSupplierRowCount() {
        return (modelS == null) ? 0 : modelS.getRowCount();
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getProd_code() {
        return prod_code;
    }

    public void setProd_code(int prod_code) {
        this.prod_code = prod_code;
    }

    public char getProd_group() {
        return prod_group;
    }

    public void setProd_group(char prod_group) {
        this.prod_group = prod_group;
    }

    public double getList_price() {
        return list_price;
    }

    public void setList_price(double list_price) {
        this.list_price = list_price;
    }

    public double getQty_on_hand() {
        return qty_on_hand;
    }

    public void setQty_on_hand(double qty_on_hand) {
        this.qty_on_hand = qty_on_hand;
    }

    public double getProcur_level() {
        return procur_level;
    }

    public void setProcur_level(double procur_level) {
        this.procur_level = procur_level;
    }

    public double getProcur_qty() {
        return procur_qty;
    }

    public void setProcur_qty(double procur_qty) {
        this.procur_qty = procur_qty;
    }

    public boolean isSearchFilter() {
        return searchFilter;
    }

    public void setSearchFilter(boolean searchFilter) {
        this.searchFilter = searchFilter;
    }

    public int getSearchPage() {
        return searchPage;
    }

    public void setSearchPage(int searchPage) {
        this.searchPage = searchPage;
    }

    public int getRowNumberP() {
        return modelP.getRowIndex();
    }

    public int getRowNumberU() {
        return modelU.getRowIndex();
    }

    public int getRowNumberS() {
        return modelS.getRowIndex();
    }

    public Product getPosProd() {
        return posProd;
    }

    public void setPosProd(Product posProd) {
        this.posProd = posProd;
    }

    public String stats1() {
        DBComm db = DBComm.getInstance();
        try {
            listP = (List<Product>) db.statistics1(1, searchMP);
        } catch (SQLException ex) {
            ex.printStackTrace();
            errorMessageProduct = "Unable to access database";
            listP = new ArrayList<>();
        }
        modelP = new ListDataModel<>(listP);
        return "";
    }

    public String stats2() {
        DBComm db = DBComm.getInstance();
        try {
            listU = (List<User>) db.statistics2(2, searchMU);
        } catch (SQLException ex) {
            ex.printStackTrace();
            errorMessageUser = "Unable to access database";
            listU = new ArrayList<>();
        }
        modelU = new ListDataModel<>(listU);
        return "";
    }

    public String stats3() {
        DBComm db = DBComm.getInstance();
        try {
            listS = (List<Supplier>) db.statistics3(3, searchMS);
        } catch (SQLException ex) {
            ex.printStackTrace();
            errorMessageSupplier = "Unable to access database";
            listS = new ArrayList<>();
        }
        modelS = new ListDataModel<>(listS);
        return "";
    }

    public String removeFilters() throws SQLException {
        searchMP = searchMS = searchMU = 0;
        searchFilter = false;
        listP = null;
        listU = null;
        listS = null;
        return "statistics?faces-redirect=true";
    }

    public Integer getSearchMP() {
        return searchMP;
    }

    public void setSearchM(Integer searchMP) {
        this.searchMP = searchMP;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public Supplier getSup() {
        return sup;
    }

    public void setSup(Supplier sup) {
        this.sup = sup;
    }

    public User getUsr() {
        return usr;
    }

    public void setUsr(User usr) {
        this.usr = usr;
    }

    public Integer getSearchMU() {
        return searchMU;
    }

    public void setSearchMU(Integer searchMU) {
        this.searchMU = searchMU;
    }

    public Integer getSearchMS() {
        return searchMS;
    }

    public void setSearchMS(Integer searchMS) {
        this.searchMS = searchMS;
    }

    public String getErrorMessageProduct() {
        return errorMessageProduct;
    }

    public void setErrorMessageProduct(String errorMessageProduct) {
        this.errorMessageProduct = errorMessageProduct;
    }

    public String getErrorMessageUser() {
        return errorMessageUser;
    }

    public void setErrorMessageUser(String errorMessageUser) {
        this.errorMessageUser = errorMessageUser;
    }

    public String getErrorMessageSupplier() {
        return errorMessageSupplier;
    }

    public void setErrorMessageSupplier(String errorMessageSupplier) {
        this.errorMessageSupplier = errorMessageSupplier;
    }
    
}
