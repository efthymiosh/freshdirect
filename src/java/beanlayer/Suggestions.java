package beanlayer;

import dbcommlayer.DBComm;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import model.Product;

@ManagedBean(name = "suggestionsBean")
@SessionScoped
public class Suggestions implements Serializable {

    @ManagedProperty(value = "#{userBean}")
    private UserSession userBean;
    
    
    int detailsMode; //0 disabled. 1 viewmode, 2 createmode, 3 editmode.
    String errorMessage;
    private int first;
    final int rows = 20;
    Product prod;
    Product posProd;
    String remQuantity;
    int prod_code;
    int updateSup;
    String name;
    String description;
    char prod_group;
    double list_price;
    double qty_on_hand;
    double procur_level;
    double procur_qty;
    double order_qty;
    double quantity;

    boolean searchFilter; //enabled / disabled
    private int searchPage;
    private List<Product> listP;
    private transient DataModel<Product> modelP;

    public void Suggestions() {
        first = 0;
        errorMessage = "";
        detailsMode = 0;
        posProd = null;
        searchFilter = false;
        searchPage = 0;
        listP = new ArrayList<>();
    }

    public void pageFirst() {
        setFirst(0);
    }

    public void pagePrevious() {
        setFirst(getFirst() - rows);
    }

    public void pageNext() {
        setFirst(getFirst() + rows);
    }

    public void pageLast() {
        int count = modelP.getRowCount();
        setFirst(count - ((count % rows != 0) ? count % rows : rows));
    }

    public int getCurrentPage() {
        int count = modelP.getRowCount();
        return (count / rows) - ((count - first) / rows) + 1;
    }

    public int getTotalPages() {
        return modelP.getRowCount() / rows;
    }

    public int getFirst() {
        return first;
    }

    public void setFirst(int first) {
        this.first = first;
    }

    public int getRows() {
        return rows;
    }

    public boolean isEditMode() {
        return detailsMode == 3;
    }

    public void setSelectedProduct(ValueChangeEvent event) {
        posProd = modelP.getRowData();
    }

    public String refresh() {
        return "product_list";
    }

    private void copyData() {
        prod_code = posProd.getProd_code();
        name = posProd.getName();
        description = posProd.getDescription();
        prod_group = posProd.getProd_group();
        list_price = posProd.getList_price();
        qty_on_hand = posProd.getQty_on_hand();
        procur_level = posProd.getProcur_level();
        procur_qty = posProd.getProcur_qty();
        quantity = posProd.getQuantity();
    }

    public String viewProduct() {
        if (posProd == null) {
            return "";
        }
        copyData();
        return "proddetails";
    }

    public DataModel<Product> getModelProduct() {
        modelP = new ListDataModel<>(listP);
        return modelP;
    }

    public int getModelProductRowCount() {
        return (modelP == null) ? 0 : modelP.getRowCount();
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String initNewRecord() {
        prod_code = 0;
        name = description = "";
        prod_group = ' ';
        list_price = 0.0D;
        qty_on_hand = 0;
        procur_level = 0;
        procur_qty = 0;
        quantity = 0.0D;
        return "";
    }

    public String cancel() {
        prod = null;
        errorMessage = "";
        return "";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getProd_code() {
        return prod_code;
    }

    public void setProd_code(int prod_code) {
        this.prod_code = prod_code;
    }

    public char getProd_group() {
        return prod_group;
    }

    public void setProd_group(char prod_group) {
        this.prod_group = prod_group;
    }

    public double getList_price() {
        return list_price;
    }

    public void setList_price(double list_price) {
        this.list_price = list_price;
    }

    public double getQty_on_hand() {
        return qty_on_hand;
    }

    public void setQty_on_hand(double qty_on_hand) {
        this.qty_on_hand = qty_on_hand;
    }

    public double getProcur_level() {
        return procur_level;
    }

    public void setProcur_level(double procur_level) {
        this.procur_level = procur_level;
    }

    public double getProcur_qty() {
        return procur_qty;
    }

    public void setProcur_qty(double procur_qty) {
        this.procur_qty = procur_qty;
    }

    public boolean isSearchFilter() {
        return searchFilter;
    }

    public void setSearchFilter(boolean searchFilter) {
        this.searchFilter = searchFilter;
    }

    public int getSearchPage() {
        return searchPage;
    }

    public void setSearchPage(int searchPage) {
        this.searchPage = searchPage;
    }

    public int getRowNumber() {
        return modelP.getRowIndex();
    }

    public Product getPosProd() {
        return posProd;
    }

    public void setPosProd(Product posProd) {
        this.posProd = posProd;
    }

    public String getRemQuantity() {
        return remQuantity;
    }

    public void setRemQuantity(String remQuantity) {
        this.remQuantity = remQuantity;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public UserSession getUserBean() {
        return userBean;
    }

    public void setUserBean(UserSession userBean) {
        this.userBean = userBean;
    }
    
    public String buildSuggestions(List<Product> prodList, int cust_no) {
        ArrayList<Product> retval;
        try {
            retval = DBComm.getInstance().suggest(prodList, cust_no);
        } catch (SQLException ex) {
            errorMessage="Unable to retrieve data from database";
            retval = new ArrayList<>();
        }
        listP = retval;
        return "";
    }

    public String buildSuggestions(List<Product> prodList) {
        ArrayList<Product> retval;
        try {
            retval = DBComm.getInstance().suggest(prodList, userBean.getCust_no());
        } catch (SQLException ex) {
            errorMessage="Unable to retrieve data from database";
            retval = new ArrayList<>();
        }
        listP = retval;
        return "";
    }
}
