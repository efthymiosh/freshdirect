/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beanlayer;

import dbcommlayer.DBComm;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import model.Order;

@ManagedBean(name="userOrders")
@ViewScoped
public class UserProfileOrders implements Serializable {

    @ManagedProperty(value="#{userBean}")
    private UserSession userBean;
    private int first;
    final int rows = 4;
    private transient DataModel<Order> modelO;
    private List<Order> listO;
    
    public UserProfileOrders() {
        first = 0;
    }
    
    @PostConstruct
    public void postConstructionUserDataFetch() {
        try {
            listO = (List<Order>) DBComm.getInstance().getUserOrders(userBean.getCust_no());
        } catch (SQLException ex) {
            listO = new ArrayList<>();
        }
    }
    
    public void pageFirst() {
        setFirst(0);
    }

    public void pagePrevious() {
        setFirst(getFirst() - rows);
    }

    public void pageNext() {
        setFirst(getFirst() + rows);
    }

    public void pageLast() {
        int count = modelO.getRowCount();
        setFirst(count - ((count % rows != 0) ? count % rows : rows));
    }

    public int getCurrentPage() {
        int count = modelO.getRowCount();
        return (count / rows) - ((count - first) / rows) + 1;
    }

    public int getTotalPages() {
        return modelO.getRowCount() / rows;
    }

    public int getFirst() {
        return first;
    }

    public void setFirst(int first) {
        this.first = first;
    }

    public int getRows() {
        return rows;
    }
    
    public DataModel<Order> getModelOrder() {
        modelO = new ListDataModel<>(listO);
        return modelO;
    }

    public int getModelOrderRowCount() {
        return (modelO == null) ? 0 : modelO.getRowCount();
    }

    public int getRowNumber() {
        return modelO.getRowIndex();
    }

    public UserSession getUserBean() {
        return userBean;
    }

    public void setUserBean(UserSession userBean) {
        this.userBean = userBean;
    }


    public List<Order> getListO() {
        return listO;
    }

    public void setListO(List<Order> listO) {
        this.listO = listO;
    }
    
    
}
