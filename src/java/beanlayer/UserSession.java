package beanlayer;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import model.User;

@ManagedBean(name="userBean")
@SessionScoped
public class UserSession implements Serializable {
    //User data:
    int cust_no;
    private String login;
    private String cust_name;
    private String email;
    private String post_code;
    private String street;
    private String town;
    private double cr_limit;
    private double curr_bal;
    
    public UserSession() {
    }
    
    public void setData(User user){
        login = user.getLogin();
        cust_name = user.getCust_name();
        email = user.getEmail();
        post_code = user.getPost_code();
        street = user.getStreet();
        town = user.getTown();
        cr_limit = user.getCr_limit();
        curr_bal = user.getCurr_bal();
        cust_no = user.getCust_no();
    }
    
    public void setData(int cust_no, String login, String cust_name, String email, String post_code,
                        String street, String town, int cr_limit, double curr_bal){
        this.cust_no = cust_no;
        this.login = login;
        this.cust_name = cust_name;
        this.email = email;
        this.post_code = post_code;
        this.street = street;
        this.town = town;
        this.cr_limit = cr_limit;
        this.curr_bal = curr_bal;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getCust_name() {
        return cust_name;
    }

    public void setCust_name(String cust_name) {
        this.cust_name = cust_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPost_code() {
        return post_code;
    }

    public void setPost_code(String post_code) {
        this.post_code = post_code;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public double getCr_limit() {
        return cr_limit;
    }

    public void setCr_limit(int cr_limit) {
        this.cr_limit = cr_limit;
    }

    public double getCurr_bal() {
        return curr_bal;
    }

    public void setCurr_bal(double curr_bal) {
        this.curr_bal = curr_bal;
    }

    public int getCust_no() {
        return cust_no;
    }

    public void setCust_no(int cust_no) {
        this.cust_no = cust_no;
    }
    
    public boolean isAdmin() {
        return login.equals("manager");
    }
}
