package beanlayer;

import dbcommlayer.DBComm;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import model.User;

@ManagedBean(name = "usersBean")
@SessionScoped
public class Users implements Serializable {

    String errorMessage;
    private int first;
    final int rows = 20;
    User user;
    User posUser;
    double moneyspent;
    String cust_name;
    int cust_no;
    Integer searchM;

    boolean searchFilter;
    private int searchPage;

    private List<User> listU;

    private transient DataModel<User> modelU;

    public Users() {
        first = 0;
        errorMessage = "";
        posUser = null;
        searchFilter = false;
        searchPage = 0;
        listU = null;
    }

    public void pageFirst() {
        setFirst(0);
    }

    public void pagePrevious() {
        setFirst(getFirst() - rows);
    }

    public void pageNext() {
        setFirst(getFirst() + rows);
    }

    public void pageLast() {
        int count = modelU.getRowCount();
        setFirst(count - ((count % rows != 0) ? count % rows : rows));
    }

    public int getCurrentPage() {
        int count = modelU.getRowCount();
        return (count / rows) - ((count - first) / rows) + 1;
    }

    public int getTotalPages() {
        return modelU.getRowCount() / rows;
    }

    public int getFirst() {
        return first;
    }

    public void setFirst(int first) {
        this.first = first;
    }

    public int getRows() {
        return rows;
    }

    public void setSelectedUser(ValueChangeEvent event) {
        posUser = modelU.getRowData();
    }

    public String refresh() {
        return "awards";
    }

    private void copyData() {
        cust_no = posUser.getCust_no();
        cust_name = posUser.getCust_name();
        moneyspent = posUser.getMoneyspent();
    }

    public String viewUser() {
        if (posUser == null) {
            return "awards?faces-redirect=true";
        }
        copyData();
        return "awards?faces-redirect=true";
    }

    public DataModel<User> getModelUser() {
        modelU = new ListDataModel<>(listU);
        return modelU;
    }

    public int getModelUserRowCount() {
        return (modelU == null) ? 0 : modelU.getRowCount();
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String initNewRecord() {
        cust_no = 0;
        cust_name = "";
        moneyspent = 0.0D;
        return "awards";
    }

    public String getName() {
        return cust_name;
    }

    public void setName(String name) {
        this.cust_name = name;
    }

    public double getMoney_Spent() {
        return moneyspent;
    }

    public void setMoney_Spent(double moneyspent) {
        this.moneyspent = moneyspent;
    }

    public boolean isSearchFilter() {
        return searchFilter;
    }

    public void setSearchFilter(boolean searchFilter) {
        this.searchFilter = searchFilter;
    }

    public int getSearchPage() {
        return searchPage;
    }

    public void setSearchPage(int searchPage) {
        this.searchPage = searchPage;
    }

    public int getRowNumber() {
        return modelU.getRowIndex();
    }

    public User getPosUser() {
        return posUser;
    }

    public void setPosUser(User posProd) {
        this.posUser = posProd;
    }

    public String search() throws SQLException {

        DBComm db = DBComm.getInstance();
        System.out.println("------------------------------------------------");
        listU = (List<User>) db.searchAwards(searchM);
        modelU = new ListDataModel<>(listU);
        return "awards?faces-redirect=true";
    }

    public String removeFilters() throws SQLException {
//        searchSupplierName = null;
        searchM = 0;
        searchFilter = false;
        listU = null;
        return "awards?faces-redirect=true";
    }

    public Integer getSearchM() {
        return searchM;
    }

    public void setSearchM(Integer searchM) {
        this.searchM = searchM;
    }

}
