package dbcommlayer;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Order;
import model.Product;
import model.Supplier;
import model.User;

public class DBComm {

    private static DBComm obj = null;

    final private Connection conn;
    final private Connection connAdmin;

    private boolean productsChanged;
    private boolean suppliersChanged;

    List<Product> products;

    final Map<String, Integer> suppliers;

    DBComm() {
        productsChanged = true;
        suppliersChanged = true;
        products = new ArrayList<>();
        suppliers = new HashMap<>();
        Connection conn = null, connAdmin = null;
        try {
            DBConfig conf = new DBConfig();
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://" + conf.getSqladdr() + ":" + conf.getPort() + "/" + conf.getDatabase();
            conn = DriverManager.getConnection(url + "?user=" + conf.getUser() + "&password=" + conf.getPass());
            connAdmin = DriverManager.getConnection(url + "?user=" + conf.getAdmin() + "&password=" + conf.getAdminPass());
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }
        this.conn = conn;
        this.connAdmin = connAdmin;
    }

    public static synchronized DBComm getInstance() {
        if (obj == null) {
            return obj = new DBComm();
        }
        return obj;
    }

    public boolean register(String login, String password, String cust_name, String email, String street, String town, String post_code) throws SQLException {
        CallableStatement cs = null;
        synchronized (conn) {
            try {
                cs = conn.prepareCall("{call register(?,?,?,?,?,?,?,?)}");
                cs.setString(1, login);
                cs.setString(2, password);
                cs.setString(3, cust_name);
                cs.setString(4, email);
                cs.setString(5, street);
                cs.setString(6, town);
                cs.setString(7, post_code);
                cs.registerOutParameter(8, Types.INTEGER);
                cs.executeQuery();
                int retBool = cs.getInt(8);
                return retBool == 1;
            } catch (SQLException ex) {
                return false;
            } finally {
                try {
                    cs.close();
                } catch (NullPointerException ex) {
                    Logger.getLogger(DBComm.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public User verify(String user, String pass) throws SQLException {
        ResultSet rs;
        synchronized (conn) {
            CallableStatement cs = conn.prepareCall("{call signin(?,?)}");
            cs.setString(1, user);
            cs.setString(2, pass);
            rs = cs.executeQuery();
        }
        User retVal = null;
        if (rs.next()) {
            retVal = new User();
            retVal.setCr_limit(rs.getInt("cr_limit"));
            retVal.setCurr_bal(rs.getDouble("curr_bal"));
            retVal.setCust_name(rs.getString("cust_name"));
            retVal.setCust_no(rs.getInt("cust_no"));
            retVal.setEmail(rs.getString("email"));
            retVal.setLogin(rs.getString("login"));
            retVal.setPassword(rs.getString("password"));
            retVal.setPost_code(rs.getString("post_code"));
            retVal.setStreet(rs.getString("street"));
            retVal.setTown(rs.getString("town"));
        }
        return retVal;
    }

    public void order_qty(Double order_qty, Integer prod, Integer sup) throws SQLException {
        synchronized (conn) {
            CallableStatement cstmt = conn.prepareCall("{call updateProduct(?,?,?)}");
            cstmt.setDouble(1, order_qty);
            cstmt.setInt(2, prod);
            cstmt.setInt(3, sup);
            cstmt.executeQuery();
            productsChanged = true;
        }
    }

    public boolean addProduct(String name, String description, String prod_group, double list_price,
            double qty_on_hand, double procur_level, double procur_qty, int supplier_id) {
        try {
            int retBool;
            synchronized (conn) {
                CallableStatement cstmt = conn.prepareCall("{call newProduct(?,?,?,?,?,?,?,?,?)}");
                cstmt.setString(1, name);
                cstmt.setString(2, description);
                cstmt.setString(3, prod_group);
                cstmt.setDouble(4, list_price);
                cstmt.setDouble(5, qty_on_hand);
                cstmt.setDouble(6, procur_level);
                cstmt.setDouble(7, procur_qty);
                cstmt.setInt(8, supplier_id);
                cstmt.executeQuery();
                retBool = cstmt.getInt(9);
                if (retBool == 1) {
                    productsChanged = true;
                    return true;
                } else {
                    return false;
                }
            }
        } catch (SQLException ex) {
            return false;
        }
    }

    public ArrayList<Product> browse() throws SQLException {
        ResultSet rs;
        synchronized (conn) {
            if (!productsChanged) {
                return new ArrayList<>(products);
            }
            PreparedStatement logQuery = conn.prepareStatement("SELECT * FROM products");
            rs = logQuery.executeQuery();
            products.clear();
            products = new ArrayList<>();
            while (rs.next()) {
                Product item = new Product();
                item.setProd_code(rs.getInt("prod_code"));
                item.setName(rs.getString("name"));
                item.setDescription(rs.getString("description"));
                item.setProd_group(rs.getString("prod_group").charAt(0));
                item.setList_price(rs.getFloat("list_price"));
                item.setQty_on_hand(rs.getFloat("qty_on_hand"));
                item.setProcur_level(rs.getFloat("procur_level"));
                item.setProcur_qty(rs.getFloat("procur_qty"));
                products.add(item);
            }
            productsChanged = false;
            return new ArrayList<>(products);
        }
    }

    public ArrayList<Product> queryingA() throws SQLException {
        ResultSet rs;
        synchronized (conn) {
            CallableStatement cstmt = conn.prepareCall("{call queryingA()}");
            rs = cstmt.executeQuery();
        }
        ArrayList<Product> items;
        items = new ArrayList<>();
        while (rs.next()) {
            Product item = new Product();
            item.setProd_code(rs.getInt("PROD_CODE"));
            item.setName(rs.getString("PNAME"));
            item.setLowest(rs.getFloat("LOW"));
            item.setHighest(rs.getFloat("HIGH"));
            items.add(item);
        }
        return items;
    }

    public ArrayList<Order> queryingB(Integer year, Integer month, double amt) throws SQLException {
        ResultSet rs;
        synchronized (conn) {
            CallableStatement cstmt = conn.prepareCall("{call queryingB(?,?,?)}");
            cstmt.setInt(1, year);
            cstmt.setInt(2, month);
            cstmt.setDouble(3, amt);
            rs = cstmt.executeQuery();
        }
        ArrayList<Order> items;
        items = new ArrayList<>();
        while (rs.next()) {
            Order item = new Order();
            item.setDate(rs.getDate("oday"));
            items.add(item);
        }
        return items;
    }

    public ArrayList<Product> queryingC(Integer limitM) throws SQLException {
        ResultSet rs;
        synchronized (conn) {
            CallableStatement cstmt = conn.prepareCall("{call queryingC(?)}");
            cstmt.setInt(1, limitM);
            rs = cstmt.executeQuery();
        }
        ArrayList<Product> items;
        items = new ArrayList<>();
        while (rs.next()) {
            Product item = new Product();
            item.setProd_code(rs.getInt("PCODE"));
            item.setName(rs.getString("PNAME"));
            item.setVolume(rs.getFloat("VOLUME"));
            items.add(item);
        }
        return items;
    }

    public ArrayList<Product> queryingD() throws SQLException {
        ResultSet rs;
        synchronized (conn) {
            CallableStatement cstmt = conn.prepareCall("{call queryingD()}");
            rs = cstmt.executeQuery();
        }
        ArrayList<Product> items;
        items = new ArrayList<>();
        while (rs.next()) {
            Product item = new Product();
            item.setProd_group(rs.getString("PGROUP").charAt(0));
            item.setProd_code(rs.getInt("PCODE"));
            item.setName(rs.getString("PNAME"));
            item.setList_price(rs.getDouble("PRICE"));
            items.add(item);
        }
        return items;
    }

    public ArrayList<Product> queryingE() throws SQLException {
        ResultSet rs;
        synchronized (conn) {
            CallableStatement cstmt = conn.prepareCall("{call queryingE()}");
            rs = cstmt.executeQuery();
        }
        ArrayList<Product> items;
        items = new ArrayList<>();
        while (rs.next()) {
            Product item = new Product();
            item.setProd_code(rs.getInt("PCODE"));
            item.setName(rs.getString("PNAME"));
            items.add(item);
        }
        return items;
    }

    public ArrayList<Product> queryingF(Integer searchY, Integer searchM) throws SQLException {
        ResultSet rs;
        synchronized (conn) {
            CallableStatement cstmt = conn.prepareCall("{call queryingF(?,?)}");
            cstmt.setInt(1, searchY);
            cstmt.setInt(2, searchM);
            rs = cstmt.executeQuery();
        }
        ArrayList<Product> items;
        items = new ArrayList<>();
        while (rs.next()) {
            Product item = new Product();
            item.setProd_code(rs.getInt("PCODE"));
            item.setName(rs.getString("PNAME"));
            items.add(item);
        }
        return items;
    }

    public ArrayList<Product> statistics1(Integer option, Integer searchM) throws SQLException {
        ResultSet rs;
        synchronized (connAdmin) {
            CallableStatement cstmt = connAdmin.prepareCall("{call statistics(?,?)}");
            cstmt.setInt(1, option);
            cstmt.setInt(2, searchM);
            rs = cstmt.executeQuery();
        }
        ArrayList<Product> items;
        items = new ArrayList<>();
        while (rs.next()) {
            Product item = new Product();
            item.setProd_code(rs.getInt("PROD_CODE"));
            item.setQuantity(rs.getFloat("QUANTITY"));
            items.add(item);
        }
        return items;
    }

    public ArrayList<User> statistics2(Integer option, Integer searchM) throws SQLException {
        ResultSet rs;
        synchronized (connAdmin) {
            CallableStatement cstmt = connAdmin.prepareCall("{call statistics(?,?)}");
            cstmt.setInt(1, option);
            cstmt.setInt(2, searchM);
            rs = cstmt.executeQuery();
        }
        ArrayList<User> items;
        items = new ArrayList<>();
        while (rs.next()) {
            User item = new User();
            item.setPost_code(rs.getString("POST_CODE"));
            item.setQuantity(rs.getFloat("QUANTITY"));
            items.add(item);
        }
        return items;
    }

    public ArrayList<Supplier> statistics3(Integer option, Integer searchM) throws SQLException {
        ResultSet rs;
        synchronized (connAdmin) {
            CallableStatement cstmt = connAdmin.prepareCall("{call statistics(?,?)}");
            cstmt.setInt(1, option);
            cstmt.setInt(2, searchM);
            rs = cstmt.executeQuery();
        }
        ArrayList<Supplier> items;
        items = new ArrayList<>();
        while (rs.next()) {
            Supplier item = new Supplier();
            item.setSupplier_name(rs.getString("SUPPLIER_NAME"));
            item.setSupplier_id(rs.getInt("SUPPLIER_ID"));
            item.setQuantity(rs.getFloat("QUANTITY"));
            item.setProd_code(rs.getInt("PROD_CODE"));
            items.add(item);
        }
        return items;
    }

    public ArrayList<User> searchAwards(Integer searchM) throws SQLException {
        ResultSet rs;
        synchronized (conn) {
            CallableStatement cstmt = conn.prepareCall("{call awards(?)}");
            cstmt.setInt(1, searchM);
            rs = cstmt.executeQuery();
        }
        ArrayList<User> items;
        items = new ArrayList<>();
        while (rs.next()) {
            User item = new User();
            item.setCust_no(rs.getInt("cust_no"));
            item.setMoneyspent(rs.getFloat("moneyspent"));
            items.add(item);
        }
        return items;
    }

    public ArrayList<Product> searchFilters(String searchSupplier, String searchName, String searchDescription,
            String searchCategory, Integer searchOrder) throws SQLException {
        ResultSet rs;
        synchronized (conn) {
            CallableStatement cstmt = conn.prepareCall("{call productBrowsing(?,?,?,?,?)}");
            cstmt.setString(1, searchSupplier);
            cstmt.setString(2, searchName);
            cstmt.setString(3, searchDescription);
            cstmt.setString(4, searchCategory);
            cstmt.setInt(5, searchOrder);
            rs = cstmt.executeQuery();
        }
        ArrayList<Product> items;
        items = new ArrayList<>();
        while (rs.next()) {
            Product item = new Product();
            item.setProd_code(rs.getInt("prod_code"));
            item.setName(rs.getString("name"));
            item.setDescription(rs.getString("description"));
            item.setProd_group(rs.getString("prod_group").charAt(0));
            item.setList_price(rs.getFloat("list_price"));
            item.setQty_on_hand(rs.getFloat("qty_on_hand"));
            item.setProcur_level(rs.getFloat("procur_level"));
            item.setProcur_qty(rs.getFloat("procur_qty"));
            if(searchOrder == 5 || searchOrder == 6)
                item.setSupplier_name(rs.getString("supplier_name"));
            items.add(item);
        }
        return items;
    }

    public ArrayList<Order> getUserOrders(int cust_no) throws SQLException {
        ResultSet rs;
        synchronized (conn) {
            CallableStatement cstmt = conn.prepareCall("{call salesHistory(?)}");
            cstmt.setInt(1, cust_no);
            rs = cstmt.executeQuery();
        }
        ArrayList<Order> items;
        items = new ArrayList<>();
        while (rs.next()) {
            Order item = new Order();
            item.setProd_name(rs.getString("name"));
            item.setOrder_no(rs.getInt("order_no"));
            item.setOrder_qty(rs.getFloat("order_qty"));
            item.setDate(rs.getDate("order_date"));
            items.add(item);
        }
        return items;
    }

    public Integer sixDegrees(Integer sup1, Integer sup2) throws SQLException {
        synchronized (conn) {
            Integer retInt;
            CallableStatement cstmt = conn.prepareCall("{call sixDegrees(?,?,?)}");
            cstmt.setInt(1, sup1);
            cstmt.setInt(2, sup2);
            ResultSet rs = cstmt.executeQuery();
            retInt = cstmt.getInt(3);
            return retInt;
        }
    }

    public String getCreditCard(int cust_no) throws SQLException {
        String retval = null;
        ResultSet rs;
        synchronized (conn) {
            PreparedStatement credQuery = conn.prepareStatement("SELECT credit_card FROM users WHERE cust_no=?");
            credQuery.setInt(1, cust_no);
            rs = credQuery.executeQuery();
        }
        if (rs.next()) {
            retval = rs.getString("credit_card");
        }
        return retval;
    }

    public ArrayList<Product> suggest(List<Product> basketProducts, int cust_no) throws SQLException {
        synchronized (conn) {
            if (basketProducts.isEmpty()) {
                return null;
            }
            ArrayList<Product> items;
            items = new ArrayList<>();
            System.out.println("----------------------------1--------------------------------------");
            CallableStatement cstmt = conn.prepareCall("{call buyingSuggestions(?,?)}");
            Iterator<Product> it = basketProducts.iterator();
            System.out.println("----------------------------2------right before iterator--------------------------------");
            while (it.hasNext()) {
                System.out.println("----------------------------iterating--------------------------------------");
                Product prod = it.next();
                cstmt.setInt(1, prod.getProd_code());
                cstmt.setInt(2, cust_no);
                ResultSet rs = cstmt.executeQuery();
                while (rs.next()) {
                    System.out.println("----------------------------getting suggestions--------------------------------------");
                    Product item = new Product();
                    item.setName(rs.getString("PNAME"));
                    item.setProd_code(rs.getInt("PCODE"));
                    item.setRank(rs.getFloat("RANK"));
                    System.out.println(item.getName());
                    System.out.println(item.getProd_code());
                    System.out.println(item.getRank());
                    items.add(item);
                    System.out.println("----------------------------got suggestions--------------------------------------");
                }
            }
            return items;
        }
    }

    public Integer checkout(String creditCard, int cust_no, List<Product> basketProducts) throws SQLException {
        synchronized (conn) {
            //contact bank for validation of credit card
            //end contact bank
            int retInt;
            PreparedStatement updateCard;
            updateCard = conn.prepareStatement("UPDATE users set credit_card = ? where cust_no = ? ");
            updateCard.setString(1, creditCard);
            updateCard.setInt(2, cust_no);
            updateCard.executeUpdate();
            CallableStatement cstmt = null;
            cstmt = conn.prepareCall("{call checkout(?,?,?,?,?)}");
            if (basketProducts.isEmpty()) {
                return 0;
            }
            //        cust_no, prod_code, amount, int(FIRST(1) OR NOT(2) )
            Product firstProd = basketProducts.get(0);
            cstmt.setInt(1, cust_no);
            cstmt.setInt(2, firstProd.getProd_code());
            cstmt.setDouble(3, firstProd.getInBasketAmt());
            cstmt.setInt(4, 1);
            cstmt.executeQuery();
            retInt = cstmt.getInt(5);
            if (retInt != 1) {
                return retInt;
            }
            Iterator<Product> it = basketProducts.iterator();
            if (it.hasNext()) {
                it.next();
            }
            while (it.hasNext()) {
                Product prod = it.next();
                cstmt.setInt(1, cust_no);
                cstmt.setInt(2, prod.getProd_code());
                cstmt.setDouble(3, prod.getInBasketAmt());
                cstmt.setInt(4, 2);
                cstmt.executeQuery();
                retInt = cstmt.getInt(5);
                if (retInt != 1) {
                    return retInt;
                }
            }
            return 1;
        }
    }

    public boolean finalCheckout(String creditCard, int cust_no) throws SQLException {
        int retBool;
            //contact bank for withdrawal from credit card account
        //end contact bank and if succeeded:
        synchronized (conn) {
            CallableStatement cstmt = conn.prepareCall("{call finalCheckOut(?,?)}");
            cstmt.setInt(1, cust_no);
            cstmt.executeQuery();
            retBool = cstmt.getInt(2);
            return retBool == 1;
        }
    }

    public HashMap<String, Integer> getSuppliers() throws SQLException {
        synchronized (conn) {
            if (suppliersChanged) {
                Statement suppQuery = conn.createStatement();
                ResultSet rs = suppQuery.executeQuery("SELECT supplier_id, supplier_name FROM suppliers_details");
                while (rs.next()) {
                    String name = rs.getString("supplier_name");
                    Integer id = rs.getInt("supplier_id");
                    suppliers.put(name, id);
                }
                suppliersChanged = false;
            }
            return new HashMap<>(suppliers);
        }
    }

}
