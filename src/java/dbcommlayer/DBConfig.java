package dbcommlayer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author efthymiosh
 */
public class DBConfig {
    private String sqladdr;
    private String port;
    private String database;
    private String user;
    private String pass;
    private String admin;
    private String adminPass;
    
    protected DBConfig(){
        FileInputStream in = null;
        try {
            String sep = System.getProperty("file.separator");
            String usr = System.getProperty("user.dir");
            File file = new File(usr + sep + "properties.ini");
              Properties prop = new Properties();
              in = new FileInputStream(file); 
              prop.load(in);
              sqladdr = prop.getProperty("SQLAddress");
              port = prop.getProperty("Port");
              database = prop.getProperty("Database");
              user = prop.getProperty("Username");
              pass = prop.getProperty("Password");
              admin = prop.getProperty("Admin");
              adminPass = prop.getProperty("AdminPass");
        }
        catch (FileNotFoundException e){
            System.err.println("properties.ini not found in user directory:" + e.getMessage());
        }
        catch (IOException e){
            System.err.println("Input/Output Exception:" + e.getMessage());
        }
        finally {
            if (in != null){
                try {
                    in.close();
               } catch (IOException e) {
                   System.err.println("Input/Output Exception:" + e.getMessage());
               }
            }
        }
    }

    public String getSqladdr() {
        return sqladdr;
    }

    public void setSqladdr(String sqladdr) {
        this.sqladdr = sqladdr;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    public String getAdminPass() {
        return adminPass;
    }

    public void setAdminPass(String adminPass) {
        this.adminPass = adminPass;
    }
    
}
