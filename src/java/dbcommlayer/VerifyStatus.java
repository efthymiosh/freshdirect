package dbcommlayer;

public enum VerifyStatus {
    OK,
    INVALID_PASS,
    USER_NOT_FOUND,
    ERROR
}
