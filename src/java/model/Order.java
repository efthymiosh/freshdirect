package model;

import java.sql.Date;

/**
 *
 * @author efthymiosh
 */
public class Order {
    int order_no;
    int prod_code;
    String prod_name;
    double order_qty;
    double order_sum;
    Date date;
    
    public Order() {
        
    }

    public Order(int order_no, int prod_code, double order_qty, double order_sum) {
        this.order_no = order_no;
        this.prod_code = prod_code;
        this.order_qty = order_qty;
        this.order_sum = order_sum;
    }

    public int getOrder_no() {
        return order_no;
    }

    public void setOrder_no(int order_no) {
        this.order_no = order_no;
    }

    public int getProd_code() {
        return prod_code;
    }

    public void setProd_code(int prod_code) {
        this.prod_code = prod_code;
    }

    public double getOrder_qty() {
        return order_qty;
    }

    public void setOrder_qty(double order_qty) {
        this.order_qty = order_qty;
    }

    public double getOrder_sum() {
        return order_sum;
    }

    public void setOrder_sum(double order_sum) {
        this.order_sum = order_sum;
    }

    public String getProd_name() {
        return prod_name;
    }

    public void setProd_name(String prod_name) {
        this.prod_name = prod_name;
    }

    public String getDate() {
        return date.toString();
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
    

}
