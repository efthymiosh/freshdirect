package model;

public class Product {

    private int prod_code;
    private String name;
    private String description;
    private char prod_group;
    private double list_price;
    private double qty_on_hand;
    private double procur_level;
    private double procur_qty;
    private double inBasketAmt;
    private double quantity;
    private double order_qty;
    private double lowest;
    private double highest;
    private double volume;
    private double total_sum;
    private double rank;
    private String supplier_name;

    public Product() {
    }

    public Product(int prod_code, String name, String description, char prod_group,
            double list_price, double qty_on_hand, double procur_level, double procur_qty, double quantity) {
        list_price = 0.0D;
        qty_on_hand = 0.0D;
        procur_level = 0.0D;
        procur_qty = 0.0D;
        this.prod_code = prod_code;
        this.name = name;
        this.description = description;
        this.prod_group = prod_group;
        this.list_price = list_price;
        this.qty_on_hand = qty_on_hand;
        this.procur_level = procur_level;
        this.procur_qty = procur_qty;
        this.quantity = quantity;
        this.order_qty = 0.0D;
        inBasketAmt = 1.0D;
    }

    public double getLowest() {
        return (double) Math.round(lowest * 100) / 100;
    }

    public void setLowest(double lowest) {
        this.lowest = lowest;
    }

    public double getHighest() {
        return highest;
    }

    public void setHighest(double highest) {
        this.highest = highest;
    }

    public int getProd_code() {
        return prod_code;
    }

    public void setProd_code(int prod_code) {
        this.prod_code = prod_code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public char getProd_group() {
        return prod_group;
    }

    public void setProd_group(char prod_group) {
        this.prod_group = prod_group;
    }

    public double getList_price() {
        return (double) Math.round(list_price * 100) / 100;
    }

    public void setList_price(double list_price) {
        this.list_price = list_price;
    }

    public double getQty_on_hand() {
        return qty_on_hand;
    }

    public void setQty_on_hand(double qty_on_hand) {
        this.qty_on_hand = qty_on_hand;
    }

    public double getProcur_level() {
        return procur_level;
    }

    public void setProcur_level(double procur_level) {
        this.procur_level = procur_level;
    }

    public double getProcur_qty() {
        return procur_qty;
    }

    public void setProcur_qty(double procur_qty) {
        this.procur_qty = procur_qty;
    }

    public double getInBasketAmt() {
        return inBasketAmt;
    }

    public void setInBasketAmt(double inBasketAmt) {
        this.inBasketAmt = inBasketAmt;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getOrder_qty() {
        return order_qty;
    }

    public void setOrder_qty(double order_qty) {
        this.order_qty = order_qty;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    public double getTotal_sum() {
        return (double) Math.round(inBasketAmt * list_price * 100) / 100;
    }

    public void setTotal_sum(double total_sum) {
        this.total_sum = total_sum;
    }

    public double getRank() {
        return (double) Math.round(rank * 100) / 100;
    }

    public void setRank(double rank) {
        this.rank = rank;
    }

    public String getSupplier_name() {
        return supplier_name;
    }

    public void setSupplier_name(String supplier_name) {
        this.supplier_name = supplier_name;
    }
}
