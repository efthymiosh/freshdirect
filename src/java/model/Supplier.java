package model;

public class Supplier {

    int supplier_id;
    String supplier_name;
    String email;
    double quant_sofar;
    double quantity;
    Integer prod_code ;

    public Integer getProd_code() {
        return prod_code;
    }

    public void setProd_code(Integer prod_code) {
        this.prod_code = prod_code;
    }

    public int getSupplier_id() {
        return supplier_id;
    }

    public void setSupplier_id(int supplier_id) {
        this.supplier_id = supplier_id;
    }

    public String getSupplier_name() {
        return supplier_name;
    }

    public void setSupplier_name(String supplier_name) {
        this.supplier_name = supplier_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getQuant_sofar() {
        return quant_sofar;
    }

    public void setQuant_sofar(double quant_sofar) {
        this.quant_sofar = quant_sofar;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

}
