package model;

import java.text.DecimalFormat;

public class User {

    private int cust_no;
    private String login;
    private String password;
    private String cust_name;
    private String email;
    private String street;
    private String town;
    private String post_code;
    private double cr_limit;
    private double curr_bal;
    private double moneyspent;
    private double quantity;

    public User() {
    }

    public User(int cust_no, String login, String password, String cust_name,
            String email, String street, String town, String post_code,
            double cr_limit, double curr_bal) {
        curr_bal = 0.0D;
        cr_limit = 0.0D;
        this.cust_no = cust_no;
        this.cust_name = cust_name;
        this.login = login;
        this.password = password;
        this.email = email;
        this.street = street;
        this.town = town;
        this.post_code = post_code;
        this.cr_limit = cr_limit;
        this.curr_bal = curr_bal;
    }

    public int getCust_no() {
        return cust_no;
    }

    public void setCust_no(int cust_no) {
        this.cust_no = cust_no;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCust_name() {
        return cust_name;
    }

    public void setCust_name(String cust_name) {
        this.cust_name = cust_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getPost_code() {
        return post_code;
    }

    public void setPost_code(String post_code) {
        this.post_code = post_code;
    }

    public double getCr_limit() {
        return cr_limit;
    }

    public void setCr_limit(int cr_limit) {
        this.cr_limit = cr_limit;
    }

    public double getCurr_bal() {
        return curr_bal;
    }

    public void setCurr_bal(double curr_bal) {
        this.curr_bal = curr_bal;
    }

    public double getMoneyspent() {

        return moneyspent;
    }

    public void setMoneyspent(double moneyspent) {
        this.moneyspent = moneyspent;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

}
